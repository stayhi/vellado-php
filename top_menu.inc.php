<?php


//$member = $db->row("select * from g5_member where mb_id='{$_SESSION['ss_mb_id']}'");

if($db) {
    $gzss_config = $db->row("SELECT * FROM gzss_config ");
}

?>
<!--최상단 배너-->
<div class="main_banner_container">
    <div class="main_banner_logo">
    </div>
    <div class="main_banner_btn_container">
        <?php if(!$_SESSION['ss_mb_id']) :?>
            <a href="/login"><img src="/resources/images/btn-my.png"></a>
        <?php else: ?>
            <a href="/myinfo"><img src="/resources/images/btn-my.png"></a>
        <?php endif; ?>
        <?php if($member['mb_level'] >= 8) :?>
        <a href="/board/adm/"><img src="/resources/images/btn-admin.png"></a>
        <?php endif; ?>
        <div class="main_btn_container_special"><img src="/resources/images/icon-shortcut.png"><div><a href="<?=$gzss_config['support_link']?>" target="_blank">구독안내</a></div></div>
        <?php if($_SESSION['ss_mb_id']) :?>
            <div class="main_btn_container_special"><img src="/resources/images/icon-log.png"><div><a href="/logout">로그아웃</a></div></div>
        <?php endif; ?>
    </div>
</div>
<!-- 최상단 배너 모바일-->
<div class="main_banner_container_m">
    <div class="main_banner_logo_m">
        <a href="/main"><img src="/resources/images/logo-m.png"></a>
    </div>
    <div class="main_banner_btn_m">
        <a href="javascript:;" onClick="history.back()"><img src="/resources/images/btn-back.png"></a>
        <a href="<?=$_SESSION['ss_mb_id'] ? "/myinfo" : "/login" ?>"><img src="/resources/images/btn-my-2.png"></a>
        <?php if($member['mb_level'] >= 8) :?>
            <img src="/resources/images/btn-admin-2.png">
        <?php endif; ?>
        <a href="<?=$gzss_config['support_link']?>" target="_blank"><img src="/resources/images/icon-shortcut-2.png"></a>
        <?php if($_SESSION['ss_mb_id']) :?>
            <a href="/logout"><img src="/resources/images/btn-log-2.png"></a>
        <?php endif; ?>
    </div>
</div>

