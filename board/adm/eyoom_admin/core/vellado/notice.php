<?php
/**
 * @file    /adm/eyoom_admin/core/sms/config.php
 */
if (!defined('_EYOOM_IS_ADMIN_')) exit;

$sub_menu = "800800";
include_once(EYOOM_ADMIN_CORE_PATH . '/vellado/_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = "공지사항 관리";

include_once G5_PATH . "/../lib/env.lib.php";
include_once G5_PATH . "/../lib/_dbconnect.php";
include_once G5_PATH . "/../lib/_dbconnect_chat.php";
include_once G5_PATH . "/../lib/SimpleDB.php";

$db = new SimpleDB($pdo_db);

$chatdb = new SimpleDB($pdo_chat_db);

$chat_config = $chatdb->row("SELECT * FROM pchat_config ");
$channel_config = $chatdb->row("SELECT * FROM pchat_channel_info WHERE channel_id='ALL' ");
$channel_config['guest_allow_chat'] = $channel_config['is_guest_allow_chat'] == "N" ? "(금지됨)" : "(허용됨)";

$chat_user_count = $chatdb->single("SELECT cur_user FROM pchat_channel_info WHERE channel_id='ALL' ");

$gzss_config = $db->row("SELECT * FROM gzss_config LIMIT 1");

