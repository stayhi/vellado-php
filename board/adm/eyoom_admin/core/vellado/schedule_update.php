<?php
/**
 * @file    /adm/eyoom_admin/core/member/member_form_update.php
 */
if (!defined('_EYOOM_IS_ADMIN_')) exit;

$sub_menu = "800600";


include_once(G5_LIB_PATH.'/register.lib.php');
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

include_once G5_PATH . "/../lib/env.lib.php";
include_once G5_PATH . "/../lib/_dbconnect.php";
include_once G5_PATH . "/../lib/_dbconnect_chat.php";
include_once G5_PATH . "/../lib/SimpleDB.php";

$db = new SimpleDB($pdo_db);
$chatdb = new SimpleDB($pdo_chat_db);


if ($w == 'u')
    check_demo();

auth_check($auth[$sub_menu], 'w');

check_admin_token();


$schedule = [];
for($i = 0; $i < 24; $i++ ) {
    $schedule[] = [
        "streamer" => trim($_POST['streamer'][$i]),
        "live" => trim($_POST['live'][$i]),
    ];
}

$schedule_json = json_encode($schedule, JSON_UNESCAPED_UNICODE);


$sql = "update gzss_config set schedule=:schedule";

$db->query($sql, ["schedule" => $schedule_json]);


$lev = clean_xss_tags(trim($_POST['lev']));
$cert = clean_xss_tags(trim($_POST['cert']));
$open = clean_xss_tags(trim($_POST['open']));
$adt = clean_xss_tags(trim($_POST['adt']));
$mail = clean_xss_tags(trim($_POST['mail']));
$sms = clean_xss_tags(trim($_POST['sms']));
$sdt = clean_xss_tags(trim($_POST['sdt']));
$fr_date = trim($_POST['fr_date']);
$to_date = trim($_POST['to_date']);
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';

$qstr .= $wmode ? '&amp;wmode=1': '';
$qstr .= $lev ? '&amp;lev='.$lev: '';
$qstr .= $cert ? '&amp;cert='.$cert: '';
$qstr .= $open ? '&amp;open='.$open: '';
$qstr .= $adt ? '&amp;adt='.$adt: '';
$qstr .= $mail ? '&amp;mail='.$mail: '';
$qstr .= $sms ? '&amp;sms='.$sms: '';
$qstr .= $sdt ? '&amp;sdt='.$sdt: '';
$qstr .= $fr_date ? '&amp;fr_date='.$fr_date: '';
$qstr .= $to_date ? '&amp;to_date='.$to_date: '';

run_event('admin_member_form_update', $w, $mb_id);

goto_url(G5_ADMIN_URL . '/?dir=vellado&amp;pid=schedule&amp;'.$qstr.'&amp;w=u&amp;mb_id='.$mb_id, false);