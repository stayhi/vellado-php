<?php
/**
 * @file    /adm/eyoom_admin/core/sms/config.php
 */
if (!defined('_EYOOM_IS_ADMIN_')) exit;

$sub_menu = "800400";
include_once(EYOOM_ADMIN_CORE_PATH . '/vellado/_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = "SMS 기본설정";

include_once G5_PATH . "/../lib/env.lib.php";
include_once G5_PATH . "/../lib/_dbconnect.php";
include_once G5_PATH . "/../lib/_dbconnect_chat.php";
include_once G5_PATH . "/../lib/SimpleDB.php";

$db = new SimpleDB($pdo_db);

$chatdb = new SimpleDB($pdo_chat_db);

$chat_config = $chatdb->row("SELECT * FROM pchat_config ");
$channel_config = $chatdb->row("SELECT * FROM pchat_channel_info WHERE channel_id='ALL' ");
$channel_config['guest_allow_chat'] = $channel_config['is_guest_allow_chat'] == "N" ? "(금지됨)" : "(허용됨)";

$chat_user_count = $chatdb->single("SELECT cur_user FROM pchat_channel_info WHERE channel_id='ALL' ");

$gzss_config = $db->row("SELECT * FROM gzss_config LIMIT 1");


$list = [];

$sql_common = " from bootpay_payment ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        case 'order_id' :
            $sql_search .= " ({$sfl} = '{$stx}') ";
            break;
        case 'price' :
            $sql_search .= " ({$sfl} = '{$stx}') ";
            break;
        case 'method' :
            $sql_search .= " ({$sfl} = '{$stx}') ";
            break;
        case 'item_name' :
            $sql_search .= " ({$sfl} = '{$stx}') ";
            break;
        case 'message' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        case 'card_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        case 'mb_id' :
            $sql_search .= " ({$sfl} = '{$stx}') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

// 기간검색이 있다면
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';

if ( $fr_date && $to_date) {
    $sql_search .= " and complete_datetime between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
    $qstr .= "&amp;fr_date={$fr_date}&amp;to_date={$to_date}";
}

if (!$sst) {
    $sst = "no";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = $db->row($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *  {$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";

$chat_list  = $db->query($sql);


foreach($chat_list as $i => $row) {
    $mb = get_member($row['mb_id']);
    $mb_nick = get_sideview($mb['mb_id'], get_text($mb['mb_nick']), '', '');
    $row['mb_nick'] = $mb['mb_nick'];

    $chat_donation  = $chatdb->row("select * from pchat_donation where tr_id=:tr_id", ['tr_id' => $row['receipt_id']]);
    $row['is_chat_view'] = $chat_donation['is_chat_view'];
    $row['view_second'] = $chat_donation['view_second'];
    $row['is_delete'] = $chat_donation['is_delete'];
    $row['vnum'] = intval($chat_donation['amount']);

    if($row['vnum'] >= 1 &&  $row['vnum'] <= 3) {
        $row['color'] = "#00d9e6";
    } else if($row['vnum'] >= 4 && $row['vnum'] <= 6) {
        $row['color'] = "#1980ff";
    } else if($row['vnum'] >= 7 && $row['vnum'] <= 9) {
        $row['color'] = "#aa15ff";
    } else {
        $row['color'] = "#666";
    }

    if($row['channel_id'] == "ALL") {
        $row['channel_name'] ="<span style='color:purple'>GZSS TV</span>";
    } else if($row['channel_id'] == "VELLADO") {
        $row['channel_name'] = "<span style='color:blue'>벨라 TV</span>";
    } else {
        $row['channel_name'] = "<span style='color:purple'>GZSS TV</span>";
    }

    $list[$i] = $row;
    $list[$i]['bg'] = 'bg'.($i%2);
    $list[$i]['photo_url'] = mb_photo_url($row['mb_id']);
    $list_num = $total_count - ($page - 1) * $rows;
    $list[$i]['num'] = $list_num - $k;
    $k++;
}

/**
 * 페이징
 */
$paging = $eb->set_paging('admin', $dir, $pid, $qstr);


$frm_submit  = ' <div class="text-center margin-top-10 margin-bottom-10"> ';
$frm_submit .= ' <input type="submit" value="검색" class="btn-e btn-e-lg btn-e-dark" accesskey="s">' ;
$frm_submit .= '</div>';