<?php
/**
 * @file    /adm/eyoom_admin/core/sms/config.php
 */
if (!defined('_EYOOM_IS_ADMIN_')) exit;

$sub_menu = "800600";
include_once(EYOOM_ADMIN_CORE_PATH . '/vellado/_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = "SMS 기본설정";

include_once G5_PATH . "/../lib/env.lib.php";
include_once G5_PATH . "/../lib/_dbconnect.php";
include_once G5_PATH . "/../lib/_dbconnect_chat.php";
include_once G5_PATH . "/../lib/SimpleDB.php";


$db = new SimpleDB($pdo_db);

$chatdb = new SimpleDB($pdo_chat_db);

$chat_config = $chatdb->row("SELECT * FROM pchat_config ");
$channel_config = $chatdb->row("SELECT * FROM pchat_channel_info WHERE channel_id='ALL' ");
$channel_config['guest_allow_chat'] = $channel_config['is_guest_allow_chat'] == "N" ? "(금지됨)" : "(허용됨)";

$chat_user_count = $chatdb->single("SELECT cur_user FROM pchat_channel_info WHERE channel_id='ALL' ");

$gzss_config = $db->row("SELECT * FROM gzss_config LIMIT 1");


$schedule = json_decode($gzss_config['schedule'], true);


$list = [];
for($i = 0; $i < 24; $i++) {
    $start = sprintf("%02d:00", $i);
    $end = sprintf("%02d:00", $i + 1);
    $list[$i] = [
        "num" => $i+1,
        "time" => "{$start} ~ {$end}",
        "streamer" => trim($schedule[$i]['streamer']),
        "live" => trim($schedule[$i]['live']),
    ];
}

$action_url1 = G5_ADMIN_URL . "/?dir=vellado&amp;pid=schedule_update&amp;smode=1";


/**
 * 버튼
 */

$frm_submit  = ' <div class="text-center margin-top-30 margin-bottom-30"> ';
$frm_submit .= ' <input type="submit" value="일정 변경" class="btn-e btn-e-lg btn-e-red" accesskey="s">' ;
$frm_submit .= '</div>';

