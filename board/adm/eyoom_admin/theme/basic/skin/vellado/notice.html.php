<?php
/**
 * Eyoom Admin Skin File
 * @file    ~/theme/basic/skin/config/config_form.html.php
 */
if (!defined('_EYOOM_IS_ADMIN_')) exit;

add_stylesheet('<link rel="stylesheet" href="' . G5_JS_URL . '/remodal/remodal.css">', 11);
add_stylesheet('<link rel="stylesheet" href="' . G5_JS_URL . '/remodal/remodal-default-theme.css">', 12);
add_javascript('<script src="' . G5_JS_URL . '/remodal/remodal.js"></script>', 10);
?>




    <div class="admin-config-form">
        <form name="fconfigform" id="fconfigform" method="post" onsubmit="return fconfigform_submit(this);"
              class="eyoom-form">
            <input type="hidden" name="token" id="token" value="">

            <div class="adm-headline">
                <h3>VELLADO 공지사항</h3>
            </div>

            <div id="anc_cf_basic">
                <div class="pg-anchor">
                    <?php echo adm_pg_anchor('anc_cf_basic'); ?>
                </div>
                <div class="adm-table-form-wrap margin-bottom-30">
                    <iframe src="<?=G5_BBS_URL?>/board.php?bo_table=notice" width="100%" height="800px" frameborder="0" ></iframe>


                </div>
            </div>




        </form>
    </div>


<?php
?>