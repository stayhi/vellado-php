<?php
/**
 * Eyoom Admin Skin File
 * @file    ~/theme/basic/skin/config/config_form.html.php
 */
if (!defined('_EYOOM_IS_ADMIN_')) exit;

add_stylesheet('<link rel="stylesheet" href="' . G5_JS_URL . '/remodal/remodal.css">', 11);
add_stylesheet('<link rel="stylesheet" href="' . G5_JS_URL . '/remodal/remodal-default-theme.css">', 12);
add_javascript('<script src="' . G5_JS_URL . '/remodal/remodal.js"></script>', 10);
?>

    <style>
        @media (min-width: 1100px) {
            .pg-anchor-in.tab-e2 .nav-tabs li a {
                font-size: 14px;
                font-weight: bold;
                padding: 8px 17px
            }

            .pg-anchor-in.tab-e2 .nav-tabs li.active a {
                z-index: 1;
                border: 1px solid #000;
                border-top: 1px solid #DE2600;
                color: #DE2600
            }

            .pg-anchor-in.tab-e2 .tab-bottom-line {
                position: relative;
                display: block;
                height: 1px;
                background: #000;
                margin-bottom: 20px
            }
        }

        @media (max-width: 1099px) {
            .pg-anchor-in {
                position: relative;
                overflow: hidden;
                margin-bottom: 20px;
                border: 1px solid #757575
            }

            .pg-anchor-in.tab-e2 .nav-tabs li {
                width: 33.33333%;
                margin: 0
            }

            .pg-anchor-in.tab-e2 .nav-tabs li a {
                font-size: 12px;
                padding: 6px 0;
                text-align: center;
                border-bottom: 1px solid #d5d5d5;
                margin-right: 0;
                font-weight: bold;
                background: #fff
            }

            .pg-anchor-in.tab-e2 .nav-tabs li.active a {
                border: 0;
                border-bottom: 1px solid #d5d5d5 !important;
                color: #DE2600;
                background: #fff1f0
            }

            .pg-anchor-in.tab-e2 .nav-tabs li:nth-child(1) a {
                border-right: 1px solid #d5d5d5
            }

            .pg-anchor-in.tab-e2 .nav-tabs li:nth-child(2) a {
                border-right: 1px solid #d5d5d5
            }

            .pg-anchor-in.tab-e2 .nav-tabs li:nth-child(4) a {
                border-right: 1px solid #d5d5d5
            }

            .pg-anchor-in.tab-e2 .nav-tabs li:nth-child(5) a {
                border-right: 1px solid #d5d5d5
            }

            .pg-anchor-in.tab-e2 .nav-tabs li:nth-child(7) a {
                border-right: 1px solid #d5d5d5;
                border-bottom: 0 !important
            }

            .pg-anchor-in.tab-e2 .nav-tabs li:nth-child(8) a {
                border-right: 1px solid #d5d5d5;
                border-bottom: 0 !important
            }

            .pg-anchor-in.tab-e2 .nav-tabs li:nth-child(9) a {
                border-bottom: 0 !important
            }

            .pg-anchor-in.tab-e2 .tab-bottom-line {
                display: none
            }
        }

        .cf_cert_hide {
            display: none;
            position: absolute;
            top: -20000px;
            left: -10000px
        }

        .icode_old_version th {
            background-color: #FFFCED !important;
        }

        .icode_json_version th {
            background-color: #F6F1FF !important;
        }

        .cf_tr_hide {
            display: none;
        }
    </style>

    <style>
        .input_right {
            text-align: right;
            padding-right: 5px;
        }

        .image_preview img {
            width: 495px;
            height: 278px;
        }
    </style>


    <div class="admin-config-form">
        <form name="fconfigform" id="fconfigform" method="post" onsubmit="return fconfigform_submit(this);"
              class="eyoom-form">
            <input type="hidden" name="token" id="token" value="">

            <div class="adm-headline">
                <h3>VELLADO 환경 설정</h3>
            </div>

            <div id="anc_cf_basic">
                <div class="pg-anchor">
                    <?php echo adm_pg_anchor('anc_cf_basic'); ?>
                </div>
                <div class="adm-table-form-wrap margin-bottom-30">
                    <header><strong><i class="fas fa-caret-right"></i> 기본설정 </strong></header>
                    <div class="table-list-eb">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo_id" class="label">구독안내 링크 </label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input">
                                            <input type="text" name="support_link" id='support_link' value="<?= $gzss_config['support_link'] ?>">
                                        </label>
                                        <div class="note"><strong>Note:</strong> https:// 부터 입력하세요.</div>
                                        <input id="btn_save_support" type="button" value="기본 설정 저장" class="btn-e btn-e-lg btn-e-red">
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div id="anc_cf_basic">
                <div class="pg-anchor">
                    <?php echo adm_pg_anchor('anc_cf_basic'); ?>
                </div>
                <div class="adm-table-form-wrap margin-bottom-30">
                    <header><strong><i class="fas fa-caret-right"></i> VELLADO 메인 동영상 설정</strong></header>
                    <div class="table-list-eb">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo_id" class="label">라이브 아이디</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input form-width-250px">
                                            <input type="text" name="vimeo_id" id='vimeo_id' value="<?= $gzss_config['vimeo_id'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo_url" class="label">라이브 url</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input form-width-250px">
                                            <input type="text" name="vimeo_url" id='vimeo_url' value="<?= $gzss_config['vimeo_url'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo_name" class="label">라이브 닉네임</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input form-width-250px">
                                            <input type="text" name="vimeo_name" id='vimeo_name' value="<?= $gzss_config['vimeo_name'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo_date" class="label">라이브 날짜</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input form-width-250px">
                                            <input type="text" name="vimeo_date" id='vimeo_date' value="<?= $gzss_config['vimeo_date'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo_title" class="label">라이브 타이틀</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input">
                                            <input type="text" name="vimeo_title" id='vimeo_title' value="<?= $gzss_config['vimeo_title'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo_description" class="label">라이브 설명</label>
                                    </th>
                                    <td colspan="3">

                                        <label class="textarea">
                                            <textarea name="vimeo_description" id="vimeo_description" rows="8"><?= $gzss_config['vimeo_description'] ?></textarea>
                                        </label>
                                        <div class="note"><strong>Note:</strong> 줄띄움이 필요한경우 &lt;br&gt; 태그를 입력하세요.</div>
                                    </td>

                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="" class="label">라이브 썸네일</label>
                                    </th>
                                    <td colspan="3">
                                        <div class="image_preview" data-type="image1">
                                            <?= $gzss_config['thumbnail_image'] ?>
                                        </div>
                                        <input type="file" name="vimeo_file" data-type="image1"/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center margin-top-30 margin-bottom-30">
                <input id="btn_save_vimeo" type="button" value="라이브(메인) 설정 저장" class="btn-e btn-e-lg btn-e-red" accesskey="s">
            </div>

            <div id="anc_cf_basic">
                <div class="pg-anchor">
                    <?php echo adm_pg_anchor('anc_cf_basic'); ?>
                </div>
                <div class="adm-table-form-wrap margin-bottom-30">
                    <header><strong><i class="fas fa-caret-right"></i> VELLADO 메인 동영상 설정(서브)</strong></header>
                    <div class="table-list-eb">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo2_id" class="label">라이브2 아이디</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input form-width-250px">
                                            <input type="text" name="vimeo2_id" id='vimeo2_id' value="<?= $gzss_config['vimeo2_id'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo2_url" class="label">라이브2 url</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input form-width-250px">
                                            <input type="text" name="vimeo2_url" id='vimeo2_url' value="<?= $gzss_config['vimeo2_url'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo2_name" class="label">라이브2 닉네임</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input form-width-250px">
                                            <input type="text" name="vimeo2_name" id='vimeo2_name' value="<?= $gzss_config['vimeo2_name'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo2_date" class="label">라이브2 날짜</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input form-width-250px">
                                            <input type="text" name="vimeo_date" id='vimeo2_date' value="<?= $gzss_config['vimeo2_date'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo2_title" class="label">라이브2 타이틀</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="input">
                                            <input type="text" name="vimeo2_title" id='vimeo2_title' value="<?= $gzss_config['vimeo2_title'] ?>">
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="vimeo2_description" class="label">라이브2 설명</label>
                                    </th>
                                    <td colspan="3">

                                        <label class="textarea">
                                            <textarea name="vimeo2_description" id="vimeo2_description" rows="8"><?= $gzss_config['vimeo2_description'] ?></textarea>
                                        </label>
                                        <div class="note"><strong>Note:</strong> 줄띄움이 필요한경우 &lt;br&gt; 태그를 입력하세요.</div>
                                    </td>

                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="" class="label">라이브2 썸네일</label>
                                    </th>
                                    <td colspan="3">
                                        <div class="image_preview" data-type="image2">
                                            <?= $gzss_config['thumbnail2_image'] ?>
                                        </div>
                                        <input type="file" name="vimeo_file" data-type="image2"/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center margin-top-30 margin-bottom-30">
                <input id="btn_save_vimeo2" type="button" value="라이브(서브) 설정 저장" class="btn-e btn-e-lg btn-e-red" accesskey="s">
            </div>


            <div id="anc_cf_board">
                <div class="pg-anchor">
                    <?php echo adm_pg_anchor('anc_cf_board'); ?>
                </div>

                <div class="adm-table-form-wrap margin-bottom-30">
                    <header><strong><i class="fas fa-caret-right"></i> 채팅 기본 설정</strong></header>

                    <div class="table-list-eb">

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th class="table-form-th">
                                       채팅 접속자수
                                    </th>
                                    <td>
                                        <label class="input form-width-250px">

                                        </label>
                                        <?= $chat_user_count ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="table-form-th">
                                        <label for="cf_filter" class="label">단어 필터링</label>
                                    </th>
                                    <td colspan="3">
                                        <label class="textarea">
                                            <textarea name="filter_word" id="filter_word" rows="8"><?= $chat_config['filter_word'] ?></textarea>
                                        </label>
                                        <input id="btn_filter_word" type="button" value="필터링 단어 변경" class="btn-e btn-e-lg btn-e-red">
                                        <div class="note"><strong>Note:</strong> 채팅에서 필터링 할 단어를 지정합니다. 단어 구분은 콤마입니다.</div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <?php if (!G5_IS_MOBILE) { ?>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>

            <?php echo $frm_submit; // 버튼 ?>


        </form>
    </div>

    <script>
        $('.pg-anchor a').on('click', function (e) {
            e.stopPropagation();
            var scrollTopSpace;
            if (window.innerWidth >= 1100) {
                scrollTopSpace = 70;
            } else {
                scrollTopSpace = 70;
            }
            var tabLink = $(this).attr('href');
            var offset = $(tabLink).offset().top;
            $('html, body').animate({scrollTop: offset - scrollTopSpace}, 500);
            return false;
        });

        $(function () {
            <?php if (!$config['cf_cert_use']) { ?>
            $(".cf_cert_service").addClass("cf_cert_hide");
            <?php } ?>

            $("#cf_cert_use").change(function () {
                switch ($(this).val()) {
                    case "0":
                        $(".cf_cert_service").addClass("cf_cert_hide");
                        break;
                    default:
                        $(".cf_cert_service").removeClass("cf_cert_hide");
                        break;
                }
            });

            $("#cf_captcha").on("change", function () {
                if ($(this).val() == 'recaptcha' || $(this).val() == 'recaptcha_inv') {
                    $("[class^='kcaptcha_']").hide();
                } else {
                    $("[class^='kcaptcha_']").show();
                }
            }).trigger("change");
        });

        function fconfigform_submit(f) {
            f.action = "<?php echo $action_url1; ?>";
            return true;
        }

        <?php if (G5_IS_MOBILE) { ?>
        $(function () {
            $(".adm-table-form-wrap td").removeAttr('colspan');
        });
        <?php } ?>
    </script>

    <script>
        $(function () {

            $("#btn_save_support").click(function () {
                var support_link = $("#support_link").val();

                $.ajax({
                    url: g5_admin_url + "/vellado/ajax.config.php",
                    type: "POST",
                    data: {
                        cmd: 'save_support_link',
                        'support_link': support_link,
                    },
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data, textStatus) {
                        if (data.returnCode == "200") {
                            alert("변경되었습니다.")
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                });
            });


            $("#btn_save_vimeo").click(function () {
                var vimeo_id = $("#vimeo_id").val();
                var vimeo_url = $("#vimeo_url").val();
                var vimeo_title = $("#vimeo_title").val();
                var vimeo_description = $("#vimeo_description").val();
                var vimeo_name = $("#vimeo_name").val();
                var vimeo_date = $("#vimeo_date").val();
                $.ajax({
                    url: g5_admin_url + "/vellado/ajax.config.php",
                    type: "POST",
                    data: {
                        cmd: 'save_vimeo_info',
                        'vimeo_id': vimeo_id,
                        'vimeo_url': vimeo_url,
                        'vimeo_title': vimeo_title,
                        'vimeo_description': vimeo_description,
                        'vimeo_name': vimeo_name,
                        'vimeo_date': vimeo_date
                    },
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data, textStatus) {
                        if (data.returnCode == "200") {
                            alert("라이브(메인) 정보가 변경되었습니다..")
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                });
            });


            $("#btn_save_vimeo2").click(function () {
                var vimeo2_id = $("#vimeo2_id").val();
                var vimeo2_url = $("#vimeo2_url").val();
                var vimeo2_title = $("#vimeo2_title").val();
                var vimeo2_description = $("#vimeo2_description").val();
                var vimeo2_name = $("#vimeo2_name").val();
                var vimeo2_date = $("#vimeo2_date").val();
                $.ajax({
                    url: g5_admin_url + "/vellado/ajax.config.php",
                    type: "POST",
                    data: {
                        cmd: 'save_vimeo2_info',
                        'vimeo2_id': vimeo2_id,
                        'vimeo2_url': vimeo2_url,
                        'vimeo2_title': vimeo2_title,
                        'vimeo2_description': vimeo2_description,
                        'vimeo2_name': vimeo2_name,
                        'vimeo2_date': vimeo2_date
                    },
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data, textStatus) {
                        if (data.returnCode == "200") {
                            alert("라이브(서브) 정보가 변경되었습니다..")
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                });
            });

            $('input[name=vimeo_file]').change(function () {
                var data_type = $(this).data("type");

                var data = new FormData();

                var $file_data = $("input[name=vimeo_file][data-type=" + data_type + "]");

                var files = $("input[name=vimeo_file][data-type=" + data_type + "]")[0].files[0];
                data.append('image', files);
                data.append('cmd', 'upload_image');
                data.append('type', $(this).data("type"));

                $.ajax({
                    type: "POST",
                    url: g5_admin_url + "/vellado/ajax.config.php",
                    cache: false,
                    async: false,
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (response) {
                        if (response['returnCode'] != 200) {
                            alert(response['errorMsg']);
                            return false;
                        }

                        if (response.data['thumbnail_img_url']) {
                            $(".image_preview[data-type=" + data_type + "]").html("<img class='vimoe_thumbnail' src='" + response.data['thumbnail_img_url'] + "?t=" + new Date().getTime() + "'/>");
                        }
                    },
                    error: function (response, status, error) {
                        alert("썸네일 이미지 업로드 오류!");
                        return false;
                    }
                });
            });


            $("#btn_guest_allow_chat").click(function (evt) {
                if (confirm("손님 채팅을 허용하시겠습니까?")) {
                    $.ajax({
                        url: g5_admin_url + "/vellado/ajax.config.php",
                        type: "POST",
                        data: {cmd: 'allow_guest_chat'},
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function (data, textStatus) {
                            if (data.returnCode == "200") {
                                $("#status_guest_chat").html("(허용됨)").css({"color": "blue"});
                                alert("손님 채팅이 허용 되었습니다.");
                            } else {
                                alert(data.errorMsg);
                            }
                        }
                    });
                }
            });

            $("#btn_guest_deny_chat").click(function (evt) {
                if (confirm("손님 채팅을 금지하시겠습니까?")) {
                    $.ajax({
                        url: g5_admin_url + "/vellado/ajax.config.php",
                        type: "POST",
                        data: {cmd: 'deny_guest_chat'},
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function (data, textStatus) {
                            if (data.returnCode == "200") {
                                $("#status_guest_chat").html("(금지됨)").css({"color": "red"});
                                alert("손님 채팅이 금지 되었습니다.");
                            } else {
                                alert(data.errorMsg);
                            }
                        }
                    });
                }
            });

            $("#btn_filter_word").click(function (evt) {
                $filter_word = $("#filter_word").val();
                $.ajax({
                    url: g5_admin_url + "/vellado/ajax.config.php",
                    type: "POST",
                    data: {cmd: 'filter_word', filter_word: $filter_word},
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data, textStatus) {
                        if (data.returnCode == "200") {
                            alert("필터링 단어가 재설정되었습니다.")
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                });

            });

            $("#btn_additional_user_count").click(function (evt) {
                var additional_user_count = $("#additional_user_count").val();
                $.ajax({
                    url: g5_admin_url + "/vellado/ajax.config.php",
                    type: "POST",
                    data: {cmd: 'additional_user_count', additional_user_count},
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data, textStatus) {
                        if (data.returnCode == "200") {
                            alert("추가인원이 변경되었습니다.")
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                });

            });

            $("#btn_allow_chat_level").click(function (evt) {
                var allow_chat_level = $("#allow_chat_level").val();
                $.ajax({
                    url: g5_admin_url + "/vellado/ajax.config.php",
                    type: "POST",
                    data: {cmd: 'set_allow_chat_level', allow_chat_level},
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data, textStatus) {
                        if (data.returnCode == "200") {
                            alert("채팅 가능 레벨이 변경되었습니다.")
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                });
            });
        });
    </script>

<?php
?>