<?php
/**
 * Eyoom Admin Skin File
 * @file    ~/theme/basic/skin/member/mail_list.html.php
 */
if (!defined('_EYOOM_IS_ADMIN_')) exit;

add_stylesheet('<link rel="stylesheet" href="' . EYOOM_ADMIN_THEME_URL . '/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">', 0);
add_stylesheet('<link rel="stylesheet" href="' . EYOOM_ADMIN_THEME_URL . '/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">', 0);
?>

<div class="admin-mail-list">
    <form name="fschedule" id="fschedule" method="post" action="<?php echo $action_url1; ?>" onsubmit="return fmember_submit(this);" enctype="multipart/form-data" class="eyoom-form">
        <input type="hidden" name="sst" value="<?php echo $sst; ?>">
        <input type="hidden" name="sod" value="<?php echo $sod; ?>">
        <input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
        <input type="hidden" name="stx" value="<?php echo $stx; ?>">
        <input type="hidden" name="lev" value="<?php echo $lev; ?>">
        <input type="hidden" name="cert" value="<?php echo $cert; ?>">
        <input type="hidden" name="open" value="<?php echo $open; ?>">
        <input type="hidden" name="adt" value="<?php echo $adt; ?>">
        <input type="hidden" name="mail" value="<?php echo $mail; ?>">
        <input type="hidden" name="sms" value="<?php echo $sms; ?>">
        <input type="hidden" name="sdt" value="<?php echo $sdt; ?>">
        <input type="hidden" name="fr_date" value="<?php echo $fr_date; ?>">
        <input type="hidden" name="to_date" value="<?php echo $to_date; ?>">
        <input type="hidden" name="wmode" value="<?php echo $wmode ?>">
        <input type="hidden" name="token" value="">
        <div class="mail-list">
            <div class="adm-headline adm-headline-btn">
                <h3>후원 타임 테이블</h3>
            </div>


            <div class="cont-text-bg">
                <p class="bg-info font-size-12">
                    <i class="fas fa-info-circle"></i> <b>라이브</b>중인 경우 체크 박스를 선택하세요.<br>

                </p>
            </div>
            <div class="margin-bottom-20"></div>

            <?php if (G5_IS_MOBILE) { ?>
                <p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fas fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fas fa-arrows-alt-h"></i>)</p>
            <?php } ?>

            <div id="mail-list"></div>
        </div>

        <?php echo $frm_submit; ?>
    </form>

</div>


<script src="<?php echo EYOOM_ADMIN_THEME_URL; ?>/plugins/jsgrid/jsgrid.min.js"></script>
<script src="<?php echo EYOOM_ADMIN_THEME_URL; ?>/js/jsgrid.js"></script>
<script>
    function eb_modal(href) {
        $('.admin-iframe-modal').modal('show').on('hidden.bs.modal', function () {
            $("#modal-iframe").attr("src", "");
            $('html').css({overflow: ''});
        });
        $('.admin-iframe-modal').modal('show').on('shown.bs.modal', function () {
            $("#modal-iframe").attr("src", href);
            $('#modal-iframe').height(parseInt($(window).height() * 0.85));
            $('html').css({overflow: 'hidden'});
        });
        return false;
    }

    window.closeModal = function () {
        $('.admin-iframe-modal').modal('hide');
    };

    !function () {
        var db = {
            deleteItem: function (deletingClient) {
                var clientIndex = $.inArray(deletingClient, this.clients);
                this.clients.splice(clientIndex, 1)
            },
            insertItem: function (insertingClient) {
                this.clients.push(insertingClient)
            },
            loadData: function (filter) {
                return $.grep(this.clients, function (client) {
                    return !(filter.No && !(client.No.indexOf(filter.No) > -1) || filter.제목 && !(client.제목.indexOf(filter.제목) > -1))
                })
            },
            updateItem: function (updatingClient) {
            }
        };
        window.db = db,
            db.clients = [
                <?php for ($i = 0; $i < count($list); $i++) { ?>
                {
                    번호: "<?php echo $list[$i]['num']; ?>",
                    방송시간: "<?php echo $list[$i]['time']; ?>",
                    스트리머: "<label class='input'><input type='text' name='streamer[<?= $i ?>]' id='streamer<?= $i ?>' value='<?=$list[$i]['streamer']?>'></label>",
                    라이브: "<label class='checkbox'><input type='checkbox' name='live[<?= $i ?>]' id='live_<?= $i ?>' value='1' <?= $list[$i]['live'] ? 'checked' : '' ?>><i></i></label>",
                },
                <?php } ?>
            ]
    }();

    $(function () {
        $("#mail-list").jsGrid({
            filtering: false,
            editing: false,
            sorting: false,
            paging: false,
            autoload: true,
            controller: db,
            deleteConfirm: "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
            pageButtonCount: 5,
            pageSize: 24,
            width: "100%",
            height: "auto",
            fields: [
                {name: "번호", type: "text", align: "center", width: 60},
                {name: "방송시간", type: "text", align: "center", width: 200},
                {name: "스트리머", type: "text", align: "center", width: 200},
                {name: "라이브", type: "text", align: "center", width: 60},
                {name: "-", type: "text", align: "center", width: 500},
            ]
        });

    });
</script>