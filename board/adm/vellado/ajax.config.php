<?php
define('G5_IS_ADMIN', true);
include_once ('../../common.php');
include_once(G5_ADMIN_PATH.'/admin.lib.php');


include_once G5_PATH . '/common.php';
include_once G5_PATH . '/lib/thumbnail.lib.php';

include_once G5_PATH . "/../lib/env.lib.php";
include_once G5_PATH . "/../lib/_dbconnect.php";
include_once G5_PATH . "/../lib/_dbconnect_chat.php";
include_once G5_PATH . "/../lib/SimpleDB.php";


$result = array();
$result["returnCode"] = "200"; //OK
$result["errorMsg"] = "";

$cmd = $_POST['cmd'];
$mb_id = $_POST['mb_id'];



$db = new SimpleDB($pdo_db);
$chatdb = new SimpleDB($pdo_chat_db);

try {

    switch ($cmd) {

        case "change_nick" :
            $nick = filter_input(INPUT_POST, 'nick', FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => '/[가-힣A-Za-z0-9_\-]+/')));

            if ($nick == false) {
                throw new Exception("닉네임은 한글, 영문, 숫자만 가능합니다.(특문불가)");
            } else if (mb_strlen($nick) < 3) {
                throw new Exception("닉네임은 3글자 이상으로 입력하세요.");
            } else if (mb_strlen($nick) > 10) {
                throw new Exception("닉네임은 10글자 이하으로 입력하세요.");
            }

            session_start();
            $_SESSION['ss_mb_nick'] = $nick;
            $_SESSION['chat_username'] = $nick;
            break;

        case "save_streaming_url" :
            $live_streaming_url = filter_input(INPUT_POST, 'live_streaming_url');

            $db->query("UPDATE gzss_config SET live_streaming_url = :live_streaming_url LIMIT 1", compact('live_streaming_url'));


            break;

        case "save_support_link" :
            $support_link = filter_input(INPUT_POST, 'support_link');

            $db->query("UPDATE gzss_config SET support_link = :support_link LIMIT 1", compact('support_link'));

            break;


        case "save_youtube_info" :
            $youtube_id = filter_input(INPUT_POST, 'youtube_id');
            $youtube_title = filter_input(INPUT_POST, 'youtube_title');
            $youtube_description = filter_input(INPUT_POST, 'youtube_description');

            $db->query("UPDATE gzss_config SET 
                       youtube_id = :youtube_id,
                       youtube_title = :youtube_title,
                       youtube_description = :youtube_description,
                       update_datetime = now()
                LIMIT 1",
                compact('youtube_id', 'youtube_title', 'youtube_description')
            );


            break;

        case "save_vimeo_info" :
            $vimeo_id = filter_input(INPUT_POST, 'vimeo_id');
            $vimeo_url = filter_input(INPUT_POST, 'vimeo_url');
            $vimeo_title = filter_input(INPUT_POST, 'vimeo_title');
            $vimeo_description = filter_input(INPUT_POST, 'vimeo_description');
            $vimeo_name = filter_input(INPUT_POST, 'vimeo_name');
            $vimeo_date = filter_input(INPUT_POST, 'vimeo_date');

            $db->query("UPDATE gzss_config SET 
                       vimeo_id = :vimeo_id,
                       vimeo_url = :vimeo_url,
                       vimeo_title = :vimeo_title,
                       vimeo_description = :vimeo_description,
                       vimeo_name = :vimeo_name,        
                       vimeo_date = :vimeo_date,
                       update_datetime = now()
                LIMIT 1",
                compact('vimeo_id', 'vimeo_url', 'vimeo_title', 'vimeo_description', 'vimeo_name', 'vimeo_date')
            );


            break;
        case "save_vimeo2_info" :
            $vimeo2_id = filter_input(INPUT_POST, 'vimeo2_id');
            $vimeo2_url = filter_input(INPUT_POST, 'vimeo2_url');
            $vimeo2_title = filter_input(INPUT_POST, 'vimeo2_title');
            $vimeo2_description = filter_input(INPUT_POST, 'vimeo2_description');
            $vimeo2_name = filter_input(INPUT_POST, 'vimeo2_name');
            $vimeo2_date = filter_input(INPUT_POST, 'vimeo2_date');

            $db->query("UPDATE gzss_config SET 
                       vimeo2_id = :vimeo2_id,
                       vimeo2_url = :vimeo2_url,
                       vimeo2_title = :vimeo2_title,
                       vimeo2_description = :vimeo2_description,
                       vimeo2_name = :vimeo2_name,        
                       vimeo2_date = :vimeo2_date,
                       update_datetime = now()
                LIMIT 1",
                compact('vimeo2_id', 'vimeo2_url', 'vimeo2_title', 'vimeo2_description', 'vimeo2_name', 'vimeo2_date')
            );

            break;

        case "upload_image" :
            // 회원 프로필 이미지 업로드

            if ($_POST['type'] == "image1") {
                $save_filename = "live_thumbnail.png";
            } else if ($_POST['type'] == "image2") {
                $save_filename = "live2_thumbnail.png";
            } else {
                throw new Exception("업로드할 이미지 파일을 선택하세요.", 400);
            }
            if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                $image_regex = "/(\.(gif|jpe?g|png))$/i";
                $upload_tmp_dir = G5_DATA_PATH . '/vimeo/';
                $new_width = 1280;
                $new_height = 720;
                if (preg_match($image_regex, $_FILES['image']['name'])) {
                    $dest_path = $upload_tmp_dir . '/' . $save_filename;
                    $result['dest_path'] = $dest_path;
                    $result['step'] = 1;
                    move_uploaded_file($_FILES['image']['tmp_name'], $dest_path);
                    @chmod($dest_path, G5_FILE_PERMISSION);
                    if (file_exists($dest_path)) {
                        $size = @getimagesize($dest_path);
                        if (!($size[2] === 1 || $size[2] === 2 || $size[2] === 3)) { // gif jpg png 파일이 아니면 올라간 이미지를 삭제한다.
                            @unlink($dest_path);
                        } else if ($size[0] > $new_width || $size[1] > $new_height) {
                            $thumb = null;
                            if ($size[2] === 2 || $size[2] === 3) {
                                //jpg 또는 png 파일 적용
                                $thumb = thumbnail($save_filename, $upload_tmp_dir, $upload_tmp_dir, $new_width, $new_height, true, true);
                                if ($thumb) {
                                    @unlink($dest_path);
                                    rename($upload_tmp_dir . '/' . $thumb, $dest_path);
                                }
                            }
                        }
                    }
                } else {
                    throw new Exception($_FILES['image']['name'] . '은(는) gif/jpg 파일이 아닙니다.', 400);
                }
            } else {
                throw new Exception("업로드할 이미지 파일을 선택하세요.", 400);
            }

            $thumbnail_img_url = "/data/vimeo/" . $save_filename;

            $data = [
                'thumbnail_img_url' => $thumbnail_img_url
            ];

            $result['data'] = $data;

            $db->query("UPDATE gzss_config SET update_datetime = now() ");

            break;


        case "red_card" :

            $redis = new Redis();
            $redis->connect("127.0.0.1", 6379, 2.5, NULL, 150);
            $redis->select(0);


            $block_key = "USER:RED_CARD:" . $mb_id;

            if ($redis->exists($block_key)) {
                $ttl = $redis->ttl($block_key);
                $result['status_msg'] = "차단됨 [해제까지 남은시간:{$ttl}]초";
                throw new Exception("이미 레드카드를 받은 상태입니다.", 403);
            }
            $redis->setex($block_key, 86400, $mb_id);
            $ttl = $redis->ttl($block_key);
            $result['status_msg'] = "차단됨 [해제까지 남은시간:{$ttl}]초";

            $mb = get_member($mb_id, "mb_nick");

            //send_chat_message("public", $mb_id, "RED_CARD", "{$mb['mb_nick']} 님이 레드카드를 받으셨습니다.");

            break;

        case "cancel_red_card" :

            $redis = new Redis();
            $redis->connect("127.0.0.1", 6379, 2.5, NULL, 150);
            $redis->select(0);

            $block_key = "USER:RED_CARD:" . $mb_id;

            if (!$redis->exists($block_key)) {
                throw new Exception("레드카드를 받은 상태가 아니거나 해제된 상태입니다.", 403);
            }

            $redis->delete($block_key);

            $mb = get_member($mb_id, "mb_nick");

            //send_chat_message("private", $mb_id, "CANCEL_RED_CARD", "{$mb['mb_nick']} 님의 레드카드를 해제하였습니다.");

            break;

        case "block" :

            $mb_id = filter_input(INPUT_POST, 'mb_id');


            $redis = new Redis();
            $redis->connect("127.0.0.1", 6379, 2.5, NULL, 150);
            $redis->select(3);


            $block_key = "CHAT:BLOCK_USER:" . $mb_id;

            if ($redis->exists($block_key)) {
                $ttl = $redis->ttl($block_key);
                $result['status_msg'] = "차단됨 [해제까지 남은시간:{$ttl}]초";
                throw new Exception("이미 차단된 상태입니다.", 403);
            }
            $redis->setex($block_key, 86400, $mb_id);
            $ttl = $redis->ttl($block_key);
            $result['status_msg'] = "차단됨 [해제까지 남은시간:{$ttl}]초";


            break;

        case "unblock" :

            $no = filter_input(INPUT_POST, 'no');

            $redis = new Redis();
            $redis->connect("127.0.0.1", 6379, 2.5, NULL, 150);
            $redis->select(3);

            $penalty = $chatdb->row("select * from pchat_penalty_user where no=:no", ['no' => $no]);

            if (!$penalty) {
                throw new Exception("차단 관련 정보가 없습니다.", 400);
            }

            $block_key = "CHAT:BLOCK_USER:" . $penalty['userid'];

            if (!$redis->exists($block_key)) {
                throw new Exception("차단 상태가 아니거나  이미 해제된 상태입니다.", 403);
            }

            $redis->del($block_key);

            $chatdb->query("update  pchat_penalty_user set release_datetime = now() where no=:no", ['no' => $no]);

            break;

        case "allow_guest_chat" :

            //send_chat_message("public", "@all", "ALLOW_GUEST_CHAT", "손님 채팅을 허용합니다.");

            $sql = "UPDATE  pchat_channel_info SET is_guest_allow_chat = 'Y' WHERE channel_id='ALL' ";
            $chatdb->query($sql);
            break;

        case "deny_guest_chat" :

            //send_chat_message("public", "@all", "DENY_GUEST_CHAT", "손님 채팅을 금지합니다.");

            $sql = "UPDATE  pchat_channel_info SET is_guest_allow_chat = 'N' WHERE channel_id='ALL' ";
            $chatdb->query($sql);
            break;

        case "filter_word" :
            $filter_word = $_POST['filter_word'];

            //todo message type 은 내부 시스템간의 호출임으로 system으로 하는게 맞는것 같음
            //send_chat_message("public", "@all", "SET_FILTER_WORD", $filter_word);

            $sql = "UPDATE pchat_config SET filter_word = :filter_word ";
            $chatdb->query($sql, compact('filter_word'));
            break;

        case "additional_user_count" :

            $additional_user_count = filter_input(INPUT_POST, 'additional_user_count', FILTER_VALIDATE_INT);

            $data = [
                "additional_user_count" => $additional_user_count . "",
                "channel_id" => "ALL"
            ];
            //send_chat_message("public", "@all", "ADDITIONAL_USER_COUNT", "인원수 변경", $data);

            $sql = "UPDATE pchat_channel_info SET additional_user_count = :additional_user_count WHERE channel_id='ALL' ";
            $chatdb->query($sql, compact('additional_user_count'));
            break;

        case "set_allow_chat_level" :
            $allow_chat_level = filter_input(INPUT_POST, 'allow_chat_level', FILTER_VALIDATE_INT);

            $data = [
                "allow_chat_level" => $allow_chat_level . "",
            ];

            //send_chat_message("public", "@all", "SET_CHAT_LEVEL", "채팅 레벨 변경", $data);

            $sql = "UPDATE pchat_channel_info SET allow_chat_level = :allow_chat_level WHERE channel_id='ALL' ";

            $chatdb->query($sql, compact('allow_chat_level'));

            break;


        case "get_vbox_list" :
            //todo 노출시간이 남은 슈퍼챗 목록을 조회한다.

            $before_day = date("Y-m-d", strtotime("-1 day", time()));
            $today = date("Y-m-d");

            $sql = "SELECT * 
                    FROM pchat_donation 
                    WHERE create_datetime BETWEEN '{$before_day} 00:00:00' AND '{$today} 23:59:59'
                      AND is_delete = 'N'
                    
            ";

            $rows = $chatdb->query($sql);

            $list = [];

            foreach($rows as $row) {
                //todo view_second 가 남은지 확인하여 남은시간을 넣는다.

                $start_time = strtotime($row['create_datetime']);
                $now_time = time();

                $remain_second = ($start_time + $row['view_second']) - $now_time;
                if($remain_second > 0) { //현재 시간을 기준으로 표시할 시간이 남았다면,
                    $list[] = [
                        'userid' => $row['userid'],
                        'username' => $row['username'],
                        'profile_img' => $row['profile_img'],
                        'amount' => intval($row['amount']),
                        'message' => $row['message'],
                        'view_second' => intval($remain_second),
                    ];
                }

            }

            $result['data'] = $list;

            break;


        case "delete_vbox" :


        default :
            throw new Exception("호출오류", 500);

    }
} catch (Exception $ex) {
    $result['returnCode'] = $ex->getCode() . "";
    $result["errorMsg"] = $ex->getMessage();
}
echo json_encode($result);
?>