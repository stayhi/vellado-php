<?php
$menu['menu800'] = array (
    array('800000', 'VELLADO', ''.G5_ADMIN_URL.'/vellado/config.php', 'vellado'),
    array('800100', '스트리밍 설정', ''.G5_ADMIN_URL.'/vellado/config.php', 'vellado'),
    array('800200', '회원목록', ''.G5_ADMIN_URL.'/vellado/member_list.php', 'vellado'),
    array('800300', '채팅목록', ''.G5_ADMIN_URL.'/vellado/chat_list.php', 'vellado'),
    array('800400', '결제목록', ''.G5_ADMIN_URL.'/vellado/payment_list.php', 'vellado'),
    array('800500', '채팅차단내역', ''.G5_ADMIN_URL.'/vellado/penalty_list.php', 'vellado'),
    array('800600', '후원 타임테이블', ''.G5_ADMIN_URL.'/vellado/schedule.php', 'vellado'),
    array('800700', '컨텐츠 내용관리', ''.G5_ADMIN_URL.'/vellado/contentlist.php', 'vellado'),
    array('800800', '공지사항', ''.G5_ADMIN_URL.'/vellado/notice.php', 'vellado'),
);
?>