<?php
$sub_menu = "800100";
include_once("./_common.php");

ini_set('display_errors', 1);
echo "test...";
exit;


include_once G5_PATH . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/_dbconnect_chat.php";
include_once __DIR__ . "/lib/SimpleDB.php";

auth_check($auth[$sub_menu], 'r');

if ($is_admin != 'super') {
    alert('최고관리자만 접근 가능합니다.');
}

$db = new SimpleDB($pdo_db);

$chatdb = new SimpleDB($pdo_chat_db);


$chat_config = $chatdb->row("SELECT * FROM pchat_config ");
$channel_config = $chatdb->row("SELECT * FROM pchat_channel_info WHERE channel_id='ALL' ");
$channel_config['guest_allow_chat'] = $channel_config['is_guest_allow_chat'] == "N" ? "(금지됨)" : "(허용됨)";

$chat_user_count = $chatdb->single("SELECT cur_user FROM pchat_channel_info WHERE channel_id='ALL' ");

$gzss_config = $db->row("SELECT * FROM gzss_config LIMIT 1");


$path1 = G5_DATA_PATH . "/vimeo/live_thumbnail.png";
$path2 = G5_DATA_PATH . "/vimeo/live2_thumbnail.png";

if (file_exists($path1)) {
    $gzss_config['thumbnail_image'] = "<img class='vimeo_thumbnail' src='/data/vimeo/live_thumbnail.png?t=" . time() . "'/>";
}

if (file_exists($path2)) {
    $gzss_config['thumbnail2_image'] = "<img class='vimeo2_thumbnail' src='/data/vimeo/live2_thumbnail.png?t=" . time() . "'/>";
}

$g5['title'] = '환경설정-채팅';
include_once('../admin.head.php');

$pg_anchor = '<ul class="anchor">
    <li><a href="#anc_cf_basic">기본환경</a></li>
    <li><a href="#anc_cf_vimeo">라이브설정1</a></li>
    <li><a href="#anc_cf_vimeo2">라이브설정2</a></li>    
    <li><a href="#anc_cf_chat">채팅설정</a></li>
</ul>';
?>

<style>
    .input_right {
        text-align: right;
        padding-right: 5px;
    }

    .image_preview img {
        width: 495px;
        height: 278px;
    }
</style>

<form name="fconfigform" id="fconfigform" method="post" onsubmit="return fconfigform_submit(this);">
    <input type="hidden" name="token" value="" id="token">

    <section id="anc_cf_vimeo">
        <h2 class="h2_frm">동영상 설정1</h2>
        <?php echo $pg_anchor ?>

        <div class="tbl_frm01 tbl_wrap">

            <table>
                <caption></caption>
                <colgroup>
                    <col class="grid_4">
                    <col>
                    <col class="grid_4">
                    <col>
                </colgroup>
                <tbody>
                <tr class="row">
                    <th class="col">비메오 라이브 아이디</th>
                    <td class="col">
                        <input class="frm_input" name="vimeo_id" id="vimeo_id"
                               value="<?= $gzss_config['vimeo_id'] ?>">
                    </td>
                </tr>

                <tr class="row">
                    <th class="col">비메오 라이브 url</th>
                    <td class="col">
                        <input class="frm_input" name="vimeo_url" id="vimeo_url"
                               value="<?= $gzss_config['vimeo_url'] ?>" size="80">
                    </td>
                </tr>


                <tr class="row">
                    <th class="col">비메오 라이브 닉네임</th>
                    <td class="col"><input class="frm_input" type="text" name="vimeo_name" id="vimeo_name"
                                           value="<?= $gzss_config['vimeo_name'] ?>"/></td>
                </tr>
                <tr class="row">
                    <th class="col">비메오 라이브 날짜</th>
                    <td class="col"><input class="frm_input" type="text" name="vimeo_date" id="vimeo_date"
                                           value="<?= $gzss_config['vimeo_date'] ?>"/></td>
                </tr>

                <tr class="row">
                    <th class="col">비메오 라이브 타이틀</th>
                    <td class="col"><input class="frm_input" type="text" name="vimeo_title" id="vimeo_title"
                                           value="<?= $gzss_config['vimeo_title'] ?>" size="80"/></td>
                </tr>

                <tr class="row">
                    <th class="col">비메오 라이브 설명</th>
                    <td class="col">
                        <textarea class="frm_input" name="vimeo_description" id="vimeo_description"
                                  rows="3"><?= $gzss_config['vimeo_description'] ?></textarea>
                    </td>
                </tr>

                <tr class="row">
                    <th class="col">비메오 썸네일</th>
                    <td class="col">
                        <div class="image_preview" data-type="image1">
                            <?= $gzss_config['thumbnail_image'] ?>
                        </div>
                        <input type="file" name="vimeo_file" data-type="image1" class="frm_input"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <input id="btn_save_vimeo" type="button" value="vimeo 라이브 설정 저장" class="btn_submit btn"
                               accesskey="s">
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </section>

    <section id="anc_cf_vimeo2">
        <h2 class="h2_frm">동영상 설정2</h2>
        <?php echo $pg_anchor ?>

        <div class="tbl_frm01 tbl_wrap">

            <table>
                <caption></caption>
                <colgroup>
                    <col class="grid_4">
                    <col>
                    <col class="grid_4">
                    <col>
                </colgroup>
                <tbody>
                <tr class="row">
                    <th class="col">비메오2 라이브 아이디</th>
                    <td class="col">
                        <input class="frm_input" name="vimeo2_id" id="vimeo2_id"
                               value="<?= $gzss_config['vimeo2_id'] ?>">
                    </td>
                </tr>

                <tr class="row">
                    <th class="col">비메오2 라이브 url</th>
                    <td class="col">
                        <input class="frm_input" name="vimeo2_url" id="vimeo2_url"
                               value="<?= $gzss_config['vimeo2_url'] ?>" size="80">
                    </td>
                </tr>


                <tr class="row">
                    <th class="col">비메오2 라이브 닉네임</th>
                    <td class="col"><input class="frm_input" type="text" name="vimeo2_name" id="vimeo2_name"
                                           value="<?= $gzss_config['vimeo2_name'] ?>"/></td>
                </tr>
                <tr class="row">
                    <th class="col">비메오2 라이브 날짜</th>
                    <td class="col"><input class="frm_input" type="text" name="vimeo2_date" id="vimeo2_date"
                                           value="<?= $gzss_config['vimeo2_date'] ?>"/></td>
                </tr>
                <tr class="row">
                    <th class="col">비메오2 라이브 타이틀</th>
                    <td class="col"><input class="frm_input" type="text" name="vimeo2_title" id="vimeo2_title"
                                           value="<?= $gzss_config['vimeo2_title'] ?>" size="80"/></td>
                </tr>
                <tr class="row">
                    <th class="col">비메오2 라이브 설명</th>
                    <td class="col">
                        <textarea class="frm_input" name="vimeo2_description" id="vimeo2_description"
                                  rows="3"><?= $gzss_config['vimeo2_description'] ?></textarea>
                    </td>
                </tr>

                <tr class="row">
                    <th class="col">비메오2 썸네일</th>
                    <td class="col">

                        <div class="image_preview" data-type="image2">
                            <?= $gzss_config['thumbnail2_image'] ?>
                        </div>
                        <input type="file" name="vimeo_file" data-type="image2" class="frm_input"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <input id="btn_save_vimeo2" type="button" value="vimeo 라이브(2) 설정 저장" class="btn_submit btn"
                               accesskey="s">
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </section>

    <section id="anc_cf_chat">
        <h2 class="h2_frm">채팅 기본환경 설정(작업중 - 미동작)</h2>
        <?= $pg_anchor ?>

        <div class="tbl_frm01 tbl_wrap">
            <table>
                <caption>채팅 기본환경 설정(작업중 - 미동작)</caption>
                <colgroup>
                    <col class="grid_4">
                    <col>
                    <col class="grid_4">
                    <col>
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="cf_title">현재 접속자<strong class="sound_only"></strong></label></th>
                    <td colspan="3"><?= $chat_user_count ?></td>
                </tr>
                <tr>
                    <th scope="row">공개채팅 손님 채팅 허용</th>
                    <td colspan="3">
                        <input type="button" id="btn_guest_allow_chat" value="손님 채팅 허용"/>
                        <input type="button" id="btn_guest_deny_chat" value="손님 채팅 금지"/>
                        <span id="status_guest_chat"><?= $channel_config['guest_allow_chat'] ?></span>
                    </td>
                </tr>

                <tr>
                    <th scope="row">공개채팅 채팅 레벨 설정</th>
                    <td colspan="3">
                        <?= help('채팅 입력이 가능한 레벨을 설정할수 있습니다. 채팅 레벨은 회원 레벨과 다릅니다.') ?>
                        <input type="text" name="allow_chat_level" id="allow_chat_level" class="frm_input input_right"
                               value="<?= intval($chat_config['allow_chat_level']) ?>">
                        <input type="button" id="btn_allow_chat_level" value="변경"/>
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="filter_word">추가인원<strong class="sound_only"></strong></label></th>
                    <td colspan="3">
                        <?= help('채팅에서 표시되는 인원수가 해당 수치와 합산되어 표시됩니다.') ?>
                        <input type="text" name="additional_user_count" id="additional_user_count"
                               class="frm_input input_right"
                               value="<?= intval($channel_config['additional_user_count']) ?>">
                        <input type="button" id="btn_additional_user_count" value="변경"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="filter_word">단어 필터링<strong class="sound_only"></strong></label></th>
                    <td colspan="3">
                        <?= help('채팅에서 필터링 할 단어를 지정합니다. 단어에 포함된 특수문자는 제거후 체크함으로 입력하지 않아도 됩니다. 단어 구분은 콤마입니다.') ?>
                        <textarea name="filter_word" id="filter_word" class="frm_input"
                                  row="3"><?= $chat_config['filter_word'] ?></textarea>
                        <input type="button" id="btn_filter_word" value="변경"/>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </section>

    <div class="btn_fixed_top btn_confirm">
        <input type="submit" value="확인" class="btn_submit btn" accesskey="s">
    </div>

</form>

<script>
    $(function () {

        $("#btn_save_vimeo").click(function () {
            var vimeo_id = $("#vimeo_id").val();
            var vimeo_url = $("#vimeo_url").val();
            var vimeo_title = $("#vimeo_title").val();
            var vimeo_description = $("#vimeo_description").val();
            var vimeo_name = $("#vimeo_name").val();
            var vimeo_date = $("#vimeo_date").val();
            $.ajax({
                url: "/ajax.chat.php",
                type: "POST",
                data: {
                    cmd: 'save_vimeo_info',
                    'vimeo_id': vimeo_id,
                    'vimeo_url': vimeo_url,
                    'vimeo_title': vimeo_title,
                    'vimeo_description': vimeo_description,
                    'vimeo_name': vimeo_name,
                    'vimeo_date': vimeo_date
                },
                dataType: "json",
                async: true,
                cache: false,
                success: function (data, textStatus) {
                    if (data.returnCode == "200") {
                        alert("비메오 라이브 정보가 변경되었습니다..")
                    } else {
                        alert(data.errorMsg);
                    }
                }
            });
        });

        $("#btn_save_vimeo2").click(function () {
            var vimeo2_id = $("#vimeo2_id").val();
            var vimeo2_url = $("#vimeo2_url").val();
            var vimeo2_title = $("#vimeo2_title").val();
            var vimeo2_description = $("#vimeo2_description").val();
            var vimeo2_name = $("#vimeo2_name").val();
            var vimeo2_date = $("#vimeo2_date").val();
            $.ajax({
                url: "/ajax.chat.php",
                type: "POST",
                data: {
                    cmd: 'save_vimeo2_info',
                    'vimeo2_id': vimeo2_id,
                    'vimeo2_url': vimeo2_url,
                    'vimeo2_title': vimeo2_title,
                    'vimeo2_description': vimeo2_description,
                    'vimeo2_name': vimeo2_name,
                    'vimeo2_date': vimeo2_date
                },
                dataType: "json",
                async: true,
                cache: false,
                success: function (data, textStatus) {
                    if (data.returnCode == "200") {
                        alert("비메오 라이브(2) 정보가 변경되었습니다..")
                    } else {
                        alert(data.errorMsg);
                    }
                }
            });
        });

        $('input[name=vimeo_file]').change(function () {
            var data_type = $(this).data("type");

            var data = new FormData();

            var $file_data = $("input[name=vimeo_file][data-type=" +data_type + "]");

            var files = $("input[name=vimeo_file][data-type=" +data_type + "]")[0].files[0];
            data.append('image', files);
            data.append('cmd', 'upload_image');
            data.append('type', $(this).data("type"));

            $.ajax({
                type: "POST",
                url: "/ajax.chat.php",
                cache: false,
                async: false,
                enctype: 'multipart/form-data',
                data: data,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (response) {
                    if (response['returnCode'] != 200) {
                        alert(response['errorMsg']);
                        return false;
                    }

                    if (response.data['thumbnail_img_url']) {
                        $(".image_preview[data-type=" + data_type + "]").html("<img class='vimoe_thumbnail' src='" + response.data['thumbnail_img_url'] + "?t=" + new Date().getTime() + "'/>");
                    }
                },
                error: function (response, status, error) {
                    alert("썸네일 이미지 업로드 오류!");
                    return false;
                }
            });
        });


        $("#btn_guest_allow_chat").click(function (evt) {
            if (confirm("손님 채팅을 허용하시겠습니까?")) {
                $.ajax({
                    url: "/ajax.chat.php",
                    type: "POST",
                    data: {cmd: 'allow_guest_chat'},
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data, textStatus) {
                        if (data.returnCode == "200") {
                            $("#status_guest_chat").html("(허용됨)").css({"color": "blue"});
                            alert("손님 채팅이 허용 되었습니다.");
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                });
            }
        });

        $("#btn_guest_deny_chat").click(function (evt) {
            if (confirm("손님 채팅을 금지하시겠습니까?")) {
                $.ajax({
                    url: "/ajax.chat.php",
                    type: "POST",
                    data: {cmd: 'deny_guest_chat'},
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data, textStatus) {
                        if (data.returnCode == "200") {
                            $("#status_guest_chat").html("(금지됨)").css({"color": "red"});
                            alert("손님 채팅이 금지 되었습니다.");
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                });
            }
        });

        $("#btn_filter_word").click(function (evt) {
            $filter_word = $("#filter_word").val();
            $.ajax({
                url: "/ajax.chat.php",
                type: "POST",
                data: {cmd: 'filter_word', filter_word: $filter_word},
                dataType: "json",
                async: true,
                cache: false,
                success: function (data, textStatus) {
                    if (data.returnCode == "200") {
                        alert("필터링 단어가 재설정되었습니다.")
                    } else {
                        alert(data.errorMsg);
                    }
                }
            });

        });

        $("#btn_additional_user_count").click(function (evt) {
            var additional_user_count = $("#additional_user_count").val();
            $.ajax({
                url: "/ajax.chat.php",
                type: "POST",
                data: {cmd: 'additional_user_count', additional_user_count},
                dataType: "json",
                async: true,
                cache: false,
                success: function (data, textStatus) {
                    if (data.returnCode == "200") {
                        alert("추가인원이 변경되었습니다.")
                    } else {
                        alert(data.errorMsg);
                    }
                }
            });

        });

        $("#btn_allow_chat_level").click(function (evt) {
            var allow_chat_level = $("#allow_chat_level").val();
            $.ajax({
                url: "/ajax.chat.php",
                type: "POST",
                data: {cmd: 'set_allow_chat_level', allow_chat_level},
                dataType: "json",
                async: true,
                cache: false,
                success: function (data, textStatus) {
                    if (data.returnCode == "200") {
                        alert("채팅 가능 레벨이 변경되었습니다.")
                    } else {
                        alert(data.errorMsg);
                    }
                }
            });

        });

    });
</script>
<?php
include_once('../admin.tail.php');
?>
