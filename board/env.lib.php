<?php
error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING );


function is_local_development() {
    if(is_production()) return false;
    if(is_beta()) return false;
    if(getenv("local_dev") == "true") {
        return true;
    }
    return false;
}

/**
 * 개발서버인지 확인한다.
 * 베타 서버도 범주에선 개발서버임으로,
 * 개발서버와 베타서버를 확인해야 하는 경우에는 베타 서버인지(is_beta)를 먼저 확인해야 함.
 * @return bool
 */
function is_development() {
    $host = gethostname();
    if(is_production()) return false;
    if(is_beta()) return false;
    if ($host === 'dev') {
        return true;
    }
    return false;
}

/**
 * 베타 서버인지 확인
 *
 * @return bool
 */
function is_beta() {
    if (strtolower($_SERVER['HTTP_HOST']) === "beta") {
        return true;
    }
    return false;
}

/**
 * 운영서버인지 확인
 * @return bool
 */
function is_production() {
    if ($_SERVER['REMOTE_HOST'] == "www.") { //live로 시작하는것만 프로덕션 서비스
       return true;
    }
    return false;
}

/**
 * 사무실 아이피인지 확인
 * @return bool
 */
function is_office_ip() {
    $addr = $_SERVER['REMOTE_ADDR'];
    switch (true) {

        case (strpos($addr, "192.168.100.") !== false) : // 개발서버

        case ($addr === "127.0.0.1") :
            return true;
            break;
        default :
            return false;
            break;
    }
}

//------------------------------------------------------------------------------
// 서비스 인스턴스 환경 상수 정의
//------------------------------------------------------------------------------
if (is_production()) { // 베타 서버
    define('ENVIRONMENT', 'PRODUCTION');
} else if (is_beta()) { // 개발 서버
    define('ENVIRONMENT', 'BETA');
} else if(is_local_development()) {
    define('ENVIRONMENT', 'LOCAL');
} else {
    define('ENVIRONMENT', 'DEV');
}
?>
