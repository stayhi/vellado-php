<?php
include_once('../common.php');

if(true) {
    header("HTTP/1.0 404 Not Found");
    exit;
}
if(isset($default['de_shop_layout_use']) && $default['de_shop_layout_use']) {
    if (!defined('G5_USE_SHOP') || !G5_USE_SHOP)
        die('<p>쇼핑몰 설치 후 이용해 주십시오.</p>');

    define('_SHOP_', true);
}