<?php

include_once __DIR__."/env.lib.php";

if (is_development() == true) {
    $db_host = "localhost";
    $db_user = "sol";
    $db_password = "sol@!#$%";
    $db_name = "demo1";
} else if (is_beta() == true) {
    $db_host = "localhost";
    $db_user = "sol";
    $db_password = "sol@!#$%";
    $db_name = "demo1";
} else { //운영서비스 정보
    $db_host = "localhost";
    $db_user = "sol";
    $db_password = "sol@!#$%";
    $db_name = "demo1";
}

//docker 같은 로컬 개발환경인 경우..
if (is_local_development() == true) {
    $db_host = "solgnu.com";
    $db_user = "sol";
    $db_password = "sol@!#$%";
    $db_name = "demo1";

}

$pdo_db = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
$pdo_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo_db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

unset($db_host);
unset($db_user);
unset($db_password);
unset($db_name);
?>