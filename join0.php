<?php


$google_client_id = "374306251607-k72tprctfcqm4id22pttreiehmof0qi7.apps.googleusercontent.com";

$google_client_security = "XkDAc4y4xtLRGC4jwtixya_5";

// 주요 값들.
$redirection_url = "https://vellado.com/join";
$scope = "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.profile.emails.read";

// 인증 코드 주소 생성.
$reqAddr = "https://accounts.google.com/o/oauth2/v2/auth"
    . "?client_id=" . $google_client_id
    . "&redirect_uri=" . urlencode($redirection_url)
    . "&scope=" . urlencode($scope)
    . "&state=OK"
    . "&access_type=offline"
    . "&include_granted_scopes=true"
    . "&response_type=code";

// 지정한 주소로 리디렉션 시키기.
header('Location: ' . $reqAddr);
