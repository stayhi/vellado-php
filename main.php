<?php

include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";
include_once __DIR__ . "/board/common.php";

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL ^ E_NOTICE);

$db = new SimpleDB($pdo_db);

$video = $db->row("SELECT * FROM vimeo_video_list ORDER BY id DESC LIMIT 1");

$config = $db->row("SELECT * FROM gzss_config LIMIT 1");

$config['vimeo_thumbnail_image'] = "https://i.vimeocdn.com/video/1006597909.webp?mw=1600&mh=901&q=70";


$path1 = G5_DATA_PATH . "/vimeo/live_thumbnail.png";
$path2 = G5_DATA_PATH . "/vimeo/live2_thumbnail.png";

if (file_exists($path1)) {
    $config['vimeo_thumbnail_image'] = "<img class='vimeo_thumbnail' src='/data/vimeo/live_thumbnail.png?t=" . strtotime($config['update_datetime']) . "'/>";
}

if (file_exists($path2)) {
    $config['vimeo2_thumbnail_image'] = "<img class='vimeo2_thumbnail' src='/data/vimeo/live2_thumbnail.png?t=" . strtotime($config['update_datetime']) . "'/>";
}

$live_streaming_url = $config['live_streaming_url'];

$youtube_id = $config['youtube_id'];
$is_live = trim($config['youtube_id']) ? true : false;

$main = [];

if ($is_live) {
    $main['thumbnail_image'] = "https://img.youtube.com/vi/{$youtube_id}/hqdefault.jpg";
    $main['title'] = $config['youtube_title'];
    $main['description'] = $config['youtube_description'];
    $main['date'] = date('Y.m.d');
} else {
    $main['thumbnail_image'] = $video['thumbnail3'];
    $main['title'] = $video['title'];
    $main['description'] = $video['description'];
    $main['date'] = date('Y.m.d', strtotime($video['release_datetime']));
}

//메인 하단 동영상 목록
$rows = $db->query("SELECT * FROM vimeo_video_list ORDER BY id DESC LIMIT 0, 12");
$list = [];

foreach ($rows as $row) {
    $list[] = [
        'id' => $row['id'],
        'title' => $row['title'],
        'description' => $row['description'],
        'release_datetime' => $row['release_datetime'],
        'datetime' => date('Y.m.d', strtotime($row['release_datetime'])),
        'link' => $row['link'],
        'thumbnail' => $row['thumbnail2'],
    ];
}
?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/resources/css/common.css">
    <script src="resources/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#aa15ff">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <meta name="naver-site-verification" content="f47bc1e19f2ef9f8ec34e16e88a11c1f99840b73" />
    <title>VELLADO</title>

</head>
<body oncontextmenu='return false' onselectstart='return false' ondragstart='return false' >

<?php include __DIR__ . "/top_menu.inc.php"; ?>
<!-- 메인 컨텐츠 -->
<div class="main_container">
    <div class="main_content_div">
        <!-- 상단 라이브 썸네일-->


        <!--div class="main_upper_content_left"><img src="resources/images/thumb-1.jpg"></div>
        <div class="main_upper_content_right">
            <div>
                <div class="title">8.15 광복과 함께 채널 독립!</div>
                <div class="date">2020.03.21</div>
                <div class="body">오래 기다리셨습니다. 드디어 돌아왔습니다. 자축합시다!
                    동해물과 백두산이 마르고 닳도록 하느님이 보우하사 우리나라만세 무궁화 삼천리
                    화려강산! 대한사람 대한으로 길이 보전하세
                </div>
            </div>

            <div class="box">
                <div class="count">12,345명 시청중</div>
                <div class="onair">ON AIR</div>
            </div>
        </div-->
        <div class="main_thumb_container">
            <div class="main_box">
                <div class="main_bar">
                    <div class="bar_left">
                        GZSS TV
                    </div>
                    <div class="bar_right">
                        <a href="/live">LIVE NOW</a>
                    </div>
                </div>
                <div class="main_thumbnail">
                    <a href="/live?type=1"><?=$config['vimeo_thumbnail_image']?></a>
                </div>
                <div class="title"><?=$config['vimeo_title']?></div>
                <div class="nick_box">
                    <div class="nick"><?=$config['vimeo_name']?></div>
                    <div class="date"><?=$config['vimeo_date']?></div>
                </div>

                <div class="body"><?=$config['vimeo_description']?>
                </div>
            </div>
            <div class="main_box">
                <div class="main_bar">
                    <div class="bar_left">
                        벨라 TV
                    </div>
                    <div class="bar_right purple">
                    <a href="/live?type=2">RE-AIRING</a>
                    </div>
                </div>
                <div class="main_thumbnail">
                    <a href="/live?type=2"><?=$config['vimeo2_thumbnail_image']?></a>
                </div>
                <div class="title"><?=$config['vimeo2_title']?></div>
                <div class="nick_box">
                    <div class="nick"><?=$config['vimeo2_name']?></div>
                    <div class="date"><?=$config['vimeo2_date']?></div>
                </div>

                <div class="body"><?=$config['vimeo2_description']?>
                </div>
            </div>
        </div>

        <!-- 공간 60px -->
        <div class="spacer_60"></div>
        <!-- 메뉴 -->
        <?php include __DIR__ . "/menu.inc.php" ?>

        <!--검색 박스-->
        <!-- VOD 썸네일 리스트 -->
        <div class="vod_list_container">
            <div>
            <img class="vod_list_img_temp" src="resources/images/under-desk.png" style="object-fit: contain;">
            </div>
                <?php /* ?>
            <div class="vod_list_box" id="vod_list_box">
                <?php foreach ($list as $index => $row) : ?>
                    <div class="vod_thumb_box">
                        <div class="thumbnail"><a href="./vod/<?= $row['id'] ?>" rel="<?= $row['id'] ?>"><img src="<?= $row['thumbnail'] ?>"></a></div>
                        <div class="title"><a href="./vod/<?= $row['id'] ?>" rel="<?= $row['id'] ?>"><?= $row['title'] ?></a></div>
                        <div class="body"></div>
                        <div class="date_box">
                            <div class="date"><?= $row['datetime'] ?></div>
                            <div class="count">- <img src="resources/images/icon-view.png"></div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
            <?php */ ?>
        </div>
    </div>
</div>





<?php include_once __DIR__."/footer.php";?>
