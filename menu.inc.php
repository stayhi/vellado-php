<?php
    $page_name = basename($_SERVER['SCRIPT_FILENAME']);

?>
<div class="main_btn_container">
    <div class="btn <?=$page_name == "vod.php" ? "active" : "" ?>">
        <div class="text"><a href="/vod">영상</a></div>
        <a href="/vod"><img class="icon1" src="/resources/images/btn-video_off.png"></a>
    </div>
    <div class="btn <?=$page_name == "notice.php" ? "active" : "" ?>">
        <div class="text"><a href="/notice">공지</a></div>
        <a href="/notice"><img class="icon2" src="/resources/images/btn-noti_off.png"></a>
    </div>
    <div class="btn <?=$page_name == "support.php" ? "active" : "" ?>">
        <div class="text"><a href="/support">정기구독</a></div>
        <a href="/support"><img class="icon3" src="/resources/images/btn-spon_off.png"></a>
    </div>
</div>

<!-- 메뉴 모바일 -->
<div class="main_btn_container_m">
    <div class="btn <?=$page_name == "vod.php" ? "active" : "" ?>">
        <a href="/vod"><img class="icon1" src="/resources/images/btn-video_off.png"></a>
    </div>
    <div class="btn <?=$page_name == "notice.php" ? "active" : "" ?>">
        <a href="/notice"><img class="icon2" src="/resources/images/btn-noti_off.png"></a>
    </div>
    <div class="btn <?=$page_name == "support.php" ? "active" : "" ?>">
        <a href="/support"><img class="icon3" src="/resources/images/btn-spon_off.png"></a>
    </div>
</div>
