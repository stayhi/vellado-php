<?php

use App\SimpleDB;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/lib/env.lib.php';
require __DIR__ . '/board/common.php';

if(!isset($pdo_db)) {
    include_once(__DIR__ . "/lib/_dbconnect_gzss.php");
}


$db = new SimpleDB($pdo_db);


$support = $db->row("select * from g5_content where co_id='support' limit 1");

?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#aa15ff">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta name="naver-site-verification" content="f47bc1e19f2ef9f8ec34e16e88a11c1f99840b73" />
    <title>VELLADO</title>
    <link rel="stylesheet" type="text/css" href="resources/css/common.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>
<body oncontextmenu='return false' onselectstart='return false' ondragstart='return false' >

<?php include __DIR__ . "/top_menu.inc.php"; ?>
<!-- 메인 컨텐츠 -->
<div class="main_container">


    <div class="main_content_div">
        <!-- 메뉴 -->
        <?php include __DIR__."/menu.inc.php"?>


        <div class="support_container">
            <?=$support['co_content']?>
        </div>


    </div>

</div>

<script>
    window.addEventListener("load", function () {
        document.querySelector(".main_banner_logo").addEventListener("click", function (e) {
            location.href='/main'
        }, false);

    }, false);

</script>

<?php include_once __DIR__."/footer.php";?>
