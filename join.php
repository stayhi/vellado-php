<?php

include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";


ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL ^ E_NOTICE);

if(!isset($pdo_db)) {
    include_once(__DIR__ . "/lib/_dbconnect_gzss.php");
}


$db = new SimpleDB($pdo_db);

session_start();

if ($_SESSION['ss_mb_id']) {
    Header("Location: /main");
}

/*
print_r($_GET);
//Array ( [state] => OK [code] => 4/0AY0e-g62mMJRcsa68mWhd9M2UWkeIgiHsMyc7QO3Rrh5JoYwv1R_chf6LxdkjrkPubKw_w [scope] => email profile https://www.googleapis.com/auth/userinfo.email openid https://www.googleapis.com/auth/userinfo.profile [authuser] => 0 [prompt] => consent )


$google_client_id = "374306251607-k72tprctfcqm4id22pttreiehmof0qi7.apps.googleusercontent.com";

$google_client_secret = "XkDAc4y4xtLRGC4jwtixya_5";
$redirection_url = "https://vellado.com/join";

$curl = curl_init();

curl_setopt_array($curl, array(
       CURLOPT_URL => 'https://www.googleapis.com/oauth2/v4/token',
       CURLOPT_POST => true,
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_TIMEOUT => 30,
       CURLOPT_POSTFIELDS => http_build_query([
           'code' => $_GET['code'],
           'client_id' => $google_client_id,
           'client_secret' => $google_client_secret,
           'redirect_uri' => $redirection_url,
           'grant_type' => 'authorization_code',
       ]),
));

$result = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);


var_dump($result);
*/


// $this->session->set_userdata("access_token", $arrResult["access_token"]); //사용자정보조회
//    $this->curl->http_header("Authorization", "Bearer ".$arrResult["access_token"]);
//    $this->curl->create("https://www.googleapis.com/oauth2/v2/userinfo");
//    $this->curl->execute(); $arrResult = $this->curl->lastResponseRaw();
//    $arrResult = json_decode($arrResult, true);


?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <title>VELLADO</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#aa15ff">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta name="naver-site-verification" content="f47bc1e19f2ef9f8ec34e16e88a11c1f99840b73" />
    <link rel="stylesheet" type="text/css" href="resources/css/common.css">
    <link href="resources/css/doka.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="resources/js/doka.min.js"></script>
    <script>
        //input type=file 버튼 이미지로 교체 이벤트
        window.addEventListener("load", function () {
            var fileSelect = document.querySelector(".join_input_pf");
            var fileElem = document.getElementById("file1");

            fileSelect.addEventListener("click", function (e) {
                if (fileElem) {
                    fileElem.click();
                }
            }, false);
        }, false);

    </script>
</head>

<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    .join_input_pf img.profile {
        border-radius: 50%;
        width: 150px;
        height: 150px;
    }
</style>

<body oncontextmenu='return false' onselectstart='return false' ondragstart='return false' >
<?php include __DIR__ . "/top_menu.inc.php"; ?>

<!-- 메인 컨텐츠 -->
<div class="main_container">
    <div class="main_content_div">

        <div class="join_container">

            <form name="join_form" id="join_form" method="POST" action="" enctype="multipart/form-data" onsubmit="check_submit()">
                <input type="hidden" name="dup_id_check" value="0"/>
                <input type="hidden" name="dup_nick_check" value="0"/>
                <input type="hidden" name="zipcode" id="zipcode" value=""/>
                <input type="file" id="file1" name="image" accept="image/*" style="display:none"/>
                <div class="join_input_pf">
                    <img src="resources/images/btn-photo.png">
                </div>

                <div class="join_warning big white">프로필 사진등록</div>
                <div class="join_warning white narrow">정사각형 사진을 업로드해 주세요.</div>
                <!--div class="join_warning big">gzss198@gmail.com</div-->
                <div class="join_input_box">
                    <input type="text" name='userid' placeholder="회원 아이디를 입력하세요" autocomplete="off">
                    <div><a href="javascript:;" id="btn_dup_id_check">중복체크</a></div>
                </div>
                <div class="join_warning" id="id_join_warning"></div>
                <div class="join_input_box">
                    <input type="text" name='nick' placeholder="닉네임을 입력하세요" autocomplete="off">
                    <div><a href="javascript:;" id="btn_dup_nick_check">중복체크</a></div>
                </div>
                <div class="join_warning" id="nick_join_warning"></div>
                <div class="join_input_box">
                    <input type="password" name="password1" placeholder="비밀번호를 입력하세요." autocomplete="off">
                </div>
                <div class="join_input_box">
                    <input type="password" name="password2" placeholder="비밀번호를 입력하세요.(확인)" autocomplete="off">
                </div>
                <div class="join_input_box">
                    <input type="number" name="phone_num" placeholder="전화번호를 입력하세요. (-생략)" autocomplete="off">
                </div>

                <div class="select_container">
                    <select class="box1" id="select" name="sel_year">
                        <option selected>생년</option>
                        <?php for ($i = 1910; $i <= 2010; $i++) : ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                    <select class="box2" id="select" name="sel_month">
                        <option selected>월</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>11</option>
                        <option>12</option>
                    </select>
                    <select class="box3" id="select" name="sel_day">
                        <option selected>일</option>
                        <?php for ($i = 1; $i <= 31; $i++) : ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>


                <div class="join_sex_container">
                    <div class="join_sex_box_male">
                        <div>남자</div>
                        <img src="resources/images/icon-male-off.png">
                    </div>
                    <div class="join_sex_box_female">
                        <div>여자</div>
                        <img src="resources/images/icon-female-off.png">
                    </div>
                </div>
                <div class="line"></div>
                <div class="join_warning white">향후 쇼핑몰 운영시 사용하실 주소를 입력해 주세요.</div>
                <div class="join_input_box">
                    <input type="text" name="addr1" id="addr1" placeholder="주소" readonly>
                    <div><a href="javascript:execPostCode()">주소검색</a></div>
                </div>
                <div class="join_input_box">
                    <input type="text" name="addr2" id="addr2" placeholder="상세주소" autocomplete="off">
                </div>
                <div class="join_warning" id="join_warning"></div>
        </div>
        <div class="common_btn_container">
            <div class="btn" onclick="check_submit()">
                <div class="text">가입완료</div>
                <input type="image" class="icon1" src="/resources/images/btn-ok-off.png"/>
            </div>
        </div>
        </form>
    </div>

</div>


<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script>
    function execPostCode() {
        daum.postcode.load(function () {
            new daum.Postcode({
                oncomplete: function (data) {
                    var addr = '';
                    if (data.userSelectedType === 'R') {
                        addr = data.roadAddress;
                    } else {
                        addr = data.jibunAddress;
                    }
                    document.getElementById('zipcode').value = data.zonecode;
                    document.getElementById("addr1").value = addr;
                    document.getElementById("addr2").focus();
                }
            }).open();
        });
    }
</script>

<script>
    //변경 이벤트 등록, 파일 변경 되었을때 doka 편집기 실행
    document.getElementById("file1").addEventListener('change', e => {
        const doka = createDoka(e);
    });

    var gender = 0; //이 값으로 0이면 선택 강제 시키고, 1이면 남, 2면 여로 form 에 담아 넘기면 됩니다.

    var genderMale = document.querySelector(".join_sex_box_male");
    var genderFemale = document.querySelector(".join_sex_box_female");

    genderMale.addEventListener("click", genderSelect, false);
    genderMale.param = 1;
    genderFemale.addEventListener("click", genderSelect, false);
    genderFemale.param = 2;

    function genderSelect() {
        gender = this.param;
        console.log(gender);
        if (genderMale.classList.contains("sel")) genderMale.classList.remove("sel");
        if (genderFemale.classList.contains("sel")) genderFemale.classList.remove("sel");

        this.classList.toggle("sel")
    }

    /**
     * 도카 객체 생성 및 옵션설정
     * @returns {*}
     */
    function createDoka(e) {
        Doka.setOptions({
            labelButtonCancel: '취소',
            labelButtonConfirm: '완료',
            labelStatusAwaitingImage: '이미지 대기중...',
            labelStatusLoadingImage: '이미지 로딩중...',
            labelStatusLoadImageError: '이미지 로딩 오류',
            labelStatusProcessingImage: '이미지 처리중',
            labelColorBrightness: '밝기',
            labelColorContrast: '대비',
            labelColorExposure: '노출',
            labelColorSaturation: '채도',
            labelResizeWidth: '너비',
            labelResizeHeight: '높이',
            labelResizeApplyChanges: '적용',
            labelCropInstructionZoom: '마우스 휠이나 터치패드로 확대/축소가 가능합니다',
            labelButtonCropZoom: '확대/축소',
            labelButtonCropRotateLeft: '좌로 회전',
            labelButtonCropRotateRight: '우로 회전',
            labelButtonCropFlipHorizontal: '좌우 반전',
            labelButtonCropFlipVertical: '상하 반전',
            labelButtonCropAspectRatio: '크기 비율',
            labelButtonCropToggleLimit: '선택영역 잘라내기',
            labelButtonUtilCrop: '잘라내기',
            labelButtonUtilFilter: '필터',
            labelButtonUtilColor: '컬러 조정',
            labelButtonUtilResize: '크기 조정',
            labelButtonUtilMarkup: '그리기 도구',
            labelButtonUtilSticker: '스티커',
            labelMarkupTypeRectangle: '사각형',
            labelMarkupTypeEllipse: '원',
            labelMarkupTypeText: '텍스트',
            labelMarkupTypeLine: '화살표',
            labelMarkupSelectFontSize: '글꼴 크기',
            labelMarkupSelectFontFamily: '글꼴',
            labelMarkupSelectLineDecoration: '장식',
            labelMarkupSelectLineStyle: '스타일',
            labelMarkupSelectShapeStyle: '스타일',
            labelMarkupRemoveShape: '삭제',
            labelMarkupToolSelect: '선택',
            labelMarkupToolDraw: '그리기',
            labelMarkupToolLine: '화살표',
            labelMarkupToolText: '텍스트',
            labelMarkupToolRect: '사각형',
            labelMarkupToolEllipse: '원'

        });
        return Doka.create({
            src: e.target.files[0],
            utils: ['crop', 'filter', 'color', 'markup'],
            styleLayoutMode: 'modal',
            allowAutoDestroy: true,
            //outputType: 'png',
            cropAllowResizeRect: false,
            cropAspectRatio: 1,
            outputWidth: 300,
            outputHeight: 300,
            onconfirm: function (output) {
                var form = $('#join_form')[0];
                var data = new FormData(form);

                $.ajax({
                    type: "POST",
                    url: "/api/member/upload_image",
                    cache: false,
                    async: false,
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        if (response.code != 200) {
                            $("#join_warning").html(response.message);
                            return false;
                        }

                        if (response.data['profile_img_url']) {
                            $(".join_input_pf").html("<img class='profile' src='" + response.data['profile_img_url'] + "'/>");
                        }
                    },
                    error: function (response, status, error) {
                        console.log("%o", response);
                        alert("프로필 이미지 업로드 오류!");
                        return false;
                    }
                });
            }
        });
    }

</script>

<script>
    function check_submit() {

        var $form = $("form[name=join_form]");
        var email = $("input[name=email]").val();
        var userid = $("input[name=userid]").val();
        var password1 = $("input[name=password1]").val();
        var password2 = $("input[name=password2]").val();
        var nick = $("input[name=nick]").val();
        var phone_num = $("input[name=phone_num]").val();

        var sel_year = $("select[name=sel_year]").val();
        var sel_month = $("select[name=sel_month]").val();
        var sel_day = $("select[name=sel_day]").val();
        var zipcode = $("input[name=zipcode]").val();
        var addr1 = $("input[name=addr1]").val();
        var addr2 = $("input[name=addr2]").val();

        var gender = "";

        var dup_id_check = $("input[name=dup_id_check]").val();
        var dup_nick_check = $("input[name=dup_nick_check]").val();


        if ($(".join_sex_box_male").hasClass("sel")) {
            gender = "M";
        }

        if ($(".join_sex_box_female").hasClass("sel")) {
            gender = "F";
        }

        /*
        if (!email) {
            $("#join_warning").html("이메일 주소는 필수 입력 항목 입니다.");
            return;
        }

         */

        if (!userid) {
            $("#join_warning").html("아이디는 필수 입력 항목 입니다.");
            return;
        }

        if (dup_id_check != 1) {
            $("#join_warning").html("아이디 중복 체크해주세요.");
            return;
        }

        if (!nick) {
            $("#join_warning").html("닉네임은 필수 입력 항목 입니다.");
            return;
        }

        if (dup_nick_check != 1) {
            $("#join_warning").html("닉네임 중복 체크해주세요.");
            return;
        }

        if (!password1) {
            $("#join_warning").html("비밀번호는 필수값입니다.");
            return;
        }

        if (password1 != password2) {
            $("#join_warning").html("확인 비밀번호가 일치하지 않습니다.");
            return;
        }

        if (!phone_num) {
            $("#join_warning").html("전화번호 입력하세요.");
            return;
        }

        if (!sel_year || !sel_month || !sel_day) {
            $("#join_warning").html("생년월일을 선택해주세요.");
            return;
        }

        if (!gender) {
            $("#join_warning").html("성별을 선택해주세요.");
            return;
        }

        var birth = sel_year + "-" + sel_month + "-" + sel_day;


        var param = {
            'email': email,
            'userid': userid,
            'password1': password1,
            'password2': password2,
            'nick': nick,
            'phone_num': phone_num,
            'gender': gender,
            'birth': birth,
            'zipcode': zipcode,
            'addr1': addr1,
            'addr2': addr2,
        };
        $.ajax({
            type: "POST",
            url: "/api/member/register",
            cache: false,
            async: false,
            data: param,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if(response.code == 200) {
                    alert("회원가입에 성공하셨습니다.");
                    location.href='/main';
                } else {
                    $("#join_warning").html(response.message);
                    return;
                }
            },
            error: function (response, status, error) {
                console.log("%o", response);
                $("#join_warning").html(response.message);
                return;
            }


        });
        return false;
    }


    $(function () {
        $("#btn_dup_nick_check").click(function () {
            var nick = $("input[name=nick]").val();

            if (nick == "") {
                $("#nick_join_warning").html("중복체크할 닉네임을 입력하세요.");
                return;
            }

            var param = {
                'nick': nick
            };

            $.ajax({
                type: "POST",
                url: "/api/member/dup_nick_check",
                cache: false,
                async: false,
                data: param,
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    if (response.code == 200) {
                        $("input[name=dup_nick_check]").val("1");
                        $("#nick_join_warning").html("이 닉네임은 사용하실 수 있습니다.");
                        $("input[name=nick]").attr("readonly", true);

                    } else {
                        $("#nick_join_warning").html(response['message']);
                        return;
                    }
                }
            });
        });

        $("#btn_dup_id_check").click(function () {
            var userid = $("input[name=userid]").val();

            if (userid == "") {
                $("#id_join_warning").html("중복체크할 아이디를 입력하세요.");
                return;
            }

            var param = {
                'userid': userid
            };

            $.ajax({
                type: "POST",
                url: "/api/member/dup_id_check",
                cache: false,
                async: false,
                data: param,
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    if (response.code == 200) {
                        $("input[name=dup_id_check]").val("1");
                        $("#id_join_warning").html("이 아이디는 사용하실 수 있습니다.");
                        $("input[name=userid]").attr("readonly", true);

                    } else {
                        $("#id_join_warning").html(response['message']);
                        return;
                    }
                }
            });
        });

    });

</script>
<script>
    window.addEventListener("load", function () {
        document.querySelector(".main_banner_logo").addEventListener("click", function (e) {
            location.href='/main'
        }, false);

    }, false);

</script>
<?php include_once __DIR__ . "/footer.php" ?>


