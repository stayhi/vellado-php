<?php

use App\Router;
use App\SimpleDB;
use App\Controllers\HelloController;
use App\Controllers\MemberController;
use App\Controllers\BoardController;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/lib/env.lib.php';
require __DIR__ . '/board/common.php';

if(!isset($pdo_db)) {
    include_once(__DIR__ . "/lib/_dbconnect_gzss.php");
}


$db = new SimpleDB($pdo_db);

$content = $db->row("select * from g5_content where co_id='return_policy' limit 1");

?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <title>VELLADO</title>
    <link rel="stylesheet" type="text/css" href="resources/css/common.css">
</head>

<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>

<body>
<!--최상단 배너-->
<div class="main_banner_container">
    <div class="main_banner_logo">
    </div>
</div>
<!-- 최상단 배너 모바일-->
<div class="main_banner_container_m">
    <div class="main_banner_logo_m">
        <img src="resources/images/logo-m.png">
    </div>
</div>

<!-- 메인 컨텐츠 -->
<div class="main_container">
    <div class="main_content_div">
        <div class="content" style="color:white">
            <?=$content['co_content']?>
        </div>
    </div>

</div>
<div class="footer_container">

</div>
</body>
</html>

