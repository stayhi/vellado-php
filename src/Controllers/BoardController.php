<?php

namespace App\Controllers;

use App\Router;
use phpbrowscap\Exception;

class BoardController extends BaseController
{

    protected $router = null;
    protected $pdo_db = null;
    protected $config = null;
    protected $db = null;

    public function __construct($router)
    {
        parent::__construct($router);
    }

    public function test()
    {
        echo "test..  <br>";
    }

    public function test2($value)
    {
        return $this->router->withJson(['message' => 'ok', 'name' => $value], 200);
    }

    public function test3($id)
    {
        return ['id' => $id];
    }


    public function test4($id)
    {
        return ['user' => $id];
    }

    public function getList($bo_table, $page=1, $rows=5)
    {
        try {
            $page = intval($page);
            $rows = intval($rows);
            if (!$page) {
                $page = 1;
            }

            if(!$rows) {
                $rows = 5;
            }

            if ($bo_table != "notice") {
                throw new Exception("접근할수 없는 게시판입니다.", 403);
            }

            $from_record = ($page-1) * $rows;
            $list = [];
            $sql = "SELECT * 
                    FROM g5_write_{$bo_table} 
                    WHERE wr_is_comment=0 
                    ORDER BY wr_id 
                    DESC LIMIT {$from_record},  {$rows} 
            ";
            $rows = $this->db->query($sql);


            foreach($rows as $row) {
                $list[] = [
                    'subject' => $row['wr_subject'],
                    'content' => $row['wr_content'],
                    'datetime' => $row['wr_datetime'],
                ];
            }

            return $this->router->withJson(['message' => 'OK', 'code' => 200, 'data' => $list], 200);

        } catch (Exception $ex) {
            return $this->router->withJson(['message' => $ex->getMessage(), 'code' => $ex->getCode()], 200);
        }
    }

    public function getView($bo_table, $wr_id) {
        $view = [];
        return $this->router->withJson(['message' => 'OK', 'code' => 200, 'data' => $view], 200);
    }
}