<?php
namespace App\Controllers;
use App\Router;

class HelloController extends BaseController
{

    protected $router = null;
    protected $pdo_db = null;
    protected $config = null;
    protected $db = null;

    public function __construct($router) {
        parent::__construct($router);
    }
    public function test() {
        echo "test..  <br>";
    }

    public function test2($value) {
        return $this->router->withJson(['message' =>'ok', 'name' => $value], 200);
    }

    public function test3($id) {
        return ['id' => $id];
    }

    public function test4($id) {
        return ['user' => $id];
    }

}