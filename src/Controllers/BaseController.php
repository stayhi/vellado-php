<?php

namespace App\Controllers;

use App\Router;
use App\SimpleDB;

class BaseController
{
    protected $router;
    protected $config;
    protected $login_mb_id = null; //로그인 사용자 아이디
    protected $is_login = false; //로그인 여부
    protected $db = null;

    public function __construct($router) {
        $this->router = $router;
        $this->config = $router->getConfig();
        $this->db = new SimpleDB($this->config['pdo_db']);
        $this->login_mb_id = $_SESSION['ss_mb_id'];
        $this->is_login = isset($this->login_mb_id);
    }

    public function getConfig($key = null) {
        if ($key) {
            return $this->config[$key];
        } else {
            return $this->config;
        }
    }

}
