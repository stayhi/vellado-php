<?php

namespace App\Controllers;

use Exception;

class MemberController extends BaseController
{

    protected $router = null;
    protected $pdo_db = null;
    protected $config = null;

    public function __construct($router) {
        parent::__construct($router);
    }

    public function test() {
        echo "test..  <br>";
    }

    public function test4($id) {
        return ['user' => $id];
    }


    public function test2($value) {
        return $this->router->withJson(['message' => 'ok', 'name' => $value], 200);
    }

    /**
     * 회원 로그인 처리
     */
    public function login($requestData) {

        try {
            $config = $this->db->row("SELECT * FROM g5_config LIMIT 1");

            $mb_id = trim($requestData['userid']);
            $mb_password = trim($requestData['password']);


            if (!empty($_SESSION['ss_mb_id'])) {
                throw new Exception("이미 로그인 되어 있습니다.", 400);
            }

            if (empty($mb_id)) {
                throw new Exception("아이디가 넘어오지 않았습니다.", 400);
            }

            if (empty($mb_password)) {
                throw new Exception("비밀번호를 입력해 주세요.", 400);
            }

            $mb = get_member($mb_id);

            //소셜 로그인추가 체크

            $is_social_login = false;
            $is_social_password_check = false;

            // 소셜 로그인이 맞는지 체크하고 해당 값이 맞는지 체크합니다.
            if (function_exists('social_is_login_check')) {
                $is_social_login = social_is_login_check();

                //패스워드를 체크할건지 결정합니다.
                //소셜로그인일때는 체크하지 않고, 계정을 연결할때는 체크합니다.
                $is_social_password_check = social_is_login_password_check($mb_id);
            }

            //소셜 로그인이 맞다면 패스워드를 체크하지 않습니다.
            // 가입된 회원이 아니다. 비밀번호가 틀리다. 라는 메세지를 따로 보여주지 않는 이유는
            // 회원아이디를 입력해 보고 맞으면 또 비밀번호를 입력해보는 경우를 방지하기 위해서입니다.
            // 불법사용자의 경우 회원아이디가 틀린지, 비밀번호가 틀린지를 알기까지는 많은 시간이 소요되기 때문입니다.
            if (!$is_social_password_check && (!$mb['mb_id'] || !login_password_check($mb, $mb_password, $mb['mb_password']))) {
                throw new Exception('가입된 회원아이디가 아니거나 비밀번호가 틀립니다. 비밀번호는 대소문자를 구분합니다.', 403);
            }

            // 차단된 아이디인가?
            if ($mb['mb_intercept_date'] && $mb['mb_intercept_date'] <= date("Ymd", G5_SERVER_TIME)) {
                $date = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "\\1년 \\2월 \\3일", $mb['mb_intercept_date']);
                throw new Exception('회원님의 아이디는 접근이 금지되어 있습니다.\n처리일 : ' . $date, 403);
            }

            // 탈퇴한 아이디인가?
            if ($mb['mb_leave_date'] && $mb['mb_leave_date'] <= date("Ymd", G5_SERVER_TIME)) {
                $date = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "\\1년 \\2월 \\3일", $mb['mb_leave_date']);
                throw new Exception('탈퇴한 아이디이므로 접근하실 수 없습니다.\n탈퇴일 : ' . $date, 403);
            }

            // 회원아이디 세션 생성
            set_session('ss_mb_id', $mb['mb_id']);
            // FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
            set_session('ss_mb_key', md5($mb['mb_datetime'] . get_real_client_ip() . $_SERVER['HTTP_USER_AGENT']));

            // 포인트 체크
            if ($config['cf_use_point']) {
                $sum_point = get_point_sum($mb['mb_id']);

                $sql = " update g5_member set mb_point = '$sum_point' where mb_id = '{$mb['mb_id']}' ";
                sql_query($sql);
            }

            $data = [
                'userid' => $mb_id
            ];

            return $this->router->withJson(['message' => 'OK', 'code' => 200, 'data' => $data], 200);


        } catch (Exception $ex) {
            return $this->router->withJson(['message' => $ex->getMessage(), 'code' => $ex->getCode()], 200);
        }
    }

    /**
     * 회원가입 처리
     * @param $requestData
     * @return bool|float|int|mixed|\Services_JSON_Error|string
     */
    public function register($requestData) {

        try {

            //$mb_id = generate_random_string(10);
            $mb_hp = clean_xss_tags(trim($requestData['phone_num']));
            $mb_id = clean_xss_tags(trim($requestData['userid']));
            $mb_password = clean_xss_tags(trim($requestData['password1']));
            $mb_nick = clean_xss_tags(trim($requestData['nick']));
            $mb_birth = clean_xss_tags(trim($requestData['birth']));
            $mb_email = clean_xss_tags(trim($requestData['email']));
            $mb_addr1 = clean_xss_tags(trim($requestData['addr1']));
            $mb_addr2 = clean_xss_tags(trim($requestData['addr2']));
            $mb_zip1 = clean_xss_tags(trim($requestData['zipcode']));
            $mb_sex = clean_xss_tags(trim($requestData['gender']));


            if ($msg = empty_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }
            if ($msg = valid_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = count_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = exist_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = reserve_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = empty_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }


            /*
            if ($msg = empty_mb_email($mb_email)) {
                throw new Exception($msg, 400);
            }
            */
            if ($msg = reserve_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }

            // 이름에 한글명 체크를 하지 않는다.
            if ($msg = valid_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }

            if ($msg = exist_mb_nick($mb_nick, $mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = exist_mb_nick($mb_nick, $mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($mb_birth) {
                $mb_birth = date("Y-m-d", strtotime($mb_birth));
            }

            /*
            if ($msg = valid_mb_email($mb_email)) {
                throw new Exception($msg, 400);
            }
            if ($msg = prohibit_mb_email($mb_email)) {
                throw new Exception($msg, 400);
            }
            */

            $reg_mb_hp = preg_replace("/[^0-9]/", "", $mb_hp);
            if (!$reg_mb_hp) {
                throw new Exception("휴대폰번호를 입력해 주십시오.", 400);
            } else {
                if (!preg_match("/^0[0-9]{8,10}$/", $reg_mb_hp)) {
                    throw new Exception("연락처를 올바르게 입력해 주십시오.", 400);
                }
            }

            $data = [
                'mb_id' => $mb_id,
                'mb_password' => $mb_password,
                'mb_nick' => $mb_nick,
                'mb_birth' => $mb_birth,
                'mb_sex' => $mb_sex,
                'mb_email' => $mb_email,
                'mb_addr1' => $mb_addr1,
                'mb_addr2' => $mb_addr2,
                'mb_zip1' => $mb_zip1,
                'mb_hp' => $reg_mb_hp
            ];


            //회원 db 등록
            $this->insertMemeberDB($data);

            $tmp_mb_id = $_SESSION['tmp_mb_id'];

            $mb_tmp_dir = G5_DATA_PATH . '/member_image/';

            $tmp_file_path = $mb_tmp_dir . substr($tmp_mb_id, 0, 2) . "/" . $tmp_mb_id . ".gif";
            $tmp_file_path25 = $mb_tmp_dir . substr($tmp_mb_id, 0, 2) . "/" . $tmp_mb_id . "_25x25.gif";
            syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " tmp_file_path   " . $tmp_file_path . "");

            if (file_exists($tmp_file_path)) {
                $mb_dir = $mb_tmp_dir . substr($mb_id, 0, 2);
                if (!is_dir($mb_tmp_dir)) {
                    @mkdir($mb_tmp_dir, G5_DIR_PERMISSION);
                    @chmod($mb_tmp_dir, G5_DIR_PERMISSION);
                }

                $new_file_path = $mb_dir . substr($mb_id, 0, 2) . "/" . $mb_id . ".gif";
                $new_file_path25 = $mb_dir . substr($mb_id, 0, 2) . "/" . $mb_id . "_25x25.gif";

                syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " new_file_path   " . $new_file_path . "");

                @copy($tmp_file_path, $new_file_path);
                @copy($tmp_file_path25, $new_file_path25);
            }

            set_session('ss_mb_id', $mb_id);

            set_session('ss_mb_reg', $mb_id);


            return $this->router->withJson(['message' => 'OK', 'code' => 200], 200);
        } catch (Exception $ex) {
            return $this->router->withJson(['message' => $ex->getMessage(), 'code' => $ex->getCode()], 200);
        }
    }

    /**
     * 회원가입 처리
     * @param $requestData
     * @return bool|float|int|mixed|\Services_JSON_Error|string
     */
    public function modify($requestData) {

        try {

            $mb_id = $_SESSION['ss_mb_id'];

            //$mb_id = generate_random_string(10);
            $mb_hp = clean_xss_tags(trim($requestData['phone_num']));
            $mb_password = clean_xss_tags(trim($requestData['password1']));
            $mb_nick = clean_xss_tags(trim($requestData['nick']));
            $mb_addr1 = clean_xss_tags(trim($requestData['addr1']));
            $mb_addr2 = clean_xss_tags(trim($requestData['addr2']));
            $mb_zip1 = clean_xss_tags(trim($requestData['zipcode']));


            if ($msg = empty_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }

            if ($msg = reserve_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }

            // 이름에 한글명 체크를 하지 않는다.
            if ($msg = valid_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }

            if ($msg = exist_mb_nick($mb_nick, $mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = exist_mb_nick($mb_nick, $mb_id)) {
                throw new Exception($msg, 400);
            }


            $reg_mb_hp = preg_replace("/[^0-9]/", "", $mb_hp);
            if (!$reg_mb_hp) {
                throw new Exception("휴대폰번호를 입력해 주십시오.", 400);
            } else {
                if (!preg_match("/^0[0-9]{8,10}$/", $reg_mb_hp)) {
                    throw new Exception("연락처를 올바르게 입력해 주십시오.", 400);
                }
            }

            $data = [
                'mb_id' => $mb_id,
                'mb_password' => $mb_password,
                'mb_nick' => $mb_nick,
                'mb_addr1' => $mb_addr1,
                'mb_addr2' => $mb_addr2,
                'mb_zip1' => $mb_zip1,
                'mb_hp' => $reg_mb_hp
            ];

            //회원 db 수정
            $this->updateMemberDB($data);

            return $this->router->withJson(['message' => 'OK', 'code' => 200], 200);
        } catch (Exception $ex) {
            return $this->router->withJson(['message' => $ex->getMessage(), 'code' => $ex->getCode()], 200);
        }
    }


    public function logout() {
        $_SESSION = [];
        session_destroy();

        return $this->router->withJson(['message' => 'OK', 'code' => 200], 200);
    }

    public function checkDuplicationNick($mb_nick, $reg_mb_id) {
        try {

            set_session('ss_check_mb_nick', '');
            if ($msg = empty_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }
            if ($msg = valid_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }
            if ($msg = count_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }
            if ($msg = exist_mb_nick($mb_nick, $reg_mb_id)) {
                throw new Exception($msg, 400);
            }
            if ($msg = reserve_mb_nick($mb_nick)) {
                throw new Exception($msg, 400);
            }

            set_session('ss_check_mb_nick', $mb_nick);

            return $this->router->withJson(['message' => 'OK', 'code' => 200], 200);
        } catch (Exception $ex) {
            return $this->router->withJson(['message' => $ex->getMessage(), 'code' => $ex->getCode()], 200);
        }
    }

    public function checkDuplicationUserid($mb_id) {
        try {

            set_session('ss_check_mb_id', '');
            if ($msg = empty_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }
            if ($msg = valid_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = count_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = reserve_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }

            if ($msg = exist_mb_id($mb_id)) {
                throw new Exception($msg, 400);
            }

            set_session('ss_check_mb_id', $mb_id);

            return $this->router->withJson(['message' => 'OK', 'code' => 200], 200);
        } catch (Exception $ex) {
            return $this->router->withJson(['message' => $ex->getMessage(), 'code' => $ex->getCode()], 200);
        }
    }


    public function insertMemeberDB($data) {
        //회원가입 데이터 정의
        $data['mb_id'] = clean_xss_tags(trim($data['mb_id']));
        $data['mb_password'] = clean_xss_tags(trim($data['mb_password']));
        $data['mb_name'] = clean_xss_tags(trim($data['mb_name']));
        $data['mb_nick'] = clean_xss_tags(trim($data['mb_nick']));
        $data['mb_email'] = clean_xss_tags(trim($data['mb_email']));
        $data['mb_sex'] = isset($data['mb_sex']) ? trim($data['mb_sex']) : "";
        $data['mb_birth'] = isset($data['mb_birth']) ? trim($data['mb_birth']) : "";
        $data['mb_hp'] = isset($data['mb_hp']) ? trim($data['mb_hp']) : "";
        $data['mb_profile'] = isset($data['mb_profile']) ? trim($data['mb_profile']) : "";
        $data['mb_memo'] = isset($data['mb_memo']) ? trim($data['mb_memo']) : "";
        $data['mb_lost_certify'] = isset($data['mb_lost_certify']) ? trim($data['mb_lost_certify']) : 0;
        $data['mb_mailling'] = isset($data['mb_mailling']) ? trim($data['mb_mailling']) : 0;
        $data['mb_sms'] = isset($data['mb_sms']) ? trim($data['mb_sms']) : 0;
        $data['mb_point'] = 0;
        $data['mb_nick_date'] = date("Y-m-d");
        $data['mb_today_login'] = date("Y-m-d H:i:s");
        $data['mb_datetime'] = date("Y-m-d H:i:s");
        $data['mb_level'] = 2;
        $data['mb_login_ip'] = $_SERVER['REMOTE_ADDR'];
        $data['mb_ip'] = $_SERVER['REMOTE_ADDR'];
        $data['mb_open_date'] = date("Y-m-d");
        $data['mb_email_certify'] = date("Y-m-d H:i:s");


        $sql = "INSERT INTO g5_member
                    SET mb_id = :mb_id,
                         mb_password = :mb_password,
                         mb_name = '',
                         mb_nick = :mb_nick,
                         mb_nick_date = '" . G5_TIME_YMD . "',
                         mb_email = :mb_email,
                         mb_zip1 = :mb_zip1,
                         mb_addr1 = :mb_addr1,
                         mb_addr2 = :mb_addr2,
                         mb_hp = :mb_hp,
                         mb_birth = :mb_birth,
                         mb_today_login = '" . G5_TIME_YMDHIS . "',
                         mb_datetime = '" . G5_TIME_YMDHIS . "',
                         mb_ip = '{$_SERVER['REMOTE_ADDR']}',
                         mb_level = '2',
                         mb_login_ip = '{$_SERVER['REMOTE_ADDR']}',
                         mb_mailling = '{$data['mb_mailling']}',
                         mb_sms = '{$data['mb_sms']}',
                         mb_open = '{$data['mb_open']}',
                         mb_open_date = '" . G5_TIME_YMD . "'
               ";

        $param = [
            'mb_id' => $data['mb_id'],
            'mb_password' => get_encrypt_string($data['mb_password']),
            'mb_nick' => $data['mb_nick'],
            'mb_email' => $data['mb_email'],
            'mb_zip1' => $data['mb_zip1'],
            'mb_addr1' => $data['mb_addr1'],
            'mb_addr2' => $data['mb_addr2'],
            'mb_hp' => $data['mb_hp'],
            'mb_birth' => $data['mb_birth'],
        ];

        //print_r($param);

        $is_insert = $this->db->query($sql, $param);
        if (!$is_insert) {
            throw new Exception("회원 가입 실패 재시도 부탁드립니다.", 400);
        }

    }

    public function updateMemberDB($data) {
        //회원가입 데이터 정의
        $data['mb_id'] = clean_xss_tags(trim($data['mb_id']));
        $data['mb_password'] = clean_xss_tags(trim($data['mb_password']));
        $data['mb_name'] = clean_xss_tags(trim($data['mb_name']));
        $data['mb_nick'] = clean_xss_tags(trim($data['mb_nick']));
        $data['mb_email'] = clean_xss_tags(trim($data['mb_email']));
        $data['mb_sex'] = isset($data['mb_sex']) ? trim($data['mb_sex']) : "";
        $data['mb_birth'] = isset($data['mb_birth']) ? trim($data['mb_birth']) : "";
        $data['mb_hp'] = isset($data['mb_hp']) ? trim($data['mb_hp']) : "";
        $data['mb_profile'] = isset($data['mb_profile']) ? trim($data['mb_profile']) : "";
        $data['mb_memo'] = isset($data['mb_memo']) ? trim($data['mb_memo']) : "";
        $data['mb_lost_certify'] = isset($data['mb_lost_certify']) ? trim($data['mb_lost_certify']) : 0;
        $data['mb_mailling'] = isset($data['mb_mailling']) ? trim($data['mb_mailling']) : 0;
        $data['mb_sms'] = isset($data['mb_sms']) ? trim($data['mb_sms']) : 0;
        $data['mb_point'] = 0;
        $data['mb_nick_date'] = date("Y-m-d");
        $data['mb_today_login'] = date("Y-m-d H:i:s");
        $data['mb_datetime'] = date("Y-m-d H:i:s");
        $data['mb_level'] = 2;
        $data['mb_login_ip'] = $_SERVER['REMOTE_ADDR'];
        $data['mb_ip'] = $_SERVER['REMOTE_ADDR'];
        $data['mb_open_date'] = date("Y-m-d");
        $data['mb_email_certify'] = date("Y-m-d H:i:s");


        $sql = "UPDATE  g5_member
                    SET 
                         mb_nick = :mb_nick,
                         mb_nick_date = '" . G5_TIME_YMD . "',
                         mb_zip1 = :mb_zip1,
                         mb_addr1 = :mb_addr1,
                         mb_addr2 = :mb_addr2,
                         mb_hp = :mb_hp
                WHERE mb_id=:mb_id         
         ";

        $param = [
            'mb_id' => $data['mb_id'],
            'mb_nick' => $data['mb_nick'],
            'mb_zip1' => $data['mb_zip1'],
            'mb_addr1' => $data['mb_addr1'],
            'mb_addr2' => $data['mb_addr2'],
            'mb_hp' => $data['mb_hp'],
        ];

        $this->db->query($sql, $param);


        if ($data['mb_password']) {
            $sql = "UPDATE g5_member 
                        SET mb_password = :mb_password
                    WHERE mb_id = :mb_id
                    
            ";

            $mb_password = get_encrypt_string($data['mb_password']);

            $this->db->query($sql, [
                'mb_password' => $mb_password,
                'mb_id' => $data['mb_id']
            ]);
        }
    }

    public function uplaodProfileImage() {

        try {

            $config = $this->db->row("SELECT * FROM g5_config LIMIT 1");

            $is_member = false;
            if ($_SESSION['ss_mb_id'] != "") {
                $mb_id = $_SESSION['ss_mb_id'];
                $is_member = true;
            } else if ($_SESSION['tmp_mb_id'] != "") {
                $mb_id = $_SESSION['tmp_mb_id'];
            } else {
                $mb_id = generate_random_string(10);
                set_session("tmp_mb_id", $mb_id);
            }

            $mb_icon = '';
            $image_regex = "/(\.(gif|jpe?g|png))$/i";
            $mb_icon_img = get_mb_icon_name($mb_id) . '.gif';
            $mb_icon_img50 =  $mb_id."_50x50.gif";

            $mb_tmp_dir = G5_DATA_PATH . '/member_image/';
            $mb_dir = $mb_tmp_dir . substr($mb_id, 0, 2);
            if (!is_dir($mb_tmp_dir)) {
                @mkdir($mb_tmp_dir, G5_DIR_PERMISSION);
                @chmod($mb_tmp_dir, G5_DIR_PERMISSION);
            }

            syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " test1   " . $_FILES['image']['tmp_name']);


            // 회원 프로필 이미지 업로드
            if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {

                syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " test 2 " . $_FILES['image']['tmp_name']);


                if (preg_match($image_regex, $_FILES['image']['name'])) {
                    // 아이콘 용량이 설정값보다 이하만 업로드 가능
                    syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " test 3 " . $_FILES['image']['tmp_name']);

                    if ($_FILES['image']['size'] <= $config['cf_member_img_size']) {

                        syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " test 4 " . $config['cf_member_img_width'] . "x" . $config['cf_member_img_height']);


                        @mkdir($mb_dir, G5_DIR_PERMISSION);
                        @chmod($mb_dir, G5_DIR_PERMISSION);
                        $dest_path = $mb_dir . '/' . $mb_icon_img;
                        $dest_path_thumb50 = $mb_dir . '/' . $mb_icon_img50;
                        move_uploaded_file($_FILES['image']['tmp_name'], $dest_path);
                        @chmod($dest_path, G5_FILE_PERMISSION);
                        if (file_exists($dest_path)) {

                            syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " test 5 " . $_FILES['image']['tmp_name']);

                            $size = @getimagesize($dest_path);

                            syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " test 6 " . json_encode($size));

                            if (!($size[2] === 1 || $size[2] === 2 || $size[2] === 3)) { // gif jpg png 파일이 아니면 올라간 이미지를 삭제한다.
                                @unlink($dest_path);
                            } else if ($size[0] > $config['cf_member_img_width'] || $size[1] > $config['cf_member_img_height']) {
                                $thumb = null;
                                if ($size[2] === 2 || $size[2] === 3) {
                                    //jpg 또는 png 파일 적용
                                    $thumb = thumbnail($mb_icon_img, $mb_dir, $mb_dir, $config['cf_member_img_width'], $config['cf_member_img_height'], true, true);
                                    if ($thumb) {
                                        @unlink($dest_path);
                                        rename($mb_dir . '/' . $thumb, $dest_path);
                                    }
                                    $thumb50 = thumbnail($mb_icon_img, $mb_dir, $mb_dir, 50, 50, true, true);
                                    if ($thumb50) {
                                        rename($mb_dir . '/' . $thumb50, $dest_path_thumb50);
                                    }
                                }
                                if (!$thumb) {
                                    // 아이콘의 폭 또는 높이가 설정값 보다 크다면 이미 업로드 된 아이콘 삭제
                                    @unlink($dest_path);
                                }
                            }
                            //=================================================================\
                        }
                    } else {
                        throw new Exception('회원이미지을 ' . number_format($config['cf_member_img_size']) . '바이트 이하로 업로드 해주십시오.', 400);
                    }

                } else {
                    throw new Exception($_FILES['image']['name'] . '은(는) gif/jpg 파일이 아닙니다.', 400);
                }
            }


            $profile_img_url = "/data/member_image/" . substr($mb_id, 0, 2) . "/" . $mb_id . ".gif";

            $data = [
                'profile_img_url' => $profile_img_url
            ];
            return $this->router->withJson(['message' => 'OK', 'code' => 200, 'data' => $data], 200);
        } catch (Exception $ex) {
            return $this->router->withJson(['message' => $ex->getMessage(), 'code' => $ex->getCode()], 200);
        }
    }


}