<?php

namespace App\Controllers;

use Exception;

class PaymentController extends BaseController
{

    protected $router = null;
    protected $pdo_db = null;
    protected $config = null;

    private $application_id = "5feafd922fa5c2002103971c";
    private $private_key = "s34nweY1M3rrnO0NSdQn0txn6agl8j/8Vk9UlQtZE/Q=";


    private $view_seconds = [
        "1" => 0,
        "2" => 60 * 2,
        "3" => 60 * 5,
        "4" => 60 * 10,
        "5" => 60 * 30,
        "6" => 60 * 60 * 1,
        "7" => 60 * 60 * 2,
        "8" => 60 * 60 * 3,
        "9" => 60 * 60 * 5,
    ];

    public function __construct($router) {
        parent::__construct($router);
    }

    public function test() {
        echo "test..  <br>";
    }

    public function test4($id) {
        return ['user' => $id];
    }


    /**
     * 결제 콜백처리
     */
    public function callback($requestData) {

        try {
            $config = $this->db->row("SELECT * FROM g5_config LIMIT 1");

            syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " request:   " . json_encode($requestData) . "");

            $status = $requestData['status'];

            $receipt_id = $requestData['receipt_id'];
            $price = $requestData['price'];


            $pg = $requestData['pg'];
            $pg_name = $requestData['pg_name'];
            $method = $requestData['method'];
            $method_name = $requestData['method_name'];
            $order_id = $requestData['order_id'];
            $card_name = $requestData['payment_data']['card_name'];

            if ($status == "1") {

                $item_name = $requestData['name'];

                $userid = trim($requestData['params']['userid']);
                $vnum = trim($requestData['params']['vnum']);
                $nick = trim($requestData['params']['nick']);
                $pfURL = trim($requestData['params']['pfURL']);
                $message = trim($requestData['params']['message']);
                $channel_id = trim($requestData['params']['chat_channel']);

                if(!$channel_id) {
                    $channel_id = "ALL";
                }

                $message = mb_substr($message, 0, 200, 'utf-8'); //200글자 제한

                $view_second = 0;

                if($vnum) {
                    $view_second = $this->view_seconds[$vnum.""];
                }

                $sql = "INSERT INTO chatdb.pchat_donation
                SET channel_type    = 'N',
                    room_uid        = :channel_id,
                    tr_id           = :tr_id,
                    userid          = :userid,
                    username        = :username,
                    profile_img     = :profile_img,
                    amount_type     = 'VBOX',
                    amount          = :amount,
                    message         = :message,
                    view_start_timestamp = now(),
                    view_second = :view_second,
                    is_chat_view    = 'N',
                    create_datetime = now()
                ";

                $this->db->query($sql,
                    [
                        'channel_id' => $channel_id,
                        'tr_id' => $receipt_id,
                        'userid' => $userid,
                        'username' => $nick,
                        'profile_img' => $pfURL,
                        'amount' => $vnum,
                        'message' => $message,
                        'view_second' => $view_second
                    ]
                );

                $sql = "INSERT INTO bootpay_payment 
                        SET 
                            receipt_id = :receipt_id,
                            status = :status,
                            price = :price,
                            pg = :pg,
                            pg_name = :pg_name,
                            method = :method,
                            method_name = :method_name,
                            order_id = :order_id,
                            card_name = :card_name,
                            mb_id = :mb_id,
                            mb_nick = :mb_nick,
                            channel_id = :channel_id,
                            message = :message,
                            item_name = :item_name,
                            create_datetime = now(),
                            update_datetime = now(),
                            complete_datetime = now()
                ";

                $this->db->query($sql,
                    [
                        'receipt_id' => $receipt_id,
                        'status' => $status,
                        'price' => $price,
                        'pg' => $pg,
                        'pg_name' => $pg_name,
                        'method' => $method,
                        'method_name' => $method_name,
                        'order_id' => $order_id,
                        'card_name' => $card_name,
                        'mb_id' => $userid,
                        'mb_nick' => $nick,
                        'channel_id' => $channel_id,
                        'message' => $message,
                        'item_name' => $item_name,
                    ]);

                echo "OK";
                if (true) exit;
            }
        } catch (Exception $ex) {
            syslog(LOG_INFO, $_SERVER['HTTP_HOST'] . "  FILE : " . __FILE__ . " LINE : " . __LINE__ . " error:   " . $ex->getMessage() . "");
            echo "OK";
            exit;
        }
    }
}