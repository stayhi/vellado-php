<?php
require_once __DIR__."/../vendor/autoload.php";
require_once __DIR__."/../lib/env.lib.php";
require_once __DIR__."/../lib/_dbconnect.php";
require_once __DIR__."/../lib/SimpleDB.php";
require_once __DIR__."/../lib/VimeoSync.php";

$sync = new VimeoSync($pdo_db);

$total_count = $sync->getTotalVideos();

echo "total : {$total_count} \n";

$per_page = 25;

$total_page  = ceil($total_count / $per_page);  // 전체 페이지 계산

$max_sync_page = 5;
for($i = 1; $i <= $total_page ; $i++) {

    echo "$i page request... \n";
    $sync->SyncVideo($i, $per_page);

    if($max_sync_page == $i) {
        break;
    }
}
//$sync->syncVOD();




