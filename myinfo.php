<?php

include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL ^ E_NOTICE);

session_start();

if (!$_SESSION['ss_mb_id']) {
    Header("Location: /login");
}

$db = new SimpleDB($pdo_db);

$member = $db->row("select * from g5_member where mb_id = :mb_id ", ['mb_id' => $_SESSION['ss_mb_id']] );


$profile_img_path = __DIR__ . "/data/member_image/" . substr($_SESSION['ss_mb_id'], 0, 2) . "/" . $_SESSION['ss_mb_id'] . ".gif";


if (file_exists($profile_img_path)) {
    $member['$profile_img_path '] = $profile_img_path;
    $member['profile_img'] = "/data/member_image/" . substr($_SESSION['ss_mb_id'], 0, 2) . "/" . $_SESSION['ss_mb_id'] . ".gif";
} else {
    $member['profile_img'] = null;
}

?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <meta name="naver-site-verification" content="f47bc1e19f2ef9f8ec34e16e88a11c1f99840b73" />
    <title>VELLADO</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#aa15ff">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="resources/css/common.css">
    <link href="resources/css/doka.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="resources/js/doka.min.js"></script>
    <script>
        //input type=file 버튼 이미지로 교체 이벤트
        window.addEventListener("load", function () {
            var fileSelect = document.querySelector(".join_input_pf");
            var fileElem = document.getElementById("file1");

            fileSelect.addEventListener("click", function (e) {
                if (fileElem) {
                    fileElem.click();
                }
            }, false);
        }, false);

    </script>
</head>

<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    .join_input_pf img.profile {
        border-radius: 50%;
        width: 150px;
        height: 150px;
    }
</style>

<body oncontextmenu='return false' onselectstart='return false' ondragstart='return false' >
<?php include __DIR__ . "/top_menu.inc.php"; ?>

<!-- 메인 컨텐츠 -->
<div class="main_container">
    <div class="main_content_div">

        <div class="join_container">

            <form name="join_form" id="join_form" method="POST" action="" enctype="multipart/form-data" onsubmit="check_submit()">
                <input type="hidden" name="dup_nick_check" value="1"/>
                <input type="hidden" name="zipcode" id="zipcode" value="<?=$member['mb_zip1']?>"/>
                <input type="file" id="file1" name="image" accept="image/*" style="display:none"/>
                <div class="join_input_pf">
                    <?php if($member['profile_img']) : ?>
                        <img class='profile' src="<?=$member['profile_img']?>?t=<?=time()?>">
                    <?php else : ?>
                        <img src="resources/images/btn-photo.png">
                    <?php endif; ?>
                </div>

                <div class="join_warning big white">프로필 사진등록</div>
                <div class="join_warning white narrow">정사각형 사진을 업로드해 주세요.</div>
                <div class="join_input_box">
                    <input type="text" name='nick'  value="<?=$member['mb_nick']?>" placeholder="닉네임을 입력하세요">
                    <div><a href="javascript:;" id="btn_dup_nick_check">중복체크</a></div>
                </div>
                <div class="join_warning" id="nick_join_warning"></div>
                <div class="join_input_box">
                    <input type="password" name="password1" placeholder="비밀번호를 입력하세요.">
                </div>
                <div class="join_input_box">
                    <input type="password" name="password2" placeholder="비밀번호를 입력하세요.(확인)">
                </div>
                <div class="join_input_box">
                    <input type="number" name="phone_num" value="<?=$member['mb_hp']?>" placeholder="전화번호를 입력하세요. (-생략)">
                </div>

                <div class="line"></div>
                <div class="join_warning white">향후 쇼핑몰 운영시 사용하실 주소를 입력해 주세요.</div>
                <div class="join_input_box">
                    <input type="text" name="addr1" id="addr1" value="<?=$member['mb_addr1']?>" placeholder="주소" readonly>
                    <div><a href="javascript:execPostCode()">주소검색</a></div>
                </div>
                <div class="join_input_box">
                    <input type="text" name="addr2" id="addr2" value="<?=$member['mb_addr2']?>" placeholder="상세주소">
                </div>
                <div class="join_warning" id="join_warning"></div>
        </div>
        <div class="common_btn_container">
            <div class="btn" onclick="check_submit()">
                <div class="text">저장</div>
            </div>
        </div>
        </form>
    </div>

</div>

<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script>
    function execPostCode() {
        daum.postcode.load(function () {
            new daum.Postcode({
                oncomplete: function (data) {
                    var addr = '';
                    if (data.userSelectedType === 'R') {
                        addr = data.roadAddress;
                    } else {
                        addr = data.jibunAddress;
                    }
                    document.getElementById('zipcode').value = data.zonecode;
                    document.getElementById("addr1").value = addr;
                    document.getElementById("addr2").focus();
                }
            }).open();
        });
    }
</script>


<script>
    //변경 이벤트 등록, 파일 변경 되었을때 doka 편집기 실행
    document.getElementById("file1").addEventListener('change', e => {
        const doka = createDoka(e);
    });
    /**
     * 도카 객체 생성 및 옵션설정
     * @returns {*}
     */
    function createDoka(e) {
        Doka.setOptions({
            labelButtonCancel: '취소',
            labelButtonConfirm: '완료',
            labelStatusAwaitingImage: '이미지 대기중...',
            labelStatusLoadingImage: '이미지 로딩중...',
            labelStatusLoadImageError: '이미지 로딩 오류',
            labelStatusProcessingImage: '이미지 처리중',
            labelColorBrightness: '밝기',
            labelColorContrast: '대비',
            labelColorExposure: '노출',
            labelColorSaturation: '채도',
            labelResizeWidth: '너비',
            labelResizeHeight: '높이',
            labelResizeApplyChanges: '적용',
            labelCropInstructionZoom: '마우스 휠이나 터치패드로 확대/축소가 가능합니다',
            labelButtonCropZoom: '확대/축소',
            labelButtonCropRotateLeft: '좌로 회전',
            labelButtonCropRotateRight: '우로 회전',
            labelButtonCropFlipHorizontal: '좌우 반전',
            labelButtonCropFlipVertical: '상하 반전',
            labelButtonCropAspectRatio: '크기 비율',
            labelButtonCropToggleLimit: '선택영역 잘라내기',
            labelButtonUtilCrop: '잘라내기',
            labelButtonUtilFilter: '필터',
            labelButtonUtilColor: '컬러 조정',
            labelButtonUtilResize: '크기 조정',
            labelButtonUtilMarkup: '그리기 도구',
            labelButtonUtilSticker: '스티커',
            labelMarkupTypeRectangle: '사각형',
            labelMarkupTypeEllipse: '원',
            labelMarkupTypeText: '텍스트',
            labelMarkupTypeLine: '화살표',
            labelMarkupSelectFontSize: '글꼴 크기',
            labelMarkupSelectFontFamily: '글꼴',
            labelMarkupSelectLineDecoration: '장식',
            labelMarkupSelectLineStyle: '스타일',
            labelMarkupSelectShapeStyle: '스타일',
            labelMarkupRemoveShape: '삭제',
            labelMarkupToolSelect: '선택',
            labelMarkupToolDraw: '그리기',
            labelMarkupToolLine: '화살표',
            labelMarkupToolText: '텍스트',
            labelMarkupToolRect: '사각형',
            labelMarkupToolEllipse: '원'

        });
        return Doka.create({
            src: e.target.files[0],
            utils: ['crop', 'filter', 'color', 'markup'],
            styleLayoutMode: 'modal',
            allowAutoDestroy: true,
            //outputType: 'png',
            cropAllowResizeRect: false,
            cropAspectRatio: 1,
            outputWidth: 300,
            outputHeight: 300,
            onconfirm: function (output) {
                var form = $('#join_form')[0];
                var data = new FormData(form);

                $.ajax({
                    type: "POST",
                    url: "/api/member/upload_image",
                    cache: false,
                    async: false,
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        if (response.code != 200) {
                            $("#join_warning").html(response.message);
                            return false;
                        }

                        if (response.data['profile_img_url']) {
                            $(".join_input_pf").html("<img class='profile' src='" + response.data['profile_img_url'] + "?t=" + Date.now() + "'/>");
                        }
                    },
                    error: function (response, status, error) {
                        console.log("%o", response);
                        alert("프로필 이미지 업로드 오류!");
                        return false;
                    }
                });
            }
        });
    }

</script>

<script>
    function check_submit() {

        var $form = $("form[name=join_form]");
        var password1 = $("input[name=password1]").val();
        var password2 = $("input[name=password2]").val();
        var nick = $("input[name=nick]").val();
        var phone_num = $("input[name=phone_num]").val();
        var zipcode = $("input[name=zipcode]").val();
        var addr1 = $("input[name=addr1]").val();
        var addr2 = $("input[name=addr2]").val();
        var dup_nick_check = $("input[name=dup_nick_check]").val();


        if (!nick) {
            $("#join_warning").html("닉네임은 필수 입력 항목 입니다.");
            return;
        }

        if (dup_nick_check != 1) {
            $("#join_warning").html("닉네임 중복 체크해주세요.");
            return;
        }

        if (password1 != password2) {
            $("#join_warning").html("확인 비밀번호가 일치하지 않습니다.");
            return;
        }

        if (!phone_num) {
            $("#join_warning").html("전화번호 입력하세요.");
            return;
        }

        var param = {
            'password1': password1,
            'password2': password2,
            'nick': nick,
            'phone_num': phone_num,
            'zipcode': zipcode,
            'addr1': addr1,
            'addr2': addr2,
        };
        $.ajax({
            type: "POST",
            url: "/api/member/modify",
            cache: false,
            async: false,
            data: param,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if(response.code == 200) {
                    alert("회원 정보가 수정되었습니다.");
                    location.href('/main');
                } else {
                    $("#join_warning").html(response.message);
                    return false;
                }
            },
            error: function (response, status, error) {
                console.log("%o", response);
                $("#join_warning").html(response.message);
                return false;
            }

        });
        return false;
    }

    $(function () {
        $("#btn_dup_nick_check").click(function () {
            var nick = $("input[name=nick]").val();

            if (nick == "") {
                $("#nick_join_warning").html("중복체크할 닉네임을 입력하세요.");
                return;
            }

            var param = {
                'nick': nick
            };

            $.ajax({
                type: "POST",
                url: "/api/member/dup_nick_check",
                cache: false,
                async: false,
                data: param,
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    if (response.code == 200) {
                        $("input[name=dup_nick_check]").val("1");
                        $("#nick_join_warning").html("이 닉네임은 사용하실 수 있습니다.");
                        $("input[name=nick]").attr("readonly", true);

                    } else {
                        $("input[name=dup_nick_check]").val("0");
                        $("#nick_join_warning").html(response['message']);
                        return;
                    }
                }
            });
        });

    });



</script>
<script>
    window.addEventListener("load", function () {
        document.querySelector(".main_banner_logo").addEventListener("click", function (e) {
            location.href='/main'
        }, false);

    }, false);

</script>

<?php include_once __DIR__ . "/footer.php" ?>


