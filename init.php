<?php
require_once dirname(__FILE__)."/vendor/autoload.php";

// Show all errors and warnings

ini_set('display_errors', 'On');
error_reporting(E_ALL);


$config = json_decode(file_get_contents(__DIR__ . '/config.json'), true);
if (empty($config['client_id']) || empty($config['client_secret'])) {
    throw new Exception(
        'We could not locate your client id or client secret in "' . __DIR__ . '/config.json". Please create one, ' .
        'and reference config.json.example'
    );
}

return $config;