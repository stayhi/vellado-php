<?php




?>
<!DOCTYPE html>
<html lang="ko">
<head>
<body>
<script>
    alert("영상 다시보기 개선중입니다. 더 좋은 모습으로 찾아오겠습니다.");
    location.href='/main';
</script>
</body>
</html>
<?php

if(true) exit;

include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL ^ E_NOTICE);

session_start();


$db = new SimpleDB($pdo_db);

if (!$_GET['page']) {
    $page = 1;
} else {
    $page = intval($_GET['page']);
}

$vimeo_id = intval($_GET['vod_id']);

//main 비디오 리스트 조회
if($vimeo_id) {
    $video = $db->row("SELECT * FROM vimeo_video_list WHERE id = '{$vimeo_id}'");
} else {
    $video = $db->row("SELECT * FROM vimeo_video_list ORDER BY id DESC LIMIT 1");
}
$embed_script = $video['embed_script'];

$rows = $db->query("SELECT * FROM vimeo_video_list ORDER BY id DESC LIMIT 0, 12");

$list = [];

foreach ($rows as $row) {
    $list[] = [
        'id' => $row['id'],
        'title' => $row['title'],
        'description' => $row['description'],
        'release_datetime' => $row['release_datetime'],
        'datetime' => date('Y.m.d', strtotime($row['release_datetime'])),
        'link' => $row['link'],
        'thumbnail' => $row['thumbnail2'],
    ];
}

?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <meta name="naver-site-verification" content="f47bc1e19f2ef9f8ec34e16e88a11c1f99840b73" />
    <title>VELLADO</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#aa15ff">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="/resources/css/common.css">
    <script src="/resources/js/common.js"></script>
    <script src="https://player.vimeo.com/api/player.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://unpkg.com/@webcreate/infinite-ajax-scroll@^3.0.0-beta.6/dist/infinite-ajax-scroll.min.js"></script>

    <script>

        window.addEventListener("resize", changeVideoSize);


        //창 사이즈 변경 시 비디오 사이즈 16:9에 맞게 변경
        var winWidth = window.innerWidth;
        var winHeight = window.innerHeight;
        var firstResize = true;
        function changeVideoSize(){
            if(winWidth == window.innerWidth && winHeight == window.innerHeight && !firstResize){
                console.log("return resize");
                return;
            }


            if(isMobile()){
                console.log("video resize mobile");
                var width = document.getElementById('player_div').offsetWidth;
                document.getElementById("player_div").style.height = (width * 9 / 16) + "px"; //16:9 비율로 height 조절
            }
            else{
                console.log("video resize pc");
                var width = document.getElementById('player_div').offsetWidth;
                document.getElementById("player_div").style.height = (width * 9 / 16) + "px"; //16:9 비율로 height 조절
            }

            firstResize = false;
            // changeChatSize();
            // chatScrollBottom();

        }

        var vimeoPlayer;

        window.addEventListener("load", function(){
            changeVideoSize();
        });

    </script>
</head>
<body>

<?php include __DIR__ . "/top_menu.inc.php"; ?>

<!-- 메인 컨텐츠 -->
<div class="main_container">

    <div class="main_content_div">
        <!-- 메뉴 -->
        <?php include_once __DIR__."/menu.inc.php"; ?>

        <!-- 상단 플레이어 등-->
        <div class="video_container" id="video_container">
            <div class="video_left_box">
                <div class="player_div" id="player_div">
                    <iframe src="https://player.vimeo.com/video/<?= $video['id'] ?>?app_id=185495" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" title="[안정권 썰방 H/L] 대한민국 부동산 대책에 대한 안정권의 일갈!" data-ready="true"></iframe>
                </div>

                <div class="box">
                    <div class="date"><?= substr($video['upload_datetime'], 5, 6) ?></div>
                    <div class="count">- <img src="/resources/images/icon-view.png"></div>
                </div>
            </div>
            <div class="video_right_box">
                <div class="title"><?= $video['title'] ?></div>
                <div class="body"><?= $video['description'] ?></div>
            </div>
        </div>


        <!--검색 박스-->
        <form name="searchForm" method="POST">
            <div class="main_search_box">
                <input type="text" name="keyword" placeholder="검색어를 입력하세요." autocomplete="off">
                <img src="/resources/images/btn-search.png">
            </div>
        </form>

        <div class="break"></div>
        <!-- VOD 썸네일 리스트 -->
        <div class="vod_list_container">
            <div class="vod_list_box" id="vod_list_box">
                <?php foreach ($list as $index => $row) : ?>
                    <div class="vod_thumb_box">
                        <div class="thumbnail"><a href="javacript:;" rel="<?= $row['id'] ?>"><img src="<?= $row['thumbnail'] ?>"></a></div>
                        <div class="title"><a href="javacript:;" rel="<?= $row['id'] ?>"><?= $row['title'] ?></a></div>
                        <div class="body"></div>
                        <div class="date_box">
                            <div class="date"><?= $row['datetime'] ?></div>
                            <div class="count">- <img src="/resources/images/icon-view.png"></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<script>
    var cur_page = 1;
    $(function () {
        $("#vod_list_box").on("click", "a", function (event) {
            event.preventDefault();

            var video_id = $(this).attr("rel");

            $.ajax({
                type: "POST",
                url: "/ajax.video.php",
                data: {cmd: 'embed', video_id: video_id},
                dataType: 'json',
                success: function (data) {
                    console.log("%o", data);
                    if (data["returnCode"] == "200") {//성공
                        //location.reload();
                        $("#video_container").html(data['content']);
                        $("html, body").animate({scrollTop: '300px'}, 680);
                        if (isMobile()) {
                            console.log("video resize mobile");
                            var width = document.getElementById('player_div').offsetWidth;
                            document.getElementById("player_div").style.height = (width * 9 / 16) + "px"; //16:9 비율로 height 조절
                        } else {
                            console.log("video resize pc");
                            var width = document.getElementById('player_div').offsetWidth;
                            document.getElementById("player_div").style.height = (width * 9 / 16) + "px"; //16:9 비율로 height 조절
                        }
                    } else {
                        //alert(data["errorMsg"]);
                    }
                }
            });
        });

        $("form[name=searchForm").submit(function (event) {
            event.preventDefault();
            var keyword = $("input[name=keyword]").val();

            window.cur_page = 0;
            $("#vod_list_box").html("");
            $.ajax({
                type: "POST",
                url: "/ajax.video.php",
                data: {cmd: 'video_list', keyword: keyword, page: window.cur_page + 1},
                dataType: 'json',
                success: function (data) {
                    console.log("%o", data);
                    if (data["returnCode"] == "200") {//성공
                        $("#vod_list_box").append(data['content']);
                        window.cur_page = parseInt(data['page']);
                    } else {
                        //alert(data["errorMsg"]);
                    }
                }
            });

        });
    });

    $(window).scroll(function () {

        var scroll_gap =  ($(document).height() - $(this).height()) - $(this).scrollTop();
        console.log("doc.height=%d - this.height=%d = %d, this.scrollTop=%d, scroll_gap=%d", $(document).height(), $(this).height(), $(document).height() - $(this).height(), $(this).scrollTop(), scroll_gap);

        if (scroll_gap < 100) {
            var keyword = $("input[name=keyword]").val();
            $.ajax({
                type: "POST",
                url: "/ajax.video.php",
                data: {cmd: 'video_list', keyword: keyword, page: window.cur_page + 1},
                dataType: 'json',
                success: function (data) {
                    console.log("%o", data);
                    if (data["returnCode"] == "200") {//성공
                        $("#vod_list_box").append(data['content']);
                        window.cur_page = parseInt(data['page']);
                    } else {
                        //alert(data["errorMsg"]);
                    }
                }
            });
        }
    });

</script>
<script>
    window.addEventListener("load", function () {
        document.querySelector(".main_banner_logo").addEventListener("click", function (e) {
            location.href='/main'
        }, false);

    }, false);

</script>
<?php include_once __DIR__."/footer.php" ?>


