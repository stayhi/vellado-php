<?php

use App\Router;
use App\SimpleDB;
use App\Controllers\HelloController;
use App\Controllers\MemberController;
use App\Controllers\BoardController;
use App\Controllers\PaymentController;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../lib/env.lib.php';
require_once __DIR__ . '/../board/common.php';
require_once __DIR__ . '/../board/lib/register.lib.php';
require_once __DIR__ . '/../board/lib/thumbnail.lib.php';

if (!isset($pdo_db)) {
    include_once(__DIR__ . "/../lib/_dbconnect_gzss.php");
}

$db = new SimpleDB($pdo_db);

$config = [];

$config['root_dir'] = dirname(__DIR__);
$config['data_root'] = $config['root_dir'] . "/data";

define('ROOT_DIR', $config['root_dir']);
define('DATA_ROOT', $config['data_root']);

//--------------------------
// create route
//--------------------------
$router = new Router();

$router->addConfig('environment', ENVIRONMENT);
$router->addConfig('db', $db);
$router->addConfig('pdo_db', $pdo_db);
$router->addConfig('root_dir', $config['root_dir']);
$router->addConfig('data_root', $config['data_root']);

switch (ENVIRONMENT) {
    case 'PRODUCTION' :
        //redis config
        $router->addConfig('redis', [
            'ip' => '127.0.0.1',
            'port' => 6379,
            'db' => 1
        ]);
        //websocket config
        $router->addConfig('ws_chat_url', "wss://www.vellado.com:8088/");
        break;

    case 'DEV' :
    case 'LOCAL' :
        //redis config
        $router->addConfig('redis', [
            'ip' => '127.0.0.1',
            'port' => 6379,
            'db' => 1
        ]);
        //websocket config
        $router->addConfig('ws_chat_url', "wss://127.0.0.1.com:8088/");
        break;
    default:
        http_response_code(500);
        die('environment invalid');
}


// Custom 404 Handler
$router->set404(function () use ($router) {
    return $router->withJson(['message' => '404 Not Found'], 404);
});


$router->mount('/test', function () use ($router, $db) {

    $controller = new HelloController($router);
    $router->get('/', function () {
        echo 'test3';
    });

    // will result in '/movies/id'
    $router->get('/(\d+)', function ($id) use ($router, $controller) {
        $data = $controller->test3($id);
        return $router->withJson(['message' => 'OK', 'code' => 200, 'data' => $data], 200);

    });

    //환경설정 체크
    $router->get("/config", function () use ($router, $controller) {
        if (is_office_ip()) {
            return $router->withJson(['message' => 'OK', 'code' => 200, 'config' => $router->getConfig()], 200);
        } else {
            return $router->withJson(['permision deny' => 'FAIL', 'code' => 403, 'data' => ''], 403);
        }
    });

    $router->get('/([a-zA-Z]+)', function ($id) use ($router, $controller) {
        $data = $controller->test4($id);
        return $router->withJson(['message' => 'OK', 'code' => 200, 'data' => $data], 200);
    });

});

$router->mount('/board', function () use ($router, $db) {
    $controller = new BoardController($router);

    $router->get('/list', function () use ($controller) {
        return $controller->boardList($_GET);
    });

    $router->get('/([0-9a-zA-z_\-]+)', function ($bo_table) use ($controller) {
        $page = $_GET['page'];
        $rows = $_GET['rows'];
        return $controller->getList($bo_table, $page, $rows);
    });

    $router->get('/([0-9a-zA-Z\-_]+)/([0-9]+)', function ($bo_table, $wr_id) use ($controller) {
        $page = $_GET['page'];
        $rows = $_GET['rows'];
        return $controller->getView($bo_table, $wr_id);
    });
});

//인증후 접근 가능한 api
//$router->before('GET|POST|DELETE', '/members/.*|/members', function () use ($router) {
//    if (!isset($_SESSION['ss_mb_id'])) {
//        echo $router->withJson(['message' => '로그인후 이용할 수 있습니다.', 'code' => 403], 403);
//        exit;
//    }
//});


$router->mount('/member', function () use ($router, $db) {
    $controller = new MemberController($router);

    $router->post('/login', function () use ($controller) {
        return $controller->login($_POST);
    });

    $router->post('/logout', function () use ($controller) {
        return $controller->logout();
    });

    $router->post('/register', function () use ($controller) {
        return $controller->register($_POST);
    });
    $router->post('/modify', function () use ($controller) {
        return $controller->modify($_POST);
    });

    $router->post('/upload_image', function () use ($controller) {
        return $controller->uplaodProfileImage();
    });

    $router->post('/dup_nick_check', function () use ($router, $controller) {
        $nick = filter_input(INPUT_POST, "nick");
        $reg_mb_id = $_SESSION['ss_mb_id'];
        return $controller->checkDuplicationNick($nick, $reg_mb_id);
    });

    $router->post('/dup_id_check', function () use ($router, $controller) {
        $mb_id = filter_input(INPUT_POST, "userid");
        return $controller->checkDuplicationUserid($mb_id);
    });

});


$router->mount('/payment', function () use ($router, $db) {
    $controller = new PaymentController($router);

    $router->post('/callback', function () use ($controller) {
        return $controller->callback($_POST);
    });


});



$router->run();