<?php
use App\Router;
use App\SimpleDB;
use App\Controllers\BoardController;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/lib/env.lib.php';
require __DIR__ . '/board/common.php';

ini_set('display_errors', '0');

if (!isset($pdo_db)) {
    include_once(__DIR__ . "/lib/_dbconnect_gzss.php");
}

$db = new SimpleDB($pdo_db);


if (!$_GET['page']) {
    $page = 1;
} else {
    $page = intval($_GET['page']);
}


$sql = "select * 
from g5_write_notice 
where wr_is_comment=0 
order by wr_id desc 
limit 0, 5
";
$rows = $db->query($sql);

//공지사항 리스트 조회
//$rows = $db->query("SELECT * FROM notice_board ORDER BY id DESC LIMIT 0, 9");

$list = [];

foreach ($rows as $row) {

    $profile_img_path = __DIR__ . "/data/member_image/" . substr($row['mb_id'], 0, 2) . "/" . $row['mb_id'] . ".gif";
    $profile_img_path50 = __DIR__ . "/data/member_image/" . substr($row['mb_id'], 0, 2) . "/" .$row['mb_id'] . "_50x50.gif";

    if(file_exists($profile_img_path50)) {
        $profile_img_url = "/data/member_image/" . substr($row['mb_id'], 0, 2) . "/" . $row['mb_id'] . "_50x50.gif";
    } else {
        $profile_img_url = "/resources/images/profile-un.png";
    }

    $list[] = [
        'id' => $row['wr_id'],
        'userid' => $row['mb_id'],
        'username' => $row['wr_name'],
        'subject' => $row['wr_subject'],
        'content' => $row['wr_content'],
        'hit_count' => $row['wr_hit'],
        'create_datetime' => $row['wr_datetime'],
        'datetime' => date('Y.m.d', strtotime($row['wr_datetime'])),
        'profile_img_url' => $profile_img_url
    ];


}

?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <title>VELLADO</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#aa15ff">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta name="naver-site-verification" content="f47bc1e19f2ef9f8ec34e16e88a11c1f99840b73" />
    <link rel="stylesheet" type="text/css" href="resources/css/common.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>
<body oncontextmenu='return false' onselectstart='return false' ondragstart='return false' >

<!--최상단 배너-->
<?php include_once __DIR__."/top_menu.inc.php";?>
<!-- 메인 컨텐츠 -->
<div class="main_container">


    <div class="main_content_div">
        <!-- 메뉴 -->
        <?php include __DIR__ . "/menu.inc.php" ?>

        <div class="board_container">


            <?php foreach ($list as $row) : ?>
                <div class="board_item">
                    <div class="board_header">
                        <div class="title"><?= $row['subject'] ?></div>
                        <div class="date"><img src="<?=$row['profile_img_url']?>"> <?= $row['datetime'] ?></div>
                    </div>

                    <div class="body"><?= $row['content'] ?></div>
                </div>
            <?php endforeach; ?>

        </div>


    </div>

</div>
<script>
    window.addEventListener("load", function () {
        document.querySelector(".main_banner_logo").addEventListener("click", function (e) {
            location.href='/main'
        }, false);

    }, false);

</script>
<?php include_once __DIR__."/footer.php" ?>