

###  채팅 서비스 
* 운영 서비스 경로 `/var/www/chat_server/`
* 개발 서비스 경로 `/var/www/dev_chat_server/`

#### 채팅 서비스 설정
* 채팅 서비스으 기본 설정은 내부 변수로 유지하고 있으며, db 설정만 가능합니다.
* `.env`  파일을 참조
```env
chatdb.username=gzss
chatdb.password=gzss1980
chatdb.url="jdbc:mysql://127.0.0.1:3306/chatdb?useUnicode=true&characterEncoding=utf8&connectionCollation=utf8mb4_general_ci"
```

#### 채팅 서비스 운영  
* `서버를 리부팅 한 경우 반드시 채팅 서비스를 별도로 시작해 주어야 합니다.`
* 채팅 서비스 중지 `./stop.sh`
* 채팅 서비스 시작 `./start.sh`

#### 그외 채팅 서비스 관련
* 채팅에서 슈퍼챗은 chatdb.pchat_donation 테이블을 참조합니다.
* 결제가 진행되면, 해당 테이블에 값을 입력하세요. 
    * 1초마다 is_chat_view='N' 를 조회합니다.
    * 해당 row 가 조회되면, 채팅 메세지로 출력합니다.(view_second 동안 노출)
    * 출력후 is_chat_view='Y'로 업데이트합니다.
