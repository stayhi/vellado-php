server {
    listen 443 ssl;
    server_name vellado.com;
    return 301 https://www.vellado.com$request_uri;

    ssl_certificate /etc/letsencrypt/live/vellado.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/vellado.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {

        listen [::]:443 ssl ipv6only=on; # managed by Certbot
        listen 443 ssl; # managed by Certbot
        ssl_certificate /etc/letsencrypt/live/vellado.com/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/vellado.com/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot


        # include snippets/snakeoil.conf;

        index index.php index.html index.htm index.nginx-debian.html;

        server_name www.vellado.com;
        root /var/www/wuutube.com;

        location / {
                try_files $uri $uri.html $uri/ @extensionless-php;
        }

        location ~ ^/data/member_image/.+\.(?:gif|jpe?g|png)$ {
                expires 60s;
                add_header Pragma public;
                add_header Cache-Control "public";
                try_files $uri 404;
        }

        #location /api {
        #       rewrite ^/api/(.*)$ /api/index.php?$query_string break;
        #}

        location /api {
             try_files $uri /api/index.php$is_args$args;
        }

        # pass PHP scripts to FastCGI server
        #
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
                #fastcgi_pass 127.0.0.1:9000;
        }

        location @extensionless-php {
                rewrite ^/vod/([0-9]+)$ /vod.php?vod_id=$1 last;
                rewrite ^(.*)$ $1.php last;
        }

        location /chat {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $host;
                proxy_connect_timeout     120;
                proxy_send_timeout        900;
                proxy_read_timeout        900;
                proxy_headers_hash_max_size 1024;
                proxy_headers_hash_bucket_size 128;
                proxy_pass http://127.0.0.1:18088/chat;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header Host $http_host;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "Upgrade";
        }
}

server {
    if ($host = vellado.com) {
        return 301 https://www.vellado.com$request_uri;
    } # managed by Certbot

    if ($host = www.vellado.com) {
        return 301 https://www.vellado.com$request_uri;
    } # managed by Certbot


    listen 80 default_server;
    listen [::]:80 default_server;

    server_name _ www.vellado.com vellado.com;
    return 404; # managed by Certbot
}
