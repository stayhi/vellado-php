#!/bin/sh
export LANG=en_US.UTF-8
#JAVA_OPTS="-Dlog4j.configuration=log4j.xml"
jarfile=bongchat_server-1.0.1-SNAPSHOT-all.jar
SERVER_INSTANCE=prd
reldir=`dirname $0`
cd $reldir
APP_DIR=`pwd`
PID=`cat ${APP_DIR}/program.pid`
state=`ps -ef | grep $PID | grep -v grep | wc -l`
if [ $state -ge 1 ]
then
   echo "Chat Server is already running... PID : $PID"
else
   echo "Starting PushServer..."
   java -server -Xms256m -Xmx1024m  $JAVA_OPTS -Dserver_instance=${SERVER_INSTANCE} -jar $jarfile $* &
   echo $! > program.pid
fi