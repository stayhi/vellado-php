#!/bin/bash
DATE=$(date +%Y%m%d%H%M)
reldir=`dirname $0`
cd $reldir
APP_DIR=`pwd`
PIDFILE="${APP_DIR}/program.pid"
PID=$(cat $PIDFILE)

kill -9 $PID