### 기본 서비스 환경
* Web Server : `nginx`
* PHP : `7.4`
* MySQL : `5.7`
* Chat : `Java Application`
* JDK : `OpenJDK 11`
* Redis : 


### 디렉토리 구조
* 운영 서비스 경로 `/var/www/wuutube.com`
```
├── _document_       문서 
├── _publish_        디자인 문서
├── _test_           테스트 진행 문서
├── api              api 서비스 경로
├── board            관리자 기능(gnuboard admin)
├── bootpay          부트페이용 class
│   └── Rest
├── crontab          배치(cron)용 PHP 실행 파일
├── data             파일 업로드 경로(사용자 프사, etc)
├── lib              라이브러리용
├── player.vellado   라이브 동영상 플레이어
├── resources        웹페이지 정적 파일(img, css, js)
├── src              API용 class 파일 
│   └── Controllers
└── vendor           PHP composer 파일  
```

* 개발 서비스 경로 `/var/www/dev.vellado.com`

### 소스 변경 및 추가 개발
* /var/www/dev.vellado.com 에 소스 작업을 합니다.
* https://dev.vallado.com 에서 변경사항을 수행합니다.
* 운영에 배포하기 위해선 `/home/ubuntu/source_sync_deploy.sh` 를 실행합니다. 

### DB 스키마 정보
* `table_schema.sql` 파일 참조


### nginx 설정
* `_document/nginx` 폴더 참조


### crontab 설정
```
## 매분만다 최근 데이타 싱크 
* * * * * sudo -u www-data php /var/www/wuutube.com/crontab/vimeo.php
## 매시간 3분마다 전체 싱크 
03 * * * * sudo -u www-data php /var/www/wuutube.com/crontab/vimeo_full.php

#let's encryption renew
0 06 1 * * /usr/bin/certbot renew --renew-hook="sudo systemctl restart nginx"

```
* 현재 vimeo를 사용하지 않음으로, 해당 싱크는 불필요합니다.
* 무료 인증서 업데이트는 매 월요일 06시에 갱신됩니다.

### 채팅 서비스 실행 
* `_document/chatting_guide.md` 참조


### PHP 설정
* session 설정을 다음과 같이 처리합니다.
* 채팅 서버와 session을 연동되어 있습니다.
* session store 는 Redis 를 사용합니다.
* `vi /etc/php/7.4/fpm/php.ini `
```php.ini
[Session]
session.save_handler = redis
session.save_path = "tcp://127.0.0.1:6379"
session.serialize_handler = php_serialize
```

### DB 설정
* db 계정 생성
```sql
CREATE USER 'gzss'@'localhost' IDENTIFIED BY 'gzss1980';
GRANT ALL PRIVILEGES ON *.* TO 'gzss'@'localhost' WITH GRANT OPTION;

CREATE USER 'gzss'@'127.0.0.1' IDENTIFIED BY 'gzss1980';
GRANT ALL PRIVILEGES ON *.* TO 'gzss'@'127.0.0.1' WITH GRANT OPTION;
```
* DB 생성
```sql
CREATE DATABASE wuutube CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```
* DB restore
```shell
myql -uroot wuutube < lotte_20210908_01.sql
```