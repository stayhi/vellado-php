create user 'gzss'@'localhost' identified by 'gzss1980';
create user 'gzss'@'127.0.0.1' identified by 'gzss1980';

create database wuutube character set utf8mb4 collate utf8mb4_general_ci;
create database chatdb character set utf8mb4 collate utf8mb4_general_ci;

grant all privileges on wuutube.* to 'gzss'@'localhost';
grant all privileges on wuutube.* to 'gzss'@'127.0.0.1';

grant all privileges on chatdb.* to 'gzss'@'localhost';
grant all privileges on chatdb.* to 'gzss'@'127.0.0.1';

flush privileges;


### 주요 테이블 스키마 정보

CREATE TABLE `vimeo_video_list` (
  `no` int(11) NOT NULL AUTO_INCREMENT COMMENT '순번 ',
  `id` bigint(20) NOT NULL,
  `uri` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL COMMENT '동영상 설명',
  `title` varchar(500) DEFAULT NULL COMMENT '동영상 이름(name)',
  `link` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT '0' COMMENT '비디오 시간(초단위)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `embed_script` varchar(1000) DEFAULT NULL,
  `thumbnail1` varchar(255) DEFAULT NULL,
  `thumbnail2` varchar(255) DEFAULT NULL,
  `thumbnail3` varchar(255) DEFAULT NULL,
  `upload_datetime` datetime DEFAULT NULL,
  `resource_key` varchar(255) DEFAULT NULL,
  `json_data` json DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`no`),
  UNIQUE KEY `video_id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='비디오 목록'
;



## 사이트 기본 환경설정 정보
CREATE TABLE `gzss_config` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `live_streaming_url` varchar(500) DEFAULT NULL COMMENT '(사용안함)',
  `support_link` varchar(500) DEFAULT NULL COMMENT '구독안내 링크 ',
  `youtube_id` varchar(100) DEFAULT NULL COMMENT '유튜브 라이브 아이디(사용안함)',
  `main_video_id` int(11) DEFAULT NULL COMMENT 'vod 메인에 노출할 비디오 아이디 정보 ',
  `youtube_description` text COMMENT '유튜브 방송 설명',
  `youtube_title` varchar(1000) DEFAULT NULL COMMENT '메인 유튜브 라이브 설명(사용안함)',
  `update_datetime` datetime DEFAULT NULL COMMENT '설정 변경 일자 ',
  `vimeo_id` varchar(100) DEFAULT NULL COMMENT 'live에 노출될 vimeo아이디(사용안함)',
  `vimeo_url` varchar(255) DEFAULT NULL COMMENT '라이브 동영상 url ',
  `vimeo_title` varchar(1000) DEFAULT NULL COMMENT '라이브에 노출될 vimeo_title',
  `vimeo_description` text COMMENT '라이브에 노출될 설명 ',
  `vimeo_name` varchar(50) DEFAULT NULL COMMENT '라이브 표시 사용자명 ',
  `vimeo_date` varchar(50) DEFAULT NULL COMMENT '라이브 표시 날짜 ',
  `vimeo2_id` varchar(100) DEFAULT NULL COMMENT '두버째 라이브 아이디 ',
  `vimeo2_url` varchar(255) DEFAULT NULL COMMENT 'vimeo 두번째 라이브 주소 ',
  `vimeo2_title` varchar(1000) DEFAULT NULL,
  `vimeo2_description` text,
  `vimeo2_name` varchar(50) DEFAULT NULL,
  `vimeo2_date` varchar(50) DEFAULT NULL,
  `schedule` json DEFAULT NULL COMMENT '스케쥴 정보 ',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4
;


## 부트페이 결제 내역
CREATE TABLE `bootpay_payment` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_id` varchar(50) NOT NULL DEFAULT '' COMMENT '부트페이 트랜젝션 아이디 ',
  `status` varchar(1) NOT NULL COMMENT '부트페이 결제 상태, 1: 결제완료 ',
  `price` varchar(12) NOT NULL DEFAULT '0' COMMENT '결제금액 ',
  `pg` varchar(12) DEFAULT NULL COMMENT '부트페이 pg명(웰컴페이먼츠)',
  `pg_name` varchar(50) DEFAULT NULL COMMENT '부트페이 pg한글명 ',
  `method` varchar(50) DEFAULT NULL COMMENT '결제방법',
  `method_name` varchar(50) DEFAULT NULL COMMENT '결제방법 한글',
  `order_id` varchar(50) DEFAULT NULL COMMENT '주문 아이디(임의생성)',
  `card_name` varchar(50) DEFAULT NULL COMMENT '카드사명',
  `mb_id` varchar(50) DEFAULT NULL COMMENT '결제 회원 아이디 ',
  `mb_nick` varchar(50) DEFAULT NULL COMMENT '사용자 닉네임 ',
  `channel_id` varchar(50) DEFAULT NULL COMMENT '메세지 노출 채팅 채널 아이디 ',
  `message` varchar(1000) DEFAULT NULL COMMENT '벨라쳇 도네이션 메세지 ',
  `item_name` varchar(255) DEFAULT NULL COMMENT '결제 상품명(벨라챗 선택 상품)',
  `create_datetime` datetime DEFAULT NULL COMMENT '생성일자 ',
  `update_datetime` datetime DEFAULT NULL COMMENT '변경일자 ',
  `cancel_datetime` datetime DEFAULT NULL COMMENT '취소일자 ',
  `complete_datetime` datetime DEFAULT NULL COMMENT '결제 완료일자 ',
  PRIMARY KEY (`no`),
  KEY `receipt_id` (`receipt_id`),
  KEY `mb_id` (`mb_id`),
  KEY `create_datetime_index` (`create_datetime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='결제 내역'





### 채팅 테이블 설명

CREATE TABLE `pchat_config` (
  `no` int(11) NOT NULL COMMENT '번호',
  `room_lobby_notice` varchar(255) DEFAULT NULL COMMENT '방채팅 로비 한줄공지',
  `room_same_ip_check` varchar(1) NOT NULL DEFAULT 'N' COMMENT '방채팅 입장시 아이피 체크 여부',
  `room_same_ip_10` int(11) NOT NULL DEFAULT '0' COMMENT '10초간 동일 아이피 체크 개수',
  `room_same_ip_60` int(11) NOT NULL DEFAULT '0' COMMENT '60초간 동일 아이피 체크 개수',
  `fixed_room_mode` varchar(1) NOT NULL DEFAULT 'N' COMMENT '고정방 모드(Y:채팅방이 자동삭제되지 않음)',
  `filter_word` text COMMENT '채팅방 필터링 단어',
  `abnormal_disconnect_user_count` int(11) NOT NULL DEFAULT '0' COMMENT '초당 비정상 접속종료수치',
  `abnormal_disconnect_room_user_count` int(11) NOT NULL DEFAULT '0' COMMENT '초당 비정속 접속종료수치(방채팅)',
  `abnormal_disconnect_auto_protection_on` varchar(1) DEFAULT 'N' COMMENT '비정상 접속종료 발생시 프로텍션 자동 켜짐',
  `web_hook_url` varchar(255) DEFAULT NULL COMMENT '웹훅 url',
  `allow_chat_level` int(11) DEFAULT '3' COMMENT '채팅 입력 허용 레벨, 0이면 손님 채팅 가능',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='채팅 환경 설정'
;

### 공개 채팅 채널별 정보
CREATE TABLE `pchat_channel_info` (
  `no` int(5) NOT NULL AUTO_INCREMENT,
  `channel_id` varchar(50) NOT NULL,
  `channel_name` varchar(255) NOT NULL,
  `userid` varchar(50) NOT NULL,
  `max_user` int(11) unsigned NOT NULL DEFAULT '0',
  `operators` varchar(2000) NOT NULL,
  `is_guest_allow_chat` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '손님채팅 가능 여부',
  `cur_user` int(5) unsigned NOT NULL DEFAULT '0',
  `additional_user_count` int(5) unsigned NOT NULL DEFAULT '0' COMMENT '추가인원수',
  `prechat_log_size` int(11) NOT NULL DEFAULT '30',
  `intro` text,
  `update_time` datetime NOT NULL,
  `reg_time` datetime NOT NULL,
  `load_yn` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '시스템 로딩 여부',
  PRIMARY KEY (`no`),
  UNIQUE KEY `site_id` (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=379 DEFAULT CHARSET=utf8 COMMENT='채팅 채널별 정보 '




### 공개 채팅 내역
CREATE TABLE `pchat_chat_log` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '채팅 채널 아이디 ',
  `face_icon` varchar(50) DEFAULT NULL COMMENT '사용자 아이콘 ',
  `userid` varchar(255) NOT NULL COMMENT '사용자 아이디 ',
  `username` varchar(255) NOT NULL COMMENT '사용자명 ',
  `msg` text NOT NULL COMMENT '채팅 메세지 ',
  `style` varchar(255) DEFAULT NULL COMMENT '채팅 스타일. superchat:슈퍼챗 ',
  `msg_time` datetime NOT NULL COMMENT '메세지 입력시간 ',
  `ip` varchar(100) DEFAULT NULL COMMENT '사용자 아이피 ',
  PRIMARY KEY (`no`),
  KEY `site_id` (`channel_id`),
  KEY `userid` (`userid`),
  KEY `msg_time` (`msg_time`),
  KEY `ip` (`ip`)
) ENGINE=InnoDB AUTO_INCREMENT=385897 DEFAULT CHARSET=utf8 COMMENT='공개채팅로그'



### 채팅 패널티 유저
CREATE TABLE `pchat_penalty_user` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `channel_id` varchar(50) NOT NULL COMMENT '대상 채팅 채널 ',
  `userid` varchar(50) NOT NULL COMMENT '대상 사용자 아이디',
  `username` varchar(50) DEFAULT NULL COMMENT '대상 사용자명 ',
  `manager_id` varchar(50) DEFAULT NULL COMMENT '패너티를 준 관리자 아이디 ',
  `penalty_type` varchar(20) DEFAULT NULL COMMENT '채팅 패널티 타입(CHAT_OFF, BLOCK, ETC',
  `penalty_sub_type` varchar(50) NOT NULL COMMENT '패널티 서브 타입, CHAT_OFF 인 경우 off_type',
  `client_ip` varchar(200) DEFAULT NULL COMMENT '사용자 아이피 ',
  `create_datetime` datetime DEFAULT NULL COMMENT '생성일자 ',
  `update_datetime` datetime DEFAULT NULL COMMENT '수정일자 ',
  `release_datetime` datetime DEFAULT NULL COMMENT '차단해제일자',
  PRIMARY KEY (`no`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='채팅 패널티 유저'


### 채팅 후원내역
CREATE TABLE `pchat_donation` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `channel_type` varchar(1) DEFAULT 'N',
  `room_uid` varchar(20) DEFAULT NULL,
  `userid` varchar(20) DEFAULT NULL COMMENT ' 사용자 아이디',
  `username` varchar(50) DEFAULT NULL COMMENT '사용자명',
  `profile_img` varchar(255) DEFAULT NULL COMMENT '프로필 이미지 ',
  `tr_id` varchar(50) DEFAULT NULL COMMENT '트랜젝션 아이디 ',
  `amount_type` varchar(10) DEFAULT 'POINT' COMMENT '금액 타입 CASH, POINT, HEART, STICKER',
  `amount` int(11) DEFAULT NULL COMMENT '금액',
  `message_id` varchar(20) DEFAULT NULL COMMENT '메세지 아이디',
  `message` varchar(1000) DEFAULT NULL COMMENT '도네이션 메세지',
  `view_second` int(11) DEFAULT '0' COMMENT '도네이션 메세지 표시 시간(초단위)',
  `view_start_timestamp` timestamp NULL DEFAULT NULL COMMENT '도네이션 표시 시작시간(timestamp)',
  `create_datetime` datetime DEFAULT NULL,
  `is_chat_view` varchar(1) DEFAULT 'N',
  `view_datetime` datetime DEFAULT NULL,
  `is_delete` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`no`),
  KEY `room_uid_index` (`room_uid`),
  KEY `userid_index` (`userid`),
  KEY `create_datetime_index` (`create_datetime`),
  KEY `message_id_index` (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1380 DEFAULT CHARSET=utf8mb4 COMMENT='도네이션 내역'



## 도네이션 테스트 쿼리 .. 1분간 노출
INSERT INTO chatdb.pchat_donation
SET channel_type    = 'N',
    room_uid        = 'ALL',
    userid          = 'test05',
    username        = '테스트05',
    profile_img     = '/resources/images/profile-un.png',
    amount_type='VBOX',
    amount          = '2',
    message='다들 새해 복 많이 받으세요..',
    is_chat_view    = 'N',
    view_start_timestamp = now(),
    view_second = 30,
    create_datetime = now()
;







