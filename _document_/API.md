
### API 설명

* 응답 데이타는 json 
* 테스트는 `rest_test.http` 파일을 참조 바랍니다. (로그인 연동 예제 포함)
```
POST https://www.vellado.com/api/member/login
Content-Type: application/x-www-form-urlencoded; charset=UTF-8

mb_id=test01&mb_password=1234
```

* 응답 json 구조

##### 1. 회원가입
* request 샘플
```

```
* 요청 파라메타 설명
	
| Parameter | Description |
| ---------------|----------------|
| userid |사용자 아이디|
| password1 |사용자 비번|
| password2 |사용자 비번(확인)|

* response json

```

```

##### 2. 로그아웃

```
POST https://www.vellado.com/api/member/logout

```


##### 2. 로그인

```
POST https://www.vellado.com/api/member/logout

```



