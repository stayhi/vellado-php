<?php

use Vimeo\Vimeo;

require_once __DIR__ . "/../vendor/autoload.php";

require_once __DIR__ . "/SimpleDB.php";

class VimeoSync extends SimpleDB
{

    private $config = null;
    private $client_id = "4b6cb9ee64d7287d60b88b6c12d57d55797f277c";
    private $client_secret = "UF+t95NspsBxB1IDEmEQOU/unPlfYOtiXAyISyXiGTMMgzP8EUPLMFhoAwyEtWSedi18WbP7BdJe/EToZM34rO7iFZ1skAAaL1K6e94aC3k8SY1WjqY85Kb6eIeQqum4";
    private $access_token = "86cedc314f81897e9c5ce29707708ac1";

    private $vimeo = null;

    function __construct($pdo_db = null)
    {
        parent::__construct($pdo_db);

        $this->vimeo = new Vimeo($this->client_id, $this->client_secret, $this->access_token);
    }

    /**
     *
     * @return string
     * @throws Exception
     */
    function SyncVideo($page, $per_page)
    {

        try {

            $options = ['page' => $page, 'per_page' => $per_page];
            $result = $this->vimeo->request('/me/videos', $options, 'GET');

            $video_id_arr = [];
            foreach ($result['body']['data'] as $index => $row) {

                $video_id = trim($row['uri'], "/videos/");

                $this->insertOrUpdateVideo($video_id, $row);

                $video_id_arr[] = $video_id;
            }

            $min_id = min($video_id_arr);
            $max_id = max($video_id_arr);


            if ($min_id && $max_id) {
                $sql = "DELETE 
                        FROM vimeo_video_list 
                        WHERE id BETWEEN {$min_id} AND {$max_id}
                          AND id NOT IN (" . implode(",", $video_id_arr) . ")";

                $delete_count = $this->query($sql);
                echo sprintf("min=%d, max=%s, delete_count=%d \n", $min_id, $max_id, $delete_count);
            }


            if($page == 1) { //상단에 잘못 등록된것 삭제
                $sql = "DELETE 
                        FROM vimeo_video_list 
                        WHERE id > {$max_id}
                ";

                $this->query($sql);
            }


            //return $this->curl->get($this->get_url);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 전체 비디오 개수 조회
     * @return int 업로드 비디오 개수
     */
    public function getTotalVideos()
    {
        $result = $this->vimeo->request('/me/videos', [], 'GET');
        return intval($result['body']['total']);
    }


    function updateVideo($id, $data)
    {
        return false;
    }

    function insertOrUpdateVideo($id, $data)
    {

        echo "video id=$id \n";


        $row = $this->row("SELECT * FROM vimeo_video_list WHERE id = :id", compact('id'));


        $uri = $data['uri'];
        $description = $data['description'];
        $title = $data['name'];
        $link = $data['link'];

        $duration = $data['duration'];
        $width = $data['width'];
        $height = $data['height'];
        $embed_script = $data['embed']['html'];
        $thumbnail1 = $data['pictures']['sizes'][1]['link'];
        $thumbnail2 = $data['pictures']['sizes'][2]['link'];
        $thumbnail3 = $data['pictures']['sizes'][3]['link'];
        $upload_datetime = date("Y-m-d H:i:s", strtotime($data['created_time']));
        $release_datetime = date("Y-m-d H:i:s", strtotime($data['release_time']));
        $resource_key = $data['resource_key'];
        $json_data = json_encode($data);

        if (!$row['id']) { //미등록 비디오 정보

            $sql = "INSERT INTO vimeo_video_list
                    SET id              = :id,
                    uri             = :uri,
                    description     = :description,
                    title           = :title,
                    link            = :link,
                    duration        = :duration,    
                    width           = :width,
                    height          = :height,
                    embed_script    = :embed_script,
                    thumbnail1      = :thumbnail1,
                    thumbnail2      = :thumbnail2,
                    thumbnail3      = :thumbnail3,
                    upload_datetime = :upload_datetime,  
                    release_datetime = :release_datetime,    
                    resource_key    = :resource_key,
                    json_data       = :json_data,
                    create_datetime = now(),
                    update_datetime = now()
        ";


            $this->query($sql, compact(
                'id',
                'uri',
                'description',
                'title',
                'link',
                'duration',
                'width',
                'height',
                'embed_script',
                'thumbnail1',
                'thumbnail2',
                'thumbnail3',
                'upload_datetime',
                'release_datetime',
                'resource_key',
                'json_data'
            ));


        } else {
            $sql = "UPDATE vimeo_video_list
                        SET 
                        uri             = :uri,
                        description     = :description,
                        title           = :title,
                        link            = :link,
                        duration        = :duration,    
                        width           = :width,
                        height          = :height,
                        embed_script    = :embed_script,
                        thumbnail1      = :thumbnail1,
                        thumbnail2      = :thumbnail2,
                        thumbnail3      = :thumbnail3,
                        upload_datetime = :upload_datetime,  
                        release_datetime = :release_datetime,    
                        resource_key    = :resource_key,
                        json_data       = :json_data,
                        update_datetime = now()
                    WHERE id    =   :id
        ";


            $this->query($sql, compact(
                'id',
                'uri',
                'description',
                'title',
                'link',
                'duration',
                'width',
                'height',
                'embed_script',
                'thumbnail1',
                'thumbnail2',
                'thumbnail3',
                'upload_datetime',
                'release_datetime',
                'resource_key',
                'json_data'
            ));
        }
    }


}
