<?php

function save_youtube_thumbnail($youtube_id) {


    $youtube_thumbnail_url = "https://img.youtube.com/vi/{$youtube_id}/sddefault.jpg";

    $ch = curl_init($youtube_thumbnail_url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    $rawdata = curl_exec($ch);

    curl_close($ch);

    if (strlen($rawdata) < 1500) { //일부 없는 이미지가 있음. 아마 저해상도 동영상인 경우에 발생하는 걸로 추정됨.

        $youtube_thumbnail_url = "https://img.youtube.com/vi/{$youtube_id}/0.jpg";
        $ch = curl_init($youtube_thumbnail_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $rawdata = curl_exec($ch);

        curl_close($ch);

        if (strlen($rawdata) < 1000) {
            header('HTTP/1.0 404 Not Found');
            exit;
        }
    }

    $image = new Imagick();
    $image->readImageBlob($rawdata);

}

?>