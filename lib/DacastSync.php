<?php

include_once __DIR__ . "/CurlWrapper.php";

class DacastSync extends SimpleDB
{


    private $apikey = "";
    private $curl = null;
    private $config = null;
    private $base_url = "https://api.dacast.com";

    function __construct($pdo_db = null, $options = [])
    {
        if ($pdo_db) {
            parent::__construct($pdo_db);
        }

        print_r($options);
        if (isset($options['apikey'])) {
            $this->apikey = $options['apikey'];
        }
        $this->curl = new CurlWrapper();
    }

    function getRemoteData($url, $method = "GET")
    {

        try {
            if ($method == "GET") {
                return $this->curl->get($url);
            } else if ($method == "POST") {
                return $this->curl->post($url);
            }

        } catch (Exception $ex) {
            throw $ex;
        }

    }

    /**
     * curl로 vod 데이타를 크롤링후 db에 업데이트
     *
     * @return string
     * @throws Exception
     */
    function syncVOD()
    {

        try {

            $url = sprintf("%s/v2/vod?apikey=%s&_format=JSON", $this->base_url, $this->apikey);

            echo "request : {$url} \n";
            $remote_json_data = $this->getRemoteData($url);

            $result = json_decode($remote_json_data, true);

            $count = $result['totalCount'];

            echo "<xmp>";
            print_r($result);
            echo "</xmp>";

            foreach ($result['data'] as $index => $row) {
                $this->insertOrUpdateVOD($row['id'], $row);
            }


            //return $this->curl->get($this->get_url);
        } catch (Exception $ex) {
            throw $ex;
        }

    }


    /**
     * @param $id
     * @param $title
     * @param $filename
     * @param $filesize
     * @param $player_width
     * @param $player_height
     * @param $video_width
     * @param $video_height
     * @param $hds
     * @param $hls
     * @param $thumbnail_image
     * @param null $data
     */
    function insertVOD($id, $title, $filename, $filesize, $player_width, $player_height, $video_width, $video_height, $hds, $hls, $thumbnail_image, $data=null)
    {

        $sql = "INSERT INTO vod
                SET id              = :id,
                    title           = :title,
                    filename        = :filename,
                    filesize        = :filesize,
                    player_width    = :player_width,
                    player_height   = :player_height,
                    video_width     = :video_width,
                    video_height    = :video_height,
                    hds             = :hds,
                    hls             = :hls,
                    thumbnail_image = :thumbnail_image,
                    json_data       = :json_data,
                    create_datetime = now(),
                    update_datetime = now()
        ";

        $json_data = json_encode($data);
        $this->query($sql, compact(
            'id',
            'title',
            'filename',
            'filesize',
            'player_width',
            'player_height',
            'video_width',
            'video_height',
            'hds',
            'hls',
            'thumbnail_image',
            'json_data'
         ));

    }

    function updateVOD($id, $data)
    {
       return false;
    }

    function insertOrUpdateVOD($id, $data)
    {

        $row = $this->row("select * from vod where id = :id", compact('id'));

        $title = $data['title'];
        $filename = $data['filename'];
        $filesize = $data['filesize'];
        $hds = $data['hds'];
        $hls = $data['hls'];
        $thumbnails = $data['pictures']['thumbnail'];
        $player_width = $data['player_width'];
        $player_height = $data['player_height'];
        $video_width = $data['video_width'];
        $video_height = $data['video_height'];

        if (!$row['id']) { //미등록 경기임으로 insert

            $thumbnail_image = $this->createVODThumbnail($id, $thumbnails);
            $this->insertVOD($id, $title, $filename, $filesize, $player_width, $player_height,
                $video_width, $video_height, $hds, $hls, $thumbnail_image, $data);
        } else { //이미 등록된 vod으로 update todo 썸네일을 재생성 해야 함.
            //$this->updateGame($id, $data);
        }
    }


    /**
     * vod 이미지의 썸네일을 다운받아, 자체 썸네일을 생성한다.
     * @param $id vod id
     * @param $thumbnails array 원본 사이트의 썸네일 목록
     * @param array $options
     */
    private function createVODThumbnail($id, $thumbnails, $options = []) {
        //todo thumbnails 의 원본 이미지를 다운로드 받은후 필요한 사이즈로 썸네일을 생성한다.
        return "";
    }

    function save_remote_image($url, $dir)
    {
        $filename = '';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);

        curl_exec ($ch);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($http_code == 200) {
            $filename = basename($url);
            if(preg_match("/\.(gif|jpg|jpeg|png)$/i", $filename)) {
                $tmpname = date('YmdHis').(microtime(true) * 10000);
                $filepath = $dir;
                @mkdir($filepath, '0755');
                @chmod($filepath, '0755');

                // 파일 다운로드
                $path = $filepath.'/'.$tmpname;
                $fp = fopen ($path, 'w+');

                $ch = curl_init();
                curl_setopt( $ch, CURLOPT_URL, $url );
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, false );
                curl_setopt( $ch, CURLOPT_BINARYTRANSFER, true );
                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
                curl_setopt( $ch, CURLOPT_FILE, $fp );
                curl_exec( $ch );
                curl_close( $ch );

                fclose( $fp );

                // 다운로드 파일이 이미지인지 체크
                if(is_file($path)) {
                    $size = @getimagesize($path);
                    if($size[2] < 1 || $size[2] > 3) {
                        @unlink($path);
                        $filename = '';
                    } else {
                        $ext = array(1=>'gif', 2=>'jpg', 3=>'png');
                        $filename = $tmpname.'.'.$ext[$size[2]];
                        rename($path, $filepath.'/'.$filename);
                        chmod($filepath.'/'.$filename, '0644');
                    }
                }
            }
        }

        return $filename;
    }

}
