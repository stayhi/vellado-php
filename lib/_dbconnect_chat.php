<?php

include_once __DIR__."/env.lib.php";


if (is_development() == true) {
    $db_host = "localhost";
    $db_user = "gzss";
    $db_password = "gzss1980";
    $db_name = "chatdb";
} else if (is_beta() == true) {
    $db_host = "localhost";
    $db_user = "gzss";
    $db_password = "gzss1980";
    $db_name = "chatdb";
} else { //운영서비스 정보
    $db_host = "localhost";
    $db_user = "gzss";
    $db_password = "gzss1980";
    $db_name = "chatdb";
}

//docker 같은 로컬 개발환경인 경우..
if (is_local_development() == true) {
    $db_host = "localhost";
    $db_user = "gzss";
    $db_password = "gzss1980";
    $db_name = "chatdb";

}

$pdo_chat_db = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
$pdo_chat_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo_chat_db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$pdo_chat_db->exec("set session sql_mode = ''");

unset($db_host);
unset($db_user);
unset($db_password);
unset($db_name);
?>