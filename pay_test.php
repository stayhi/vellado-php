<?php
include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";

use App\BootpayApi;


//REST Application ID : 5feafd922fa5c2002103971f

//인증키 (Private Key) : s34nweY1M3rrnO0NSdQn0txn6agl8j/8Vk9UlQtZE/Q=

?>
<html>
<head>
    <title>PHP 테스트</title>
</head>
<body>
<script type="text/javascript">


</script>

<form method="post" name="LGD_PAYINFO" id="LGD_PAYINFO">
    <?php
    $instance = BootpayApi::setConfig('rest application_id', 'pk');
    $responseCancel = BootpayApi::cancel([
        'receipt_id' => 'receipt_id',
        'name' => 'name',
        'reason' => 'reason'
    ]);

    echo "status: {$responseCancel->status}, code: {$responseCancel->code}, message: {$responseCancel->message}";

    $responseConfirm = BootpayApi::confirm([
        'receipt_id' => 'receipt_id'
    ]);

    echo "status: {$responseConfirm->status}, code: {$responseConfirm->code}, message: {$responseConfirm->message}";
    ?>
</form>
</body>
</html>