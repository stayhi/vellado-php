<?php

include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL ^ E_NOTICE);


session_start();

if ($_SESSION['ss_mb_id']) {
    Header("Location: /main");
}

?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="resources/css/common.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <title>VELLADO</title>
</head>
<body oncontextmenu='return false' onselectstart='return false' ondragstart='return false' >
<?php include __DIR__ . "/top_menu.inc.php"; ?>
<!-- 메인 컨텐츠 -->
<div class="main_container">
    <div class="main_content_div">
        <div class="login_title">VELLADO 에 오신것을 환영합니다.</div>
        <div class="login_text temp">실시간 채팅이나 VOD를 시청하시려면 로그인이 필요합니다.</div>
        <div class="join_container">
         <form name="login_form" method="POST">
            <div class="join_input_box">
                <input type="text" name="userid" placeholder="아이디를 입력하세요" autocomplete="off">
            </div>
            <div class="join_input_box">
                <input type="password" name="password" placeholder="비밀번호를 입력하세요" autocomplete="off">
            </div>
            <div class="join_warning"  id="login_warning">로그인 아이디와 비밀번호를 입력하세요.</div>
        </div>
        </form>
        <div class="common_btn_container">
            <div class="btn" onclick="check_login()">
                <div class="text">로그인</div>
            </div>
        </div>
        <div class="common_btn_container margin">
            <div class="btn" onclick="go_join()">
                <div class="text">회원가입</div>
            </div>
        </div>
        <div class="flex_height">

        </div>
    </div>
</div>

<script>

    function check_login() {

        var userid = $("input[name=userid]").val();
        var password = $("input[name=password]").val();

        if (userid == "") {
            $("#login_warning").html("아이디를 입력하세요.");
            return;
        }

        if (password == "") {
            $("#login_warning").html("비밀번호를 입력하세요.");
            return;
        }

        var param = {
            'userid': userid,
            'password': password
        };

        $.ajax({
            type: "POST",
            url: "/api/member/login",
            cache: false,
            async: false,
            data: param,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.code == 200) {
                    alert('로그인 되었습니다.');
                    location.href = '/main'
                } else {
                    $("#login_warning").html(response['message']);
                    return;
                }
            }
        });
    }

    function go_join() {
        location.href = '/join';
    }
</script>
<script>
    window.addEventListener("load", function () {
        document.querySelector(".main_banner_logo").addEventListener("click", function (e) {
            location.href='/main'
        }, false);

    }, false);

</script>
<?php include_once __DIR__ . "/footer.php" ?>
