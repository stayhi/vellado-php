<?php

include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL ^ E_NOTICE);

session_start();

function str_rand($len) {
    $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $str = '';
    for ($max = strlen($chars) - 1, $i = 0; $i < $len; ++$i) {
        $str .= $chars[mt_rand(0, $max)];
    }
    return $str;
}

//손님용 아이디를 생성한다. tempval 이 있는 경우 랜덤으로 아이디를 생성할수 있도록 한다.
function generate_guest_id($ip, $tempval) {
    $peace = explode(".", $ip);
    $postfix = "";
    for ($i = 1; $i < count($peace); $i++) {
        $c = ($peace[$i] % 26) + 97;
        $postfix .= chr($c);
    }

    return $postfix . $tempval;
}

$client_ip = $_SERVER["REMOTE_ADDR"];
//$page_type = $_GET["page_type"];

//운영자에 의해 강퇴된 사용자는 접속금지시킨다.
//손님으로 접속한 사용자의 userid 를 생성한다.
//guest 에 접속 ip 뒷자리를 붙인다.
$ip = $_SERVER["REMOTE_ADDR"];
$peace = explode(".", $ip);
$uip = $peace[2] . $peace[3];

$is_prisoner = "N";
$prison_date = "";
$released_date = "";

$is_guest = true;
$is_chat_admin = false;

$allow_invite = "N";
$allow_date = "N";
$db = new SimpleDB($pdo_db);


if (!$_SESSION['ss_mb_id']) { //사용자 아이디가 없다면, 손님
    $session_id = "";
    $tempval = $_COOKIE["tempval"];
    if (!$tempval) {
        $tempval = str_rand(3);
        setcookie("tempval", $tempval, time() + 60 * 60 * 24 * 30, "/");
    }

    $ip = $_SERVER["REMOTE_ADDR"];
    $postfix = generate_guest_id($ip, $tempval);

    $userid = "GUEST_" . $postfix;

    $user_info = array();
    $chat_level = "1";

    $_SESSION['chat_userid'] = $userid;
    $_SESSION['chat_level'] = "1";
    $_SESSION['profile_img'] = "/resources/images/profile-un.png";
    $_SESSION["level_img"] = "1.gif";

    if ($_SESSION['chat_username']) {
        $name = $_SESSION['chat_username'];
    } else {
        $name = "손님_" . $postfix;
        $_SESSION['chat_username'] = $name;
    }

    $nick = $name;

} else {

    $member = $db->row("SELECT * FROM g5_member WHERE mb_id=:mb_id", ['mb_id' => $_SESSION['ss_mb_id']]);

    $is_guest = false;
    $chat_level = 3;

    if ($member['mb_level'] >= 7) {
        $chat_level = "10";
    }

    $_SESSION["chat_userid"] = $_SESSION['ss_mb_id'];
    $_SESSION["chat_username"] = $member['mb_nick'];
    $_SESSION["chat_level"] = $chat_level;
    $_SESSION["level_img"] = "3.gif";

    $nick = $member['mb_nick'];

    if (!$_SESSION["exp_level"]) {
        $_SESSION['exp_level'] = "100";
    }

    $profile_img_path = __DIR__ . "/data/member_image/" . substr($_SESSION['ss_mb_id'], 0, 2) . "/" . $_SESSION['ss_mb_id'] . ".gif";
    $profile_img_path50 = __DIR__ . "/data/member_image/" . substr($_SESSION['ss_mb_id'], 0, 2) . "/" . $_SESSION['ss_mb_id'] . "_50x50.gif";
    if (file_exists($profile_img_path50)) {
        $_SESSION['profile_img'] = "/data/member_image/" . substr($_SESSION['ss_mb_id'], 0, 2) . "/" . $_SESSION['ss_mb_id'] . "_50x50.gif";
    } else if (file_exists($profile_img_path)) {
        $_SESSION['profile_img'] = "/data/member_image/" . substr($_SESSION['ss_mb_id'], 0, 2) . "/" . $_SESSION['ss_mb_id'] . ".gif";
    } else {
        $_SESSION['profile_img'] = "/resources/images/profile-un.png";
    }

    $userid = $_SESSION['ss_mb_id'];

    if ($chat_level == "10") $is_chat_admin = true;

}

$db = new SimpleDB($pdo_db);

$video = $db->row("SELECT * FROM vimeo_video_list ORDER BY id DESC LIMIT 1");

$config = $db->row("SELECT * FROM gzss_config LIMIT 1");

$live_streaming_url = $config['live_streaming_url'];

$youtube_id = $config['youtube_id'];

$is_live = trim($youtube_id) ? true : false;

if ($is_live) {
    $main['thumbnail_image'] = "http://img.youtube.com/vi/{$youtube_id}/hqdefault.jpg";
    $main['title'] = $config['youtube_title'];
    $main['description'] = $config['youtube_description'];
    $main['date'] = date('Y.m.d');
    $main['vimeo_id'] = $video['id'];
} else {
    $main['thumbnail_image'] = $video['thumbnail3'];
    $main['title'] = $video['title'];
    $main['description'] = $video['description'];
    $main['date'] = date('Y.m.d', strtotime($video['release_datetime']));
    $main['vimeo_id'] = $video['id'];
}


//메인 하단 동영상 목록
$rows = $db->query("SELECT * FROM vimeo_video_list ORDER BY id DESC LIMIT 0, 12");
$list = [];

foreach ($rows as $row) {
    $list[] = [
        'id' => $row['id'],
        'title' => $row['title'],
        'description' => $row['description'],
        'release_datetime' => $row['release_datetime'],
        'datetime' => date('Y.m.d', strtotime($row['release_datetime'])),
        'link' => $row['link'],
        'thumbnail' => $row['thumbnail2'],
    ];
}

$chat_ws_url = "wss://www.vellado.com/chat";
if(is_beta()) {
    $chat_ws_url = "wss://dev.vellado.com/chat";
}


if($_GET['type'] == "2") {
    $chat_channel = "VELLADO";
    $live_config = [
        "vimeo_url" => $config['vimeo2_url'],
        "vimeo_title" => $config['vimeo2_title'],
        "vimeo_date" => $config['vimeo2_date'],
        "vimeo_description" => $config['vimeo2_description'],
    ];

} else {
    $chat_channel = "ALL";
    $live_config = [
        "vimeo_url" => $config['vimeo_url'],
        "vimeo_title" => $config['vimeo_title'],
        "vimeo_date" => $config['vimeo_date'],
        "vimeo_description" => $config['vimeo_description'],
    ];

}




?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" charset="utf-8">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#aa15ff">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="/resources/css/common.css?t=20210101">
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <meta name="naver-site-verification" content="f47bc1e19f2ef9f8ec34e16e88a11c1f99840b73" />
    <title>VELLADO</title>
    <style>
                //20210127 플레이어 스타일 추가
                .verticalFullModeActive {
                    width: 100% !important;
                    position: fixed !important;
                    top: 0 !important;
                    left: 0 !important;
                    z-index: 9999 !important;
                    height: 100vh !important;
                    background-color:#333;
                }
    </style>

    <script> //유튜브 임베드 코드
        /*    var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            console.log("firstScriptTag::{}", firstScriptTag);
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            var player;

            function onYouTubeIframeAPIReady() {
                player = new YT.Player('player_div', {
                    height: '100%',
                    width: '100%',
                    videoId: 'aYNyr8h50wk', //아이디값 받아서 넣기
                    rel: 0,
                    modestbranding: 1,
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                });
                console.log("youtube start");
            }

            function onPlayerReady(event) {
                event.target.playVideo();
            }

            var done = false;

            function onPlayerStateChange(event) {

            }
            function stopVideo() {
            }*/


    </script>
    <script src="resources/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.bootpay.co.kr/js/bootpay-3.3.1.min.js" type="application/javascript"></script>
    <script src="https://player.vimeo.com/api/player.js"></script>
    <script>
        var userid = "<?=$userid?>";
        var nick = "<?=$nick?>";
        var is_admin = <?= $is_chat_admin ? "true" : "false"?>;
        var is_guest = <?= $is_guest ? "true" : "false"?>;
        var profile_img = "<?=$_SESSION['profile_img']?>";
        var chat_channel = "<?=$chat_channel?>";
        var scrollLock = false;

        //도배기준, 최근 보낸 5개의 메세지 텀의 합계가 5초보다 짧은 경우 도배금지 메세지 전송후
        //10초가 메세지 입력금지
        var is_dobae = false;
        var last_msg_send_time = 0; //마지막으로 메세지 보낸시각
        var sum_msg_send_term = 0; //최근 5개 메세지 발송시간 간격
        var msg_send_terms = new Array();
        var msg_send_size = new Array();
        var msg_term_idx = 0;
        var msg_stop_time = 10; //도배차단시간 10초
        var chatlog = [];
        var msg_count = 0;

        var inactiveTime = 1000 * 60;
        var inactiveTimer = null;

        var chat_ws_url = "<?=$chat_ws_url?>";

        var vBoxSelected = 0; //구독  종류 선택 1~9. 0은 미선택

        var websocket = null;

        $(function () {

            window.inactiveTimer = setInterval(function() {
                $.get("/ajax.session.php");
            }, inactiveTime);

            // if user is running mozilla then use it's built-in WebSocket
            window.WebSocket = window.WebSocket || window.MozWebSocket;

            function connect() {
                var session_id = /SESS\w*ID=([^;]+)/i.test(document.cookie) ? RegExp.$1 : false;

                if (!session_id) {
                    return;
                }
                //var websocket = new WebSocket('ws://127.0.0.1:8088/chat', ['chat', session_id]);
                websocket = new WebSocket(chat_ws_url, ['chat', session_id]);

                //var websocket = new WebSocket('');
                //websocke.connect('ws://127.0.0.1:3003/push');

                websocket.onopen = function () {


                    var login = {
                        "v": "1.0",
                        "cmd": "LOGIN",
                        "ch_id": chat_channel,
                        "ch_type": "N",
                        "data": {}
                    };

                    websocket.send(JSON.stringify(login)); //try login
                    // connection is opened and ready to use
                };

                websocket.onerror = function (error) {

                };

                websocket.onclose = function (error) {
                    addNotiMessage("접속이 종료되었습니다. 5초뒤 재접속합니다.");

                    setTimeout(function() {
                        connect();
                    }, 5000);

                };

                websocket.onmessage = function (message) {
                    // try to decode json (I assume that each message
                    // from server is json)
                    //console.log("log : %o", message);

                    parseMessage(message);
                }
            }
            connect();


            function parseMessage(message) {
                try {
                    var body = JSON.parse(message.data);
                    console.log("body=%o", body);

                    switch (body.cmd) {
                        case "UC" :

                            var user_count = body.c.toLocaleString('en-IN');
                            document.getElementById("user_count").innerHTML = user_count + "명 시청중";
                            document.getElementById("m_user_count").innerHTML = user_count + "명 시청중";

                            break;
                        case "LOGIN" :
                            addNotiMessage("채팅서버에 접속되었습니다.");
                            break;
                        case "JOIN_OK" :

                            addNotiMessage("[" + body.ch_name + "] 채널에 입장하였습니다.");
                            break;

                        case "CHAT_MSG" :
                            var messages = body.data.messages;
                            for (i = 0; i < messages.length; i++) {
                                addMessage(messages[i], true);
                            }
                            break;
                        case "WHISPER" :
                            var messages = body.data.messages;
                            for (i = 0; i < messages.length; i++) {1
                                //addWhisperessage(messages[i]);
                            }
                            break;
                        case "PRECHAT_MSG" :
                            var messages = body.data.messages;
                            for (i = 0; i < messages.length; i++) {
                                addMessage(messages[i], false);
                            }
                            //슈퍼챗 목록 처리

                            $.ajax({
                                type: "POST",
                                url: "/ajax.chat.php",
                                data: {cmd: 'get_vbox_list', channel_id:chat_channel},
                                dataType: 'json',
                                success: function (response) {
                                    console.log("%o", response);

                                    if (response["returnCode"] == "200") {
                                        for(var i = 0; i < response['data'].length; i++) {
                                            row = response['data'][i];
                                            addTopVB(row['profile_img'], row['username'], row['message'], row['amount'], (new Date().getTime()) + (row['view_second'] * 1000));
                                        }
                                    }
                                }
                            });

                            break;
                        case "NOTICE" :
                            var notice_type = body.data.notice_type;
                            var notice_level = body.data.notice_level;
                            var notice_msg = body.data.notice_msg;

                            if (notice_level == "MSG") {
                                addSystemMessage(notice_msg);
                            } else if (notice_type == "DEL_MSG") {
                                blindMessage(body.data.message_id);
                            } else if (notice_type == "CHAT_OFF") {
                                addNotiMessage(notice_msg);
                                blindUserMessages(body.data.target_userid, body.data.target_username, body.data.off_type);
                            } else if (notice_type == "CHAT_ON") {
                                addNotiMessage(notice_msg);
                            } else if(notice_type == "RESET_STICKER") {
                                var vbTopContainer = document.querySelector(".vbox_top_container");
                                vbTopContainer.innerHTML ="";
                                if(true) break;
                                $.ajax({
                                    type: "POST",
                                    url: "/ajax.chat.php",
                                    data: {cmd: 'get_vbox_list'},
                                    dataType: 'json',
                                    success: function (response) {
                                        console.log("%o", response);

                                        if (response["returnCode"] == "200") {
                                            for(var i = 0; i < response['data'].length; i++) {
                                                row = response['data'][i];
                                                addTopVB(row['profile_img'], row['username'], row['message'], row['amount'], (new Date().getTime()) + (row['view_second'] * 1000));
                                            }
                                        }
                                    }
                                });
                            } else {
                                addNotiMessage(notice_msg);
                            }
                            break;
                        default :
                            break;
                    }
                } catch (e) {
                    console.log(e.toString(),
                        message.data);
                    return;
                }
            }

            function checkDobae(msg) {

                var result = 0; //도배시간의 합계
                var result_size = 0; //도배시간의 합계
                var tdate = new Date();


                msg_send_terms[msg_term_idx % 5] = tdate.getTime() - last_msg_send_time;
                msg_send_size[msg_term_idx % 5] = msg.length;
                last_msg_send_time = tdate.getTime();
                msg_term_idx++;

                for (var i = 0; i < msg_send_terms.length; i++) {
                    result += msg_send_terms[i];
                    result_size += msg_send_size[i];
                }

                if (msg_send_terms.length == 5 && (result < 3 * 1000 || result_size > 300) && !is_admin) {
                    is_dobae = true;
                    addSystemMessage("도배금지. " + msg_stop_time + "초간 채팅이 금지됩니다.");
                    setTimeout(clearDobae, msg_stop_time * 1000); //10초가 도배금지
                    return true;
                } else {
                    return false;
                }
            }

            function clearDobae() { //도배 초기화
                msg_term_idx = 0;
                msg_send_terms = new Array();
                is_dobae = false;
            }


            $("#inputText").keypress(function (event) {
                if (is_guest) {
                    location.href = '/login';
                    return;
                }
                if (event.keyCode == 10 || event.keyCode == 13) {
                    if(vBoxSelected == 0) {
                        event.preventDefault();
                        sendChatMessage();
                    }
                }
            });


            $("#inputBtn").click(function (event) {
                if (is_guest) {
                    location.href = '/login';
                    return;
                }
                if(vBoxSelected == 0) {
                    event.preventDefault();
                    sendChatMessage();
                }
            });

            $("#btn_block").click(function (event) {
                var target_userid = $("input[name=userid]").val();

                var msgPacket = {
                    "v": "1.0",
                    "cmd": "ADMIN",
                    "ch_id": "ALL",
                    "ch_type": "N",
                    "data": {
                        "admin_cmd": "CHAT_OFF",
                        "target_userid": target_userid
                    }
                }

                websocket.send(JSON.stringify(msgPacket)); //send message

            });

            $("#btn_unblock").click(function (event) {
                var target_userid = $("input[name=userid]").val();

                var msgPacket = {
                    "v": "1.0",
                    "cmd": "ADMIN",
                    "ch_id": "ALL",
                    "ch_type": "N",
                    "data": {
                        "admin_cmd": "CHAT_ON",
                        "target_userid": target_userid
                    }
                }

                websocket.send(JSON.stringify(msgPacket)); //send message

            });



            function sendChatMessage() {

                var msg = $("#inputText").val();


                if (!msg) return;
                if (is_guest) {
                    //addChat('', '', '로그인 후 채팅에 참여가능합니다.', false, false, 1); //시스템 메시지 스타일1
                    location.href = '/login';
                    return;
                }


                var tdate = new Date();

                msg_send_terms = tdate.getTime() - last_msg_send_time;

                if (msg_send_terms < 2 * 1000) {
                    return;
                }

                var msgPacket = {
                    "v": "1.0",
                    "cmd": "CHAT_MSG",
                    "ch_id": "ALL",
                    "ch_type": "N",
                    "data": {
                        "msg": msg
                    }
                }
                $("#inputText").val('');
                last_msg_send_time = tdate.getTime();
                websocket.send(JSON.stringify(msgPacket)); //send message
            }

            $(".btn_chat_blind_msg").click(function (evt) {
                var message_id = $(this).closest("div.chat_admin")[0].getAttribute("message_id");
                var msgPacket = {
                    "v": "1.0",
                    "cmd": "ADMIN",
                    "ch_id": "ALL",
                    "ch_type": "N",
                    "data": {
                        "admin_cmd": "DEL_MSG",
                        "message_id": message_id
                    }
                }

                closeAdmin();
                websocket.send(JSON.stringify(msgPacket)); //send message
            });


            $(".btn_chat_block24h").click(function (evt) {
                var target_userid = $(this).closest("div.chat_admin")[0].getAttribute("userid");
                var target_username = $(this).closest("div.chat_admin")[0].getAttribute("nick");
                var message_id = $(this).closest("div.chat_admin")[0].getAttribute("message_id");
                var msgPacket = {
                    "v": "1.0",
                    "cmd": "ADMIN",
                    "ch_id": "ALL",
                    "ch_type": "N",
                    "data": {
                        "admin_cmd": "CHAT_OFF",
                        "off_type": "24h",
                        "message_id": message_id,
                        "target_userid": target_userid,
                        "target_username": target_username
                    }
                }

                closeAdmin();
                websocket.send(JSON.stringify(msgPacket)); //send message
            });

            $(".btn_chat_block300s").click(function (evt) {
                var target_userid = $(this).closest("div.chat_admin")[0].getAttribute("userid");
                var target_username = $(this).closest("div.chat_admin")[0].getAttribute("nick");
                var message_id = $(this).closest("div.chat_admin")[0].getAttribute("message_id");
                var msgPacket = {
                    "v": "1.0",
                    "cmd": "ADMIN",
                    "ch_id": "ALL",
                    "ch_type": "N",
                    "data": {
                        "off_type": "300s",
                        "admin_cmd": "CHAT_OFF",
                        "message_id": message_id,
                        "target_userid": target_userid,
                        "target_username": target_username
                    }
                }

                closeAdmin();
                websocket.send(JSON.stringify(msgPacket)); //send message

            });

            $(".btn_chat_red_card").click(function (evt) {
                var target_userid = $(this).closest("div.chat_admin")[0].getAttribute("userid");
                var target_username = $(this).closest("div.chat_admin")[0].getAttribute("nick");
                var msgPacket = {
                    "v": "1.0",
                    "cmd": "ADMIN",
                    "ch_id": "ALL",
                    "ch_type": "N",
                    "data": {
                        "admin_cmd": "RED_CARD",
                        "target_userid": target_userid,
                        "target_username": target_username
                    }
                }

                closeAdmin();
                websocket.send(JSON.stringify(msgPacket)); //send message
            });

            $("#chat_view").on("click", ".chat_unblock", function () {
                var target_userid = $(this).data("userid");
                var target_username = $(this).data("username");
                var msgPacket = {
                    "v": "1.0",
                    "cmd": "ADMIN",
                    "ch_id": "ALL",
                    "ch_type": "N",
                    "data": {
                        "admin_cmd": "CHAT_ON",
                        "target_userid": target_userid,
                        "target_username": target_username
                    }
                }
                websocket.send(JSON.stringify(msgPacket));
            });
        });

    </script>
    <script>
        var resizeTimeout;
        window.addEventListener("resize", function (event) {
            clearTimeout(resizeTimeout);
            resizeTimeout = setTimeout(changeVideoSize, 300);

        });

        //모바일 가로에서 세로로 돌아올때 아이폰 버그 회피 부분
        var UA = navigator.userAgent;
        window.addEventListener('orientationchange', updateOrientation, false);

        function updateOrientation() {
            switch (window.orientation) {
                case 0: //portrait
                    setTimeout(function () {
                        var UA = navigator.userAgent;
                        isIOS = (
                            /\b(iPhone)\b/i.test(UA) ||
                            /\b(iPad)\b/i.test(UA)
                        );
                        if (isIOS) {
                            location.reload();
                        }
                    }, 200);

                    break;
            }
        }

        //창 사이즈 변경 시 비디오 사이즈 16:9에 맞게 변경
        var winWidth = window.innerWidth;
        var winHeight = window.innerHeight;
        var firstResize = true;

        function changeVideoSize() {
            if (winWidth == window.innerWidth && winHeight == window.innerHeight && !firstResize) {
                console.log("return resize");
                return;
            }


            if (isMobile()) {
                console.log("video resize mobile");
            } else {
                console.log("video resize pc");
                var width = document.getElementById('player_div').offsetWidth;
                document.getElementById("player_div").style.height = (width * 9 / 16) + "px"; //16:9 비율로 height 조절
            }

            firstResize = false;
            changeChatSize();
            chatScrollBottom();

        }

        // PC모드 일때 창 사이즈 변경에 따라 채팅창 높이 조절
        function changeChatSize() {
            //chat box height 조절 pc버전 - 스크롤바 생성위해 css가변 높이 지정 불가. 스크립트로 넣어야함.
            if (isMobile()) {
                //alert();
                console.log("chat size mobile");
            } else {
                var chatBox = document.getElementsByClassName('chat_box')[0];
                var chatView = document.getElementsByClassName('chat_view')[0];
                chatBox.style.height = "500px";
                var leftBoxHeight = document.getElementsByClassName('video_left_box')[0].scrollHeight;
                chatBox.style.height = leftBoxHeight + "px";
                chatView.style.height = leftBoxHeight - 60 + "px";
            }
        }

        window.addEventListener("load", function () {

            changeVideoSize();

            //채팅창 사이즈
            changeChatSize();

            document.querySelector("#chat_view").addEventListener("scroll", function (ev) {
                ev.target.scrollTop;//pixels scrolled from element top
                ev.target.scrollHeight;//pixels of the whole element.

                var diff_scrollTopPos = ev.target.scrollHeight - (ev.target.scrollTop + ev.target.offsetHeight)
                //console.log("chat box scroll scrollTop=%o, scrollHeight=%o, innerHeight=%o, diff_scrollTopPos=%o", ev.target.scrollTop, ev.target.scrollHeight, ev.target.offsetHeight, diff_scrollTopPos);

                if (Math.abs(diff_scrollTopPos) > 50) {
                    window.scrollLock = true;
                } else {
                    window.scrollLock = false;
                }

                if (scrollLock) {
                    document.querySelector("#chat_btn_scroll").className = "chat_btn_scroll show";
                } else {
                    document.querySelector("#chat_btn_scroll").className = "chat_btn_scroll hide";
                }
            });

            document.querySelector("#chat_btn_scroll").addEventListener("click", function (e) {
                chatScrollBottom();
            });

            // 이벤트 처리 -- 채팅관리자 창 띄우기 / vbox 선택창 띄우기 / vbox 선택 / 상단 vbox 레이블 클릭시.
            document.querySelector(".chat_box").addEventListener("click", function (e) {
                classList = e.target.classList;

                if (classList.contains('chat_item') || classList.contains('chat_text') || classList.contains('chat_pf')) return false;

                if (classList.contains('chat_nick')) {
                    showAdmin(e);
                } else if (classList.contains('chat_vb_nick')) { //VBOX 금액 선택박스 노출
                    showAdmin(e);
                } else if (e.target.parentElement.classList.contains('vBtn')) { //VBOX 금액 선택박스 노출
                    if (is_guest) {
                        alert('로그인후 이용하실수 있습니다.');
                        return;
                    }
                    showVBox();
                } else if (classList.contains('vbox') || e.target.parentElement.classList.contains('vbox')) { //금액 박스 선택 시
                    selectVBox(e)
                } else if (classList.contains('vbox_top_box') || e.target.parentElement.classList.contains('vbox_top_box')) { //상단 VBOX 클릭 시 해당 메시지 보여줌
                    var container = e.target.classList.contains("vbox_top_box") ? e.target : e.target.parentElement;

                    var instantContainer = document.querySelector(".vbox_instant_container");
                    var dataset = container.dataset;
                    var instantItem = makeVChatItem(dataset.vnum, dataset.pfurl, dataset.nick, dataset.text);

                    instantContainer.appendChild(instantItem);
                    instantContainer.style.display = "flex";
                } else if (classList.contains('vbox_instant_container') || e.target.parentElement.classList.contains('vbox_instant_container')) { //상단 VBOX클릭시 보여줬던 메시지 닫기
                    var container = e.target.classList.contains("vbox_instant_container") ? e.target : e.target.parentElement;
                    container.removeChild(container.firstChild);
                    container.style.display = "none";
                } else if(classList.contains('chat_btn_scroll')){ //스크롤 끝으로 버튼
                    chatScrollBottom();
                } else if(classList.contains('vbox_alert_close')){
                    document.querySelector(".vbox_alert").style.display = "none";
                    var inputText = document.getElementById('inputText');
                    inputText.readOnly = false;
                } else if(classList.contains('button') && e.target.parentElement.classList.contains('vbox_alert_bottom')){ //임시로 붙여둠. 원래는 pg 완료 이후 연결되어야 함.
                    // 변수들 pg 타고 돌아왔을때 입력될 수 있어야 함. 임ㅅ시 전역변수임.
                    var inputText = document.getElementById('inputText');
                    inputText.readOnly = false;
                    document.querySelector(".vbox_alert").style.display = "none";
                    payChatVBox(temp__vnum, temp__pfURL, temp__nick, temp__text);
                    //addVChat(temp__vnum, temp__pfURL, temp__nick, temp__text);
                }

            });

            //채팅상단 vbox pc스크롤 모드
            const slider = document.querySelector('.vbox_top_container');
            var isDown = false;
            var startX;
            var scrollLeft;

            slider.addEventListener('mousedown', (e) => {
                isDown = true;
                slider.classList.add('active');
                startX = e.pageX - slider.offsetLeft;
                scrollLeft = slider.scrollLeft;
            });
            slider.addEventListener('mouseleave', () => {
                isDown = false;
                slider.classList.remove('active');
            });
            slider.addEventListener('mouseup', () => {
                isDown = false;
                slider.classList.remove('active');
            });
            slider.addEventListener('mousemove', (e) => {
                if (!isDown) return;
                e.preventDefault();
                const x = e.pageX - slider.offsetLeft;
                const walk = (x - startX) * 2; //scroll-fast
                slider.scrollLeft = scrollLeft - walk;
            });


            //모바일 가로모드 채팅 보이기/숨기기 버튼 이벤트 추가
            document.querySelector(".chat_btn").addEventListener("click", function (event) {
                const vc = document.querySelector('.video_container').classList;
                if (vc.contains('hide')) {
                    vc.remove('hide')
                } else {
                    vc.add('hide')
                }
            });


            //채팅 입력 이벤트
            var inputBtn = document.getElementById('inputBtn');
            var inputText = document.getElementById('inputText');

            //어드민 창
            adminLayer = document.querySelector('.chat_admin');


            inputText.addEventListener("keyup", function (event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                    inputBtn.click();

                }
            });

            inputText.addEventListener("focus", function (event) {
                chatScrollBottom();
            });

            inputBtn.addEventListener("click", function (event) {
                //실제 닉네임과 프로필 사진으로 변경
                inputBtn.readOnly = false;
                if (inputText.value.length > 0) {
                    //VBOX 결제 시 PG 결과값 체크해서 넘겨오기. 일반채팅스타일과 vbox챗스타일 구분하여 처리.
                    if (vBoxSelected > 0) {
                        var text = inputText.value;
                        inputText.value = "";
                        inputText.readOnly = true;
                        alertVBox(vBoxSelected, profile_img, '<?=$nick?>', text); //채팅 내용
                        showVBox();
                    }
                }
            });
        });

        function addMessage(messageData, is_top_add) {
            var profile_image = messageData.profile_img;

            if (profile_image == "/img/no_img.svg") {
                profile_image = "/resources/images/profile-un.png";
            }

            if(messageData.style == "superchat") {
                var vnum = messageData.extra.amount + '';
                var pfURL = messageData.profile_img;
                var nick = messageData.username;
                var text = messageData.msg;
                var view_second = parseInt(messageData.extra.view_second);

                addVChat(vnum, pfURL, nick, text, view_second, is_top_add);
                return;
            }

            var username = messageData.username;
            var userid = messageData.userid;
            var chat_level = messageData.chat_level;
            var msg = messageData.msg;
            var message_id = messageData.message_id;
            var isHtml = !!messageData.html;
            var color = "#000000";
            isMine = window.userid == messageData.userid ? true : false;
            isAdmin = (chat_level >= 9) ? true : false;

            var regURL = new RegExp("(http|https)://([-/.a-zA-Z0-9_~#%$?&=:200-377()가-힣]+)", "gi");
            msg = msg.replace(regURL, "<a href='$1://$2' target='_blank'>$1://$2</a>");
            isHtml = true;

            msg_count++;

            if(msg_count > 500 && msg_count % 100 == 0) {
                purgeOldMessages(100);
            }

            addChat(message_id, profile_image, userid, username, msg, isMine, isAdmin, '', isHtml);
        }


        function addNotiMessage(message) {
            addChat('', '', '', '', message, false, false, 1); //시스템 메시지 스타일1
        }

        function addSystemMessage(message) {
            addChat('', '', '', '', message, false, false, 2); //시스템 메시지 스타일2
        }

        function addWarningMessage(message) {
            addChat('', '', '', '', message, false, false, 3); //시스템 메시지 스타일3
        }

        function purgeOldMessages(purgeCount) {
            var chatBox = document.getElementById('chat_view');
            for (var index = 0; index < purgeCount; index++) {
                chatBox.removeChild(chatBox.children[0]);
            }
            chatScrollBottom();

        }

        function blindMessage(message_id) {
            var chatItem = document.getElementById("msg_" + message_id);
            chatItem.className = "chat_item sys1";
            chatItem.innerHTML = '<div class="chat_text">관리자에 의해 가려진 메시지 입니다.</div>'
            closeAdmin();
        }

        function blindUserMessages(userid, username, off_type) {

            if (window.is_admin) {
                var chatBox = document.querySelector("#chat_view");
                var chatItem = document.createElement('div');
                var chatText = document.createElement('div');
                chatItem.className = "chat_item chat_unblock sys1";
                chatItem.setAttribute("data-userid", userid);
                chatItem.setAttribute("data-username", username);
                chatText.className = "chat_text";
                chatText.innerHTML = "차단해제";
                chatItem.appendChild(chatText);
                chatBox.appendChild(chatItem);
                chatScrollBottom();
            }

        }

        //채팅 입력

        function addChat(message_id, pfURL, userid, nick, text, isMine, isAdmin, sysType, is_html) {
            //sysType 0 : 일반 / 1:스타일1
            var chatBox = document.getElementById('chat_view');
            var chatItem = document.createElement('div');

            chatItem.setAttribute("id", 'msg_' + message_id);
            chatItem.setAttribute("message_id", message_id);
            chatItem.setAttribute("data-userid", userid);

            if (isMine) {
                chatItem.className = "chat_item active";
            } else {
                chatItem.className = "chat_item";
            }

            var chatProfile = document.createElement('img');
            chatProfile.className = "chat_pf";
            chatProfile.src = pfURL;

            var chatNick = document.createElement('div');
            var chatAdminIcon;
            if (isAdmin) {
                chatNick.className = "chat_nick_admin";
                chatAdminIcon = document.createElement('img');
                chatAdminIcon.className = "chat_admin_icon";
                chatAdminIcon.src = "resources/images/badge.png";
            } else {
                chatNick.className = "chat_nick";
            }
            chatNick.innerText = nick;
            chatNick.setAttribute("userid", userid);
            chatNick.setAttribute("nick", nick);
            chatNick.setAttribute("message_id", message_id);


            var chatText = document.createElement('div');
            chatText.className = "chat_text";

            switch (sysType) {
                case 0:
                    break;
                case 1:
                    chatItem.className = "chat_item sys1";
                    break;
                case 2:
                    chatItem.className = "chat_item sys2";
                    break;
                case 3:
                    chatItem.className = "chat_item sys3";
                    break;
            }

            chatText.appendChild(chatProfile);
            chatText.appendChild(chatNick);
            if (isAdmin) chatText.appendChild(chatAdminIcon);

            if (is_html) {
                var messageNode = document.createElement('span');
                messageNode.innerHTML = text;
                chatText.appendChild(messageNode);
            } else {
                var messageNode = document.createTextNode(text);
                chatText.appendChild(messageNode);
            }

            chatItem.appendChild(chatText);
            chatBox.appendChild(chatItem);
            if (!window.scrollLock) {
                chatScrollBottom();
            }
        }

        //VBOX 구매확인창

        //퍼블에서 보여주기 위한 임시 변수들. PG 연동 이후에는 다른 처리가 되어야 함.
        var temp__vnum, temp__pfURL, temp__nick, temp__text;

        function alertVBox(vnum, pfURL, nick, text){
            var vboxAlertBody = document.querySelector(".vbox_alert_vbox");
            var vboxAlertBtn = document.querySelector(".vbox_alert_bottom .button");
            vboxAlertBtn.innerText = (getVAmount(vnum)*1000).toLocaleString('en')  + "원 결제하고 전송하기";
            if(vboxAlertBody.hasChildNodes()){
                vboxAlertBody.removeChild(vboxAlertBody.firstChild);
            }
            vboxAlertBody.appendChild(makeVChatItem(vnum, pfURL, nick, text));
            document.querySelector(".vbox_alert").style.display = "block";

            temp__vnum = vnum;
            temp__pfURL = pfURL;
            temp__nick = nick;
            temp__text = text;
        }

        //V CHAT 입력 부
        function addVChat(vnum, pfURL, nick, text, view_second, is_top_add) {
            var chatBox = document.getElementsByClassName('chat_view')[0];

            chatBox.appendChild(makeVChatItem(vnum, pfURL, nick, text));

            if(is_top_add) {
                addTopVB(pfURL, nick, text, vnum, (new Date().getTime()) + (view_second * 1000));
            }

            chatScrollBottom()
        }

        function makeVChatItem(vnum, pfURL, nick, text) {
            var chatVBItem = document.createElement('div');
            chatVBItem.className = "chat_vb_item";

            var chatVBTitleBox = document.createElement('div');
            if (vnum > 3 && vnum < 7) {
                chatVBTitleBox.className = "chat_vb_title_box blue";
            } else if (vnum >= 7) {
                chatVBTitleBox.className = "chat_vb_title_box purple";
            } else {
                chatVBTitleBox.className = "chat_vb_title_box";
            }

            var chatVBTitleLeftbox = document.createElement('div');
            chatVBTitleLeftbox.className = "chat_vb_title_leftbox";

            var chatVBProfile = document.createElement('img');
            chatVBProfile.className = "chat_vb_pf";
            chatVBProfile.src = pfURL;

            var chatVBNick = document.createElement('div');
            chatVBNick.className = "chat_vb_nick";
            chatVBNick.innerText = nick + " 님의 ";

            var chatVBoxIcon = document.createElement('img');
            chatVBoxIcon.className = "chat_vb_vboxicon";
            chatVBoxIcon.src = "resources/images/icon-vbox.png";

            var chatVBTitleRightbox = document.createElement('div');
            chatVBTitleRightbox.className = "chat_vb_title_rightbox";

            var chatVBAmount = document.createElement('div');
            chatVBAmount.className = "chat_vb_amount";
            chatVBAmount.innerText = getVAmount(vnum);


            var chatVBIcon = document.createElement('img');
            chatVBIcon.className = "chat_vb_vicon";
            var barNum = (vnum % 3) == 0 ? 3 : vnum % 3;
            chatVBIcon.src = "resources/images/v-" + barNum + ".png";
            ;

            var chatVBTextBox = document.createElement('div');
            chatVBTextBox.className = "chat_vb_text_box";
            chatVBTextBox.innerText = text;

            chatVBTitleLeftbox.appendChild(chatVBProfile);
            chatVBTitleLeftbox.appendChild(chatVBNick);
            chatVBTitleLeftbox.appendChild(chatVBoxIcon);
            chatVBTitleRightbox.appendChild(chatVBAmount);
            chatVBTitleRightbox.appendChild(chatVBIcon);
            chatVBTitleBox.appendChild(chatVBTitleLeftbox);
            chatVBTitleBox.appendChild(chatVBTitleRightbox);
            chatVBItem.appendChild(chatVBTitleBox);
            chatVBItem.appendChild(chatVBTextBox);

            return chatVBItem;
        }

        function payChatVBox(temp__vnum, temp__pfURL, temp__nick, temp__text) {
            //실제 복사하여 사용시에는 모든 주석을 지운 후 사용하세요
            var pay_amount = getVAmount(temp__vnum)*1000;
            var item_name = getVAmount(temp__vnum) + ' Vbox';
            var unique =  "vbox_" + getVAmount(temp__vnum);

            BootPay.request({
                price: pay_amount, //실제 결제되는 가격
                application_id: "5feafd922fa5c2002103971c",
                name: item_name, //결제창에서 보여질 이름
                pg: 'welcome',
                method: 'digital_card', //결제수단, 입력하지 않으면 결제수단 선택부터 화면이 시작합니다.
                show_agree_window: 0, // 부트페이 정보 동의 창 보이기 여부
                items: [
                    {
                        item_name: item_name, //상품명
                        qty: 1, //수량
                        unique: unique, //해당 상품을 구분짓는 primary key
                        price: pay_amount, //상품 단가
                    }
                ],
                user_info: {
                    username: temp__nick,
                    email: '사용자 이메일',
                    addr: '사용자 주소',
                    phone: '010-1234-4567'
                },
                order_id: '<?=time()?>', //고유 주문번호로, 생성하신 값을 보내주셔야 합니다.
                params: {
                    message: temp__text,
                    callback2: temp__text,
                    vnum: temp__vnum + '',
                    userid  : "<?=$userid?>",
                    nick  : "<?=$nick?>",
                    chat_channel : chat_channel,
                    pfURL  : temp__pfURL,

                },
                account_expire_at: '2020-10-25', // 가상계좌 입금기간 제한 ( yyyy-mm-dd 포멧으로 입력해주세요. 가상계좌만 적용됩니다. )
                extra: {
                    start_at: '2019-05-10', // 정기 결제 시작일 - 시작일을 지정하지 않으면 그 날 당일로부터 결제가 가능한 Billing key 지급
                    end_at: '2022-05-10', // 정기결제 만료일 -  기간 없음 - 무제한
                    vbank_result: 1, // 가상계좌 사용시 사용, 가상계좌 결과창을 볼지(1), 말지(0), 미설정시 봄(1)
                    quota: '0,2,3', // 결제금액이 5만원 이상시 할부개월 허용범위를 설정할 수 있음, [0(일시불), 2개월, 3개월] 허용, 미설정시 12개월까지 허용,
                    theme: 'purple', // [ red, purple(기본), custom ]
                    custom_background: '#00a086', // [ theme가 custom 일 때 background 색상 지정 가능 ]
                    custom_font_color: '#ffffff' // [ theme가 custom 일 때 font color 색상 지정 가능 ]
                }
            }).error(function (data) {
                //결제 진행시 에러가 발생하면 수행됩니다.
                console.log(data);
                alert(data['msg']);
            }).cancel(function (data) {
                //결제가 취소되면 수행됩니다.
                console.log(data);
                alert("결제요청이 취소 되었습니다.");
            }).ready(function (data) {
                // 가상계좌 입금 계좌번호가 발급되면 호출되는 함수입니다.
                console.log(data);
            }).confirm(function (data) {
                //결제가 실행되기 전에 수행되며, 주로 재고를 확인하는 로직이 들어갑니다.
                //주의 - 카드 수기결제일 경우 이 부분이 실행되지 않습니다.
                console.log(data);
                var enable = true; // 재고 수량 관리 로직 혹은 다른 처리
                if (enable) {
                    BootPay.transactionConfirm(data); // 조건이 맞으면 승인 처리를 한다.
                } else {
                    BootPay.removePaymentWindow(); // 조건이 맞지 않으면 결제 창을 닫고 결제를 승인하지 않는다.
                }
            }).close(function (data) {
                // 결제창이 닫힐때 수행됩니다. (성공,실패,취소에 상관없이 모두 수행됨)
                console.log(data);
            }).done(function (data) {
                //결제가 정상적으로 완료되면 수행됩니다
                //비즈니스 로직을 수행하기 전에 결제 유효성 검증을 하시길 추천합니다.
                console.log(data);
            });
        }

        //어드민
        var adminLayer; //window load 이벤트 시 넣음. 채팅 어드민 창 레이어

        function showAdmin(evt) {
            adminLayer.setAttribute('userid', evt.target.getAttribute('userid'));
            adminLayer.setAttribute('nick', evt.target.getAttribute('nick'));
            adminLayer.setAttribute('message_id', evt.target.getAttribute('message_id'));

            adminLayer.getElementsByClassName("nick_label")[0].innerText = evt.srcElement.innerText + " 의 메시지 처리";
            adminLayer.style.display = "block";
        }

        function closeAdmin() {
            adminLayer.style.display = "none";
        }

        //vbox

        //상단 가로스크롤 결제모음 추가부 (프로필URL, 닉네임, 채팅텍스트, 결제종류1~9, 사라질시간-timestamp)
        function addTopVB(pfURL, nick, text, vnum, disposeAt) {

            var currentTimestamp = new Date().getTime();
            if (disposeAt < currentTimestamp) {
                return false;
            }

            var barNum = (vnum % 3) == 0 ? 3 : vnum % 3;

            var vbTopContainer = document.querySelector(".vbox_top_container");

            var vbTopBox = document.createElement('div');
            vbTopBox.dataset.pfurl = pfURL;
            vbTopBox.dataset.nick = nick;
            vbTopBox.dataset.text = text;
            vbTopBox.dataset.vnum = vnum;

            if (vnum > 3 && vnum < 7) {
                vbTopBox.className = "vbox_top_box blue";
            } else if (vnum >= 7) {
                vbTopBox.className = "vbox_top_box purple";
            } else {
                vbTopBox.className = "vbox_top_box";
            }

            var vbTopPf = document.createElement('img');
            vbTopPf.className = "vbox_top_pf";
            vbTopPf.src = pfURL;

            var vbTopNick = document.createElement('div');
            vbTopNick.className = "vbox_nick";
            vbTopNick.innerText = nick;

            var vbTopIcon = document.createElement('img');
            vbTopIcon.className = "vbox_top_icon";
            vbTopIcon.src = "resources/images/v-" + barNum + ".png";
            ;

            vbTopBox.appendChild(vbTopPf)
            vbTopBox.appendChild(vbTopNick)
            vbTopBox.appendChild(vbTopIcon)

            var timer = disposeAt - currentTimestamp;
            setTimeout(function () {
                vbTopBox.parentNode.removeChild(vbTopBox);
            }, timer);

            vbTopContainer.insertBefore(vbTopBox, vbTopContainer.firstChild);

        }

        //vbox 구독 종류 선택창 보이고 숨기기
        //pg 연결 후 정상 결제 이후에는 vbox_container / 그 이전에는 vbox_container_temp 를 보여줌. 아래부분
        function showVBox() {
            if (document.querySelector(".vbox_container").style.display == "block") {
                document.querySelector(".vbox_container").style.display = "none";
                vBoxSelected = 0;
                setInputBox(vBoxSelected);
            } else {
                document.querySelector(".vbox_container").style.display = "block";
            }

            if (vBoxSelected == 0) {
                vboxes = document.querySelectorAll(".vbox");
                vboxes[0].className = "vbox";
                vboxes[1].className = "vbox";
                vboxes[2].className = "vbox";
                vboxes[3].className = "vbox blue";
                vboxes[4].className = "vbox blue";
                vboxes[5].className = "vbox blue";
                vboxes[6].className = "vbox purple";
                vboxes[7].className = "vbox purple";
                vboxes[8].className = "vbox purple";
            }
        }

        //vbox 구독 종류 선택시 처리
        function selectVBox(e) {
            var obj = e.target;

            if (e.target.parentElement.classList.contains("vbox")) {
                obj = e.target.parentElement;
            }
            var boxes = document.querySelectorAll(".vbox");
            [].forEach.call(boxes, function (boxes) {
                boxes.className = "vbox disabled"
            });

            switch (obj.innerText) {
                case   "5":
                    vBoxSelected = 1;
                    obj.className = "vbox";
                    break;
                case  "10":
                    vBoxSelected = 2;
                    obj.className = "vbox";
                    break;
                case  "20":
                    vBoxSelected = 3;
                    obj.className = "vbox";
                    break;
                case  "30":
                    vBoxSelected = 4;
                    obj.className = "vbox blue";
                    break;
                case  "50":
                    vBoxSelected = 5;
                    obj.className = "vbox blue";
                    break;
                case "100":
                    vBoxSelected = 6;
                    obj.className = "vbox blue";
                    break;
                case "200":
                    vBoxSelected = 7;
                    obj.className = "vbox purple";
                    break;
                case "300":
                    vBoxSelected = 8;
                    obj.className = "vbox purple";
                    break;
                case "500":
                    vBoxSelected = 9;
                    obj.className = "vbox purple";
                    break;
            }
            setInputBox(vBoxSelected, obj.innerText);
        }

        // vbox 구독 종류 선택시 하단 입력창 디자인 변경
        function setInputBox(num, vbox_value) {
            var bg;
            var iconNum = num == 0 ? 0 : ((num <= 3) ? 1 : parseInt(((num - 1) / 3) + 1));
            var barNum = (num % 3) == 0 ? 3 : num % 3;

            switch (iconNum) {
                case 0 :
                    bg = "#666";
                    break;
                case 1 :
                    bg = "#00d9e6";
                    break;
                case 2 :
                    bg = "#1980ff";
                    break;
                case 3 :
                    bg = "#aa15ff";
                    break;
            }
            document.querySelector(".chat_input .vBtn").style.backgroundColor = bg;
            document.querySelector(".chat_input .vBtn img").src = "resources/images/v-" + barNum + ".png";
            document.querySelector(".chat_input input").style.color = bg;
            document.querySelector(".chat_input #inputBtn").src = "resources/images/btn-chat-" + iconNum + ".png";

        }

        //vbox 구독종류 선택에 따른 금액
        function getVAmount(num) {
            var ret = 0;

            switch (parseInt(num)) {
                case 1:
                    ret = 5;
                    break;
                case 2:
                    ret = 10;
                    break;
                case 3:
                    ret = 20;
                    break;
                case 4:
                    ret = 30;
                    break;
                case 5:
                    ret = 50;
                    break;
                case 6:
                    ret = 100;
                    break;
                case 7:
                    ret = 200;
                    break;
                case 8:
                    ret = 300;
                    break;
                case 9:
                    ret = 500;
                    break;
            }
            return ret;
        }


        function chatScrollBottom() {

            if (isMobile()) {
                $(document).scrollTop($(document).height()); //ios 사파리 버그로 jquery를 쓸 수 밖에 없음
                document.querySelector(".chat_view").scrollTop = 1000000;
            } else {
                document.querySelector(".chat_view").scrollTop = 1000000;
            }


            // if (isMobile()) {
            //     $(document).scrollTop($(document).height()); //ios 사파리 버그로 jquery를 쓸 수 밖에 없음
            //     document.getElementById("chat_view").scrollTop = 1000000;
            //     //console.log("scrollHeight : %o", $('#chat_view').scrollHeight);
            //     //$("#chat_view").scrollTop($('#chat_view').scrollHeight);
            // } else {
            //     document.querySelector("chat_view").scrollTop = 1000000;
            //     //$("#chat_view").scrollTop($('#chat_view').scrollHeight);
            // }

            /*
            if(browser_type == "IE" || browser_type == "MZ") {
                var chatMsgObj = document.getElementById("chatLog");
                chatMsgObj.insertAdjacentHTML("BeforeEnd", msg);
                if(!is_scroll_lock) {
                    chatMsgObj.scrollTop = chatMsgObj.scrollHeight;
                }

            } else {
                $("#chatLog").append(msg);
                //$("#chatLog").attr({ scrollTop: $("#chatLog").attr("scrollHeight") });
                if(!is_scroll_lock) {
                    $("#chatLog").scrollTop($("#chatLog")[0].scrollHeight);
                }
            }
             */

        }


    </script>
</head>
<body class="live" oncontextmenu='return false' onselectstart='return false' ondragstart='return false' >

<!--최상단 배너-->
<div class="top_banner_container">
    <div class="top_banner_box">
        <a href="/main"><img src="resources/images/logo.png"></a>
        <div class="menu_box">
            <?php if (!$_SESSION['ss_mb_id']) : ?>
                <a href="/login"><img src="/resources/images/btn-my.png"></a>
            <?php else: ?>
                <a href="/myinfo"><img src="/resources/images/btn-my.png"></a>
            <?php endif; ?>
            <?php if ($is_admin) : ?>
                <a href="/admin"><img src="/resources/images/btn-admin.png"></a>
            <?php endif; ?>
            <div class="main_btn_container_special"><img src="/resources/images/icon-shortcut.png">
                <div><a href="<?=$gzss_config['support_link']?>" target="_blank">구독안내</a></div>
            </div>
            <?php if ($_SESSION['ss_mb_id']) : ?>
                <div class="main_btn_container_special"><img src="/resources/images/icon-log.png">
                    <div><a href="/logout">로그아웃</a></div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<!-- 최상단 배너 모바일-->
<div class="main_banner_container_m">
    <div class="m_counter" id="m_user_count">
         시청중
    </div>
    <div class="main_banner_btn_m">
        <a href="javascript:history.back()"><img src="/resources/images/btn-back.png"></a>
        <?php if (!$_SESSION['ss_mb_id']) : ?>
            <a href="/login"><img src="/resources/images/btn-my-2.png"></a>
        <?php else: ?>
            <a href="/myinfo"><img src="/resources/images/btn-my-2.png"></a>
        <?php endif; ?>
        <?php if ($is_admin) : ?>
            <a href="/admin"><img src="/resources/images/btn-admin-2.png"></a>
        <?php endif; ?>
        <a href="<?=$gzss_config['support_link']?>" target="_blank"><img src="/resources/images/icon-shortcut-2.png"></a>
        <?php if ($_SESSION['ss_mb_id']) : ?>
            <a href="/logout"><img src="/resources/images/btn-log-2.png"></a>
        <?php endif; ?>
    </div>
</div>


<!-- 메인 컨텐츠 -->
<div class="main_container">


    <div class="main_content_div">
        <!-- 메뉴 -->
        <div class="main_btn_container">
            <div class="btn">
                <div class="text"><a href="/vod">영상</a></div>
                <a href="/vod"><img class="icon1" src="/resources/images/btn-video_off.png"></a>
            </div>
            <div class="btn">
                <div class="text"><a href="/notice">공지</a></div>
                <a href="/notice"><img class="icon2" src="/resources/images/btn-noti_off.png"></a>
            </div>
            <div class="btn">
                <div class="text"><a href="/support">구독</a></div>
                <a href="/support"><img class="icon3" src="/resources/images/btn-spon_off.png"></a>
            </div>
        </div>

        <!-- 상단 플레이어 등-->
        <div class="video_container">
            <div class="video_left_box">
                <div class="chat_btn"></div>
                <div class="player_div" id="player_div">
                    <iframe class="edit_mobile_iframe" src="<?= $live_config['vimeo_url'] ?>" width="100%" height="100%" frameborder=0 marginheight=0 marginwidth=0 scrolling=no  allowfullscreen allow="autoplay"></iframe>
                </div>
                <div class="box_live">
                    <div class="count2" id="user_count"></div>
                    <div class="onair">ON AIR</div>
                </div>
                <div class="title"><?= $live_config['vimeo_title'] ?></div>
                <div class="date2"><?= $live_config['vimeo_date'] ?></div>
                <div class="body"><?= $live_config['vimeo_description'] ?>
                </div>
            </div>
            <div class="video_right_box">
                <div class="chat_box">
                    <div class="vbox_top_container"></div>
                    <div class="vbox_instant_container"></div>
                    <img id="chat_btn_scroll" class="chat_btn_scroll hide" src="resources/images/btn-scroll.png">
                    <div class="chat_view" id="chat_view">

                    </div>
                    <?php if ($is_chat_admin) : ?>
                        <div class="chat_admin" style="display:none">
                            <div class="nick_box">
                                <div class="nick_label"></div>
                                <div class="btn_close"><img src="resources/images/btn-close.png" onclick="closeAdmin()"></div>
                            </div>
                            <div class="chat_admin_box"><a class="btn_chat_blind_msg" href="javascript:;"><img src="resources/images/icon-panalty-1.png">해당 메시지 가리기</a></div>
                            <div class="chat_admin_box"><a class="btn_chat_block300s" href="javascript:;"><img src="resources/images/icon-panalty-2.png">300초 동안 채팅 제한하기</a></div>
                            <div class="chat_admin_box"><a class="btn_chat_block24h" href="javascript:;"><img src="resources/images/icon-panalty-3.png">24시간 동안 채팅 제한하기</a></div>
                            <div class="chat_admin_box"><a class="btn_chat_red_card" href="javascript:;"><img src="resources/images/icon-panalty-4.png">영구정지</a></div>
                        </div>
                    <?php endif; ?>
                    <div class="vbox_container">
                        <div class="vbox_title">1 VBOX = ₩1,000</div>
                        <div class="boxes_container">
                            <div class="vbox">5<img src="resources/images/vb-1.png"></div>
                            <div class="vbox">10<img src="resources/images/vb-2.png"></div>
                            <div class="vbox">20<img src="resources/images/vb-3.png"></div>
                            <div class="vbox blue">30<img src="resources/images/vb-1.png"></div>
                            <div class="vbox blue">50<img src="resources/images/vb-2.png"></div>
                            <div class="vbox blue">100<img src="resources/images/vb-3.png"></div>
                            <div class="vbox purple">200<img src="resources/images/vb-1.png"></div>
                            <div class="vbox purple">300<img src="resources/images/vb-2.png"></div>
                            <div class="vbox purple">500<img src="resources/images/vb-3.png"></div>
                        </div>
                    </div>
                    <div class="vbox_container_temp">
                        <div class="vbox_temp"><img src="resources/images/icon-vbox.png"></div>
                        <div class="vbox_title">V-BOX 구매하기 서비스 준비중 입니다.<br>개선된 서비스로 찾아뵙겠습니다.</div>
                    </div>

                    <div class="chat_input">
                        <div class="vBtn"><img src="resources/images/v-3.png"></div>

                        <input type="text" id="inputText" placeholder="내용을 입력하세요." maxlength="200">
                        <a href="javascript:" id="inputBtn"><img id="inputBtn" src="resources/images/btn-chat-0.png"></a>
                    </div>

                    <div class="vbox_alert">
                        <div class="vbox_alert_title">
                            <img src="resources/images/vb-1.png">
                            <img class="vbox_alert_close" src="resources/images/btn-close-bk.png">
                        </div>
                        <div class="vbox_alert_body">VBox는 다른 시청자와 크리에이터에게 나의 말을 더 잘 표현할 수 있는 컨텐츠입니다.
                            VBox를 구매하시면 채팅메세지가 VBox안에 표시되어 모두에게 주목을 받으실 수 있고, 구매 금액이 높을수록 더 오래 표시됩니다.</div>
                        <div class="vbox_alert_vbox"></div>
                        <div class="vbox_alert_bottom">
                            <div class="text big">위 내용대로 전송하시겠습니까?</div>
                            <div class="button">결제하고 전송하기</div>
                            <div class="line"></div>
                            <div class="text center">구매하시면&nbsp;<a href="/return_policy" target="_blank" class="purple">판매/환불정책</a>에 동의하시게 되며,<br>이에 따라 구매를 진행합니다.</div>
                            <div class="line"></div>
                            <div class="text">주식회사 벨라도<br>
                                사업자 등록번호 : 342 86 02129<br>
                                통신판매업 : 2020 인천연수 1843<br>
                                대표자 : 안정권, 손미진<br>
                                사업장 소재지 : 인천 연수 송도과학로 56<br>
                                대표전호 : 1800 6631</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--라이브 모바일 처리 위한 컨테이너-->
        <div class="live_pc_container">
            <!--검색 박스-->
            <?php /*
            <div class="main_search_box">
                <input type="text" placeholder="검색어를 입력하세요.">
                <img src="resources/images/btn-search.png">
            </div>
            */ ?>

            <div class="break"></div>
            <!-- VOD 썸네일 리스트 -->
            <div class="vod_list_container">
                <div>
                    <img class="vod_list_img_temp" src="resources/images/under-desk.png" style="object-fit: contain;">
                </div>
                <?php /*
                <div class="vod_list_box">

                    <?php foreach ($list as $index => $row) : ?>
                        <div class="vod_thumb_box">
                            <div class="thumbnail"><a href="./vod/<?= $row['id'] ?>" rel="<?= $row['id'] ?>"><img src="<?= $row['thumbnail'] ?>"></a></div>
                            <div class="title"><a href="./vod/<?= $row['id'] ?>" rel="<?= $row['id'] ?>"><?= $row['title'] ?></a></div>
                            <div class="body"></div>
                            <div class="date_box">
                                <div class="date"><?= $row['datetime'] ?></div>
                                <div class="count">- <img src="resources/images/icon-view.png"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                */ ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("form[name=searchForm").submit(function (event) {
            event.preventDefault();
            var keyword = $("input[name=keyword]").val();

            $("#vod_list_box").html("");
            $.ajax({
                type: "POST",
                url: "./ajax.video.php",
                data: {cmd: 'main_video_list', keyword: keyword, page: 1},
                dataType: 'json',
                success: function (data) {
                    console.log("%o", data);
                    if (data["returnCode"] == "200") {//성공
                        $("#vod_list_box").append(data['content']);

                    } else {
                        //alert(data["errorMsg"]);
                    }
                }
            });

        });

    });
</script>


<script type="text/javascript">
    // F12 버튼 방지
    $(document).ready(function(){
        $(document).bind('keydown',function(e){
            if ( e.keyCode == 123 /* F12 */) {
                e.preventDefault();
                e.returnValue = false;
            }
        });
    });

    // 우측 클릭 방지
    document.onmousedown=disableclick;
    status="Right click is not available.";

    function disableclick(event){
        if (event.button==2) {
            alert(status);
            return false;
        }
    }
    var wrapper = document.querySelector("#playerArea");

    window.addEventListener('message', function(e) {
        if(e.data.fullSize == true) {
            wrapper.classList.add("verticalFullModeActive");
        }
        else if(e.data.fullSize == false) {

        }
    });

</script>



<div class="live_pc_container2">
    <div class="footer_container">
        <div class="footer_box">
            <div class="left_box">
                <div class="title">VELLADO</div>
                <div class="body">
                    주식회사 벨라도<br>
                    사업자 등록번호: 342-86-02129<br>
                    통신판매업: 2020-인천연수-1843<br>
                    사업장소재지: 인천 연수 송도과학로 56<br>
                    대표번호 : 1600-6631<br>
                    대표자 : 안정권 손미진<br>
                </div>
            </div>
            <div class="right_box">
               <div class="title">
                    기업은행 486-1948-0815<br>
                    안정권 지제트에스에스그룹<br>
                    info@vellado.com
                </div>
                <div class="btn_box">
                    <div class="btn"><a href="/privacy">개인정보처리방침</a></div>
                    <div class="btn"><a href="/agrement">이용약관</a></div>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>