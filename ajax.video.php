<?php

include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";

$result = array();
$result["returnCode"] = "200"; //OK
$result["errorMsg"] = "";



$db = new SimpleDB($pdo_db);
try {

    $cmd = $_POST['cmd'];

    switch ($cmd) {

        case "video_list" :
            $page = $_POST["page"];

            if(!$page) {
                $page = 1;
            } else {
                $page = intval($page);
            }
            $per_page = 12;


            $from_record = ($page -1) * $per_page;


            $keyword = $_POST['keyword'];

            if(strlen($keyword) > 1) {
                $keyword = $pdo_db->quote("%".$keyword."%");
                $sql = "SELECT * FROM vimeo_video_list where title like {$keyword} order by id desc limit $from_record, $per_page";
                $rows = $db->query($sql);
            } else {
                $sql = "SELECT * FROM vimeo_video_list order by id desc limit $from_record, $per_page";
                $rows = $db->query($sql);
            }

            $list = [];

            $content = "";
            foreach($rows as $row) {

                $row['datetime'] = date('Y.m.d', strtotime($row['release_datetime']));
                $row['thumbnail'] = $row['thumbnail2'];

                $list[] = [
                    "id" => $row['id'],
                    "title" => $row['title'],
                    "description" => $row['description'],
                    "thumbnail" => $row['thumbnail'],
                    "embed_script" => $row['embed_script'],
                    "datetime" => $row['datetime'],
                ];

                $content .= <<<EOT
<div class="vod_thumb_box">
                        <div class="thumbnail"><a href="javacript:;" rel="{$row['id']}"><img src="{$row['thumbnail']}"></a></div>
                        <div class="title"><a href="javacript:;" rel="{$row['id']}">{$row['title']}</a></div>
                        <div class="body"></div>
                        <div class="date_box">
                            <div class="date">{$row['datetime']}</div>
                            <div class="count">- <img src="/resources/images/icon-view.png"></div>
                        </div>
                    </div>
EOT;
            }

            $result['per_page'] = $per_page;
            $result['page'] = $page;
            $result['content'] = $content;


            break;

        case "main_video_list" :
            $page = $_POST["page"];

            if(!$page) {
                $page = 1;
            } else {
                $page = intval($page);
            }
            $per_page = 8;


            $from_record = ($page -1) * $per_page;


            $keyword = $_POST['keyword'];

            if(strlen($keyword) > 1) {
                $keyword = $pdo_db->quote("%".$keyword."%");
                $sql = "SELECT * FROM vimeo_video_list where title like {$keyword} order by id desc limit $from_record, $per_page";
                $rows = $db->query($sql);
            } else {
                $sql = "SELECT * FROM vimeo_video_list order by id desc limit $from_record, $per_page";
                $rows = $db->query($sql);
            }

            $list = [];

            $content = "";
            foreach($rows as $row) {

                $row['datetime'] = date('Y.m.d', strtotime($row['release_datetime']));
                $row['thumbnail'] = $row['thumbnail2'];

                $list[] = [
                    "id" => $row['id'],
                    "title" => $row['title'],
                    "description" => $row['description'],
                    "thumbnail" => $row['thumbnail'],
                    "embed_script" => $row['embed_script'],
                    "datetime" => $row['datetime'],
                ];

                $content .= <<<EOT
<div class="vod_thumb_box">
                        <div class="thumbnail"><a href="/vod/{$row['id']}" rel="{$row['id']}"><img
                                        src="{$row['thumbnail']}"></a></div>
                        <div class="title"><a href="/vod/{$row['id']}"
                                              rel="{$row['id']}">{$row['title']}</a></div>
                        <div class="body"></div>
                        <div class="date_box">
                            <div class="date">{$row['datetime']}</div>
                            <div class="count">- <img src="/resources/images/icon-view.png"></div>
                        </div>
                    </div>
EOT;
            }

            $result['per_page'] = $per_page;
            $result['page'] = $page;
            $result['content'] = $content;


            break;


        case "embed" :

            $id = intval($_POST["video_id"]);

            $sql = "SELECT * FROM vimeo_video_list where id = :id";

            $video = $db->row($sql, compact('id'));

            $result['video'] = $video;
            if(!$video['id']) {
                throw new Exception("video not found!", 404);
            }



            $video['datetime'] = date('Y.m.d', strtotime($video['release_datetime']));

            $result['content'] = <<<EOT
 <div class="video_left_box">
                <div class="player_div" id="player_div">
                    <iframe src="https://player.vimeo.com/video/{$video['id']}?app_id=185495" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" title="{$video['title']}" data-ready="true"></iframe>
                </div>

                <div class="box">
                    <div class="date">{$video['datetime']}></div>
                    <div class="count">- <img src="/resources/images/icon-view.png"></div>
                </div>
            </div>
            <div class="video_right_box">
                <div class="title">{$video['title']}</div>
                <div class="body">{$video['description']}</div>
            </div>


EOT;


            break;

        default :
            throw new Exception("호출오류", 500);

    }
} catch (Exception $ex) {
    $result['returnCode'] = $ex->getCode() . "";
    $result["errorMsg"] = $ex->getMessage();
}
echo json_encode($result);
?>