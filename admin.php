<?php

include_once __DIR__ . "/lib/env.lib.php";
include_once __DIR__ . "/lib/_dbconnect.php";
include_once __DIR__ . "/lib/SimpleDB.php";
include_once __DIR__ . '/board/common.php';

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL ^ E_NOTICE);


session_start();

if (!$_SESSION['ss_mb_id']) {
    Header("Location: /main");
}

$db = new SimpleDB($pdo_db);

$member = $db->row("SELECT * FROM g5_member WHERE mb_id=:mb_id", ['mb_id' => $_SESSION['ss_mb_id']]);


if(!$member['mb_level'] ||  $member['mb_level']  < 9) {
   alert("매니저 이상 접근 가능합니다.", '/main');
   exit;
}


$block_users = [];


$redis = new Redis();
$redis->connect('127.0.0.1', 6379);
$redis->select(3);

$block_list = $redis->keys("CHAT:BLOCK_USER:*");

foreach($block_list as $key) {
    $temp = explode(":", $key);
    $block_userid = $temp[2];

    $member = get_member($block_userid);
    $expire_time = $redis->ttl($key);

    $block_users[] = [
      'mb_id' => $member['mb_id'],
      'mb_nick' => $member['mb_nick'],
      'expire_sec'  => $expire_time,
      'expire_time' => gmdate("H:i:s", $expire_time)
    ];

}

print_r2($block_users);




?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="resources/css/common.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <meta property="og:url" content="https://vellado.com">
    <meta property="og:title" content="VELLADO">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://vellado.com/resources/images/logo.png">
    <meta property="og:description" content="라이브 스트리밍 플랫폼">
    <title>VELLADO</title>
</head>
<body>
<!--최상단 배너-->
<div class="main_banner_container">
    <div class="main_banner_logo">
    </div>
</div>

<!-- 최상단 배너 모바일-->
<div class="main_banner_container_m">
    <div class="main_banner_logo_m">
        <a href="/main"><img src="resources/images/logo-m.png"></a>
    </div>
    <div class="main_banner_btn_m">
        <a href="javascript:history.back()"><img src="resources/images/btn-back.png"></a>
        <a href="/main"><img src="resources/images/btn-home.png"></a>
    </div>
</div>

<!-- 메인 컨텐츠 -->
<div class="main_container">
    <div class="main_content_div">
        <div class="login_title">VELLADO 관리자 기능</div>


        <div class="login_title">차단 사용자 목록</div>
        <div>
            <ul>
                <?php foreach($block_users as $user) : ?>
                <li>
                    <?=$user['mb_nick']?>(<?=$user['mb_id']?>) 남은시간 <?=$user['expire_time']?>
                    <input type="button" value="차단 해제"/>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="flex_height">

        </div>

        <div class="login_title">라이브 스케쥴 관리</div>

        <div class="flex_height">

        </div>


        <div class="login_title"></div>
    </div>
</div>

<script>

    function check_login() {

        var userid = $("input[name=userid]").val();
        var password = $("input[name=password]").val();

        if (userid == "") {
            $("#login_warning").html("아이디를 입력하세요.");
            return;
        }

        if (password == "") {
            $("#login_warning").html("패스워드를 입력하세요.");
            return;
        }

        var param = {
            'userid': userid,
            'password': password
        };

        $.ajax({
            type: "POST",
            url: "/api/member/login",
            cache: false,
            async: false,
            data: param,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.code == 200) {
                    alert('로그인 되었습니다.');
                    location.href = '/main'
                } else {
                    $("#login_warning").html(response['message']);
                    return;
                }
            }
        });
    }

    function go_join() {
        location.href = '/join';
    }
</script>
<?php include_once __DIR__ . "/footer.php" ?>
