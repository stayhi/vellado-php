<div class="footer_container">
    <div class="footer_box">
        <div class="left_box">
            <div class="title">VELLADO</div>
            <div class="body">
                주식회사 벨라도<br>
                사업자 등록번호: 342-86-02129<br>
                통신판매업: 2020-인천연수-1843<br>
                사업장소재지: 인천 연수 송도과학로 56<br>
                대표번호 : 1600-6631<br>
                대표자 : 안정권 손미진<br>
            </div>
        </div>
        <div class="right_box">
            <div class="title">
                기업은행 486-1948-0815<br>
                안정권 지제트에스에스그룹<br>
                info@vellado.com
            </div>
            <div class="btn_box">
                <div class="btn"><a href="/privacy">개인정보처리방침</a></div>
                <div class="btn"><a href="/agrement">이용약관</a></div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    // F12 버튼 방지
    $(document).ready(function(){
        $(document).bind('keydown',function(e){
            if ( e.keyCode == 123 /* F12 */) {
                e.preventDefault();
                e.returnValue = false;
            }
        });
    });

    // 우측 클릭 방지
    document.onmousedown=disableclick;
    status="Right click is not available.";

    function disableclick(event){
        if (event.button==2) {
            alert(status);
            return false;
        }
    }
</script>

</body>
</html>


